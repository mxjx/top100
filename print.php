<?php
function p($var, $type='p') {
    header('Content-type:text/html;charset=utf-8;');
    if ( !is_array($var) && !is_object($var) ) {
        echo "要打印的变量需要是数组或者对象";
    }

    echo "<pre>";
    if(!$type || $type == 'p') {
        print_r($var);

    } else if( $type ='v' ){
        var_dump($var);

    } else {
        return $var;
    }
    echo "</pre>";
    exit;
}