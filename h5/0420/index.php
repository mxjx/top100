<?php
	require_once("JSSDK.php");
	$jssdk = new JSSDK("wx86c83315ed369afc", "16c89e4c8f978d063c8104be06334d42");
	$signPackage = $jssdk->getSignPackage();
	// var_dump($signPackage);
	// exit();
?>
<!DOCTYPE html>
<html lang="zh-cn" class="no-js">
<head>
<meta http-equiv="Content-Type">
<meta content="text/html; charset=utf-8">
<meta charset="utf-8">
<title>匠心 • 打造 腾讯SNG&msup技术开放日</title>
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="format-detection" content="telephone=no">
<meta name="keywords" content="HTML,ASP,PHP,SQL">
<link rel="stylesheet" type="text/css" href="ztimages/reset.css" />
<link rel="stylesheet" type="text/css" href="ztimages/index.css" />
<link rel="stylesheet" type="text/css" href="ztimages/animations.css" />
<link rel="stylesheet" type="text/css" href="ztimages/load.css" />
<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
</head>
<body>
<div style='margin:0 auto;width:0px;height:0px;overflow:hidden;'>
<img src="images/512.jpg" width='700'>
</div>

<?php

  	$url = 'content.php';
  	echo file_get_contents($url);
?>
<div style="display:none">
<script src="http://s4.cnzz.com/stat.php?id=1258734212&web_id=1258734212" language="JavaScript"></script>
</div>
<script> 
wx.config({
	'debug':false,
    appId: '<?php echo $signPackage["appId"];?>',
    timestamp: <?php echo $signPackage["timestamp"];?>,
    nonceStr: '<?php echo $signPackage["nonceStr"];?>',
    signature: '<?php echo $signPackage["signature"];?>',
    jsApiList: [
       'onMenuShareAppMessage','onMenuShareTimeline'
    ]
 });
wx.ready(function () {
	    //朋友
		wx.error(function(res){
			log(res);
		    // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。

		});
		//你的朋友
	    wx.onMenuShareAppMessage({
		    title: "匠心 • 打造 腾讯SNG&msup技术开放日", // 分享标题
		    desc:"14位腾讯SNG业界知名嘉宾，5月7日与你相约深圳!",
		    // link: "http://www.buzz.cn/invite", // 分享链接
		    link: "http://www.top100summit.com/h5/0420", // 分享链接
		    imgUrl: "http://www.top100summit.com/h5/0420/images/512.jpg", // 分享图标
		    type: '', // 分享类型,music、video或link，不填默认为link
		    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
		    success: function () { 
		        // 用户确认分享后执行的回调函数
		    },
		    cancel: function () { 
		        // 用户取消分享后执行的回调函数
		    }
		});
		//朋友圈
        wx.onMenuShareTimeline({
		    title: "匠心 • 打造 腾讯SNG&msup技术开放日", // 分享标题
		    link: "http://www.top100summit.com/h5/0420", // 分享链接
		    desc:"14位腾讯SNG业界知名嘉宾，5月7日与你相约深圳!",
		    imgUrl: "http://www.top100summit.com/h5/0420/images/512.jpg", // 分享图标
		    success: function () { 
		        // 用户确认分享后执行的回调函数
		    },
		    cancel: function () { 
		        // 用户取消分享后执行的回调函数
		    }
		});
   });
</script>

<div style="display:none">
<script src="http://s4.cnzz.com/stat.php?id=1258734212&web_id=1258734212" language="JavaScript"></script>
</div>

