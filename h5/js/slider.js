;! function( Slider ) {
    Slider.prototype.initAnimation = function() {
        var subSwiper,
            initSubSwiper = function() {
                if ( ! subSwiper ) {
                    var $subPower = $( ".js-sub-power-scroll" );
                    subSwiper  = $subPower.swiper( {
                        mode: "horizontal",
                        slidesPerView:1,
                        loop: false
                    } );
                } else {
                    subSwiper.reInit();
                }
            },
            startSlideAnimation = function( swiper ) {
                var $activeSlide = $( swiper.activeSlide() );
                $activeSlide.children().removeClass( "hide" );
                // init sub swiper
                if ( $activeSlide.data( "containSubSwiper" ) || ( $activeSlide.find( ".js-sub-power-scroll" ).length > 0 ) ) {
                    $activeSlide.data( "containSubSwiper", true );
                    initSubSwiper();
                }
            },
            hideAllSlideAnimation = function( swiper ) {
                for ( var i = 0, l = swiper.slides.length; i < l; ++ i ) {
                    $( swiper.slides[i] ).children().addClass( "hide" );
                }
            };

        this.swiper.addCallback( "FirstInit", function( swiper ) {
            setTimeout( function() {
                startSlideAnimation( swiper );
            }, 300);
        } );

        this.swiper.addCallback( "SlideReset", function( swiper, direction ) {
            startSlideAnimation( swiper );
        } );

        this.swiper.addCallback( "SlideChangeEnd", function( swiper, direction ) {
            hideAllSlideAnimation( swiper );
            startSlideAnimation( swiper );
        } );

        hideAllSlideAnimation( this.swiper );
    };
}( MobileSlider );

