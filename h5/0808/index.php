<?php
	require_once("JSSDK.php");
	$jssdk = new JSSDK("wx86c83315ed369afc", "16c89e4c8f978d063c8104be06334d42");
	$signPackage = $jssdk->getSignPackage();
	// var_dump($signPackage);
	// exit();
?>
<!doctype html>
<html>
    <head>
    <meta charset="UTF-8">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="x-rim-auto-match" content="none">
    <meta name="viewport" content="initial-scale=1, minimum-scale=1 maximum-scale=1, user-scalable=no">
    <meta name="screen-orientation" content="portrait">
    <meta name="x5-orientation" content="portrait">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/swiper.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/yuejian.css">
    <script src="js/jquery1.8.3.min.js"></script>
    <script src="js/swiper.min.js"></script>
    <script src="js/swiper.animate.min.js"></script>
    <script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>

    <script type="text/javascript">
      var phoneWidth = parseInt(window.screen.width);
      var phoneScale = phoneWidth / 640;
      var ua = navigator.userAgent;
      if (/Android (\d+\.\d+)/.test(ua)) {
        var version = parseFloat(RegExp.$1);
        // andriod 2.3
        if (version > 2.3) {
          document.write('<meta name="viewport" content="width=640, minimum-scale = ' + phoneScale + ', maximum-scale = ' + phoneScale + ', target-densitydpi=device-dpi">');
          // andriod 2.3以上
        } else {
          document.write('<meta name="viewport" content="width=640, target-densitydpi=device-dpi">');
        }
        // 其他系统
      } else {
        document.write('<meta name="viewport" content="width=640, user-scalable=no, target-densitydpi=device-dpi">');
      }
    </script>
    </head>

    <body>

<?php
  	$url = 'content.php';
  	echo file_get_contents($url);
?>
<div style="display:none">
<script src="http://s95.cnzz.com/stat.php?id=1260104561&web_id=1260104561" language="JavaScript"></script>
</div>
<script> 
wx.config({
	'debug':false,
    appId: '<?php echo $signPackage["appId"];?>',
    timestamp: <?php echo $signPackage["timestamp"];?>,
    nonceStr: '<?php echo $signPackage["nonceStr"];?>',
    signature: '<?php echo $signPackage["signature"];?>',
    jsApiList: [
       'onMenuShareAppMessage','onMenuShareTimeline'
    ]
 });
wx.ready(function () {
	    //朋友
		wx.error(function(res){
			log(res);
		    // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。

		});
		//你的朋友
	    wx.onMenuShareAppMessage({
		    title: "用友技术开放日", // 分享标题
		    desc:"技术变革 企业互联，8月25-26用友技术开放日开启！",
		    link: "http://www.top100summit.com/h5/0808", // 分享链接
		    imgUrl: "https://mrm.msup.com.cn/files/2016/08/10/7-_3-MY7l2m2.jpg", // 分享图标
		    type: '', // 分享类型,music、video或link，不填默认为link
		    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
		    success: function () { 
		        // 用户确认分享后执行的回调函数
		    },
		    cancel: function () { 
		        // 用户取消分享后执行的回调函数
		    }
		});
		//朋友圈
        wx.onMenuShareTimeline({
		    title: "用友技术开放日", // 分享标题
		    link: "http://www.top100summit.com/h5/0808", // 分享链接
		    desc:"技术变革 企业互联，8月25-26用友技术开放日开启！",
		    imgUrl: "https://mrm.msup.com.cn/files/2016/08/10/7-_3-MY7l2m2.jpg", // 分享图标
		    success: function () { 
		        // 用户确认分享后执行的回调函数
		    },
		    cancel: function () { 
		        // 用户取消分享后执行的回调函数
		    }
		});
   });
</script>

<div style="display:none">
<script src="http://s4.cnzz.com/stat.php?id=1258734212&web_id=1258734212" language="JavaScript"></script>
<script>
  var basePath = 'http://images.01zhuanche.dev:8080/statics/touch/html51/company';  
      $(function() {
        $(window).on("load", function() {
            
            $("#loading").fadeOut()
              
            var mySwiper = new Swiper('.swiper-container', {
              onInit: function(swiper) { 
                swiperAnimateCache(swiper);
                swiperAnimate(swiper);
              },
              onSlideChangeEnd: function(swiper) {
                swiperAnimate(swiper); 
              },
              
              mousewheelControl: true, 
              direction: 'vertical', 
              preventClicks: false,
            })
          })
      })
    </script>
</div>

