<?php
ini_set('date.timezone','Asia/Shanghai');
error_reporting(E_ERROR);

require_once "../../print.php";
require_once "../lib/WxPay.Api.php";
require_once '../lib/WxPay.Notify.php';
require_once 'log.php';
require '../../phpcms/modules/api/classes/curl.class.php';
//初始化日志
$logHandler= new CLogFileHandler("../logs/".date('Y-m-d').'.log');
$log = Log::Init($logHandler, 15);

class NativeNotifyCallBack extends WxPayNotify
{
	public function unifiedorder($openId, $product_id)
	{
		//统一下单
		$input = new WxPayUnifiedOrder();
		$input->SetBody("test");
		$input->SetAttach("test");
		$input->SetOut_trade_no(WxPayConfig::MCHID.date("YmdHis"));
		$input->SetTotal_fee("1");
		$input->SetTime_start(date("YmdHis"));
		$input->SetTime_expire(date("YmdHis", time() + 600));
		$input->SetGoods_tag("test");
		$input->SetNotify_url("http://paysdk.weixin.qq.com/example/notify.php");
		$input->SetTrade_type("NATIVE");
		$input->SetOpenid($openId);
		$input->SetProduct_id($product_id);
		$result = WxPayApi::unifiedOrder($input);
		Log::DEBUG("unifiedorder:" . json_encode($result));
		return $result;
	}
	
	public function NotifyProcess($data, &$msg)
	{
		if($data['result_code'] == 'SUCCESS'){
			$this->updateMrmOrderStart($data);
		}else{

		}
		return true;
	}
	/**
	 * 支付成功之后 同步更新后台订单的数据
	 */
	public function updateMrmOrderStart($data)
	{
		$curl = new curl();
		$request = [
			'goupiaoyonghuxinxi' =>[
            	'mou_outTradeNo' => $data['out_trade_no'],
			],
		];
		$return = $curl->curl_action('/order-api/pay-order-success',$request);
		Log::DEBUG("开始测试更新后台数据:".$return);
	}
}
$notify = new NativeNotifyCallBack();
$notify->Handle(true);
