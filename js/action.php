<?php
if(empty($_GET['act'])){
	$action = "";
}else{
	$action = $_GET['act'];
}
if($action=='delimg'){
	$filename = $_POST['imagename'];
	if(!empty($filename)){
		unlink('upload/face/'.$filename);
		echo '1';
	}else{
		echo '删除失败.';
	}
}else{
	$picname = $_FILES['mypic']['name'];
	$picsize = $_FILES['mypic']['size'];
	if ($picname != "") {
		if ($picsize > 3*1024000) {
			echo '<span style="color:red;">图片大小不能超过3M</span>';
			exit;
		}
		$type = explode(".",$picname);
        $type = $type[count($type)-1];
		$type = strtolower($type);
		if ($type != "gif" && $type != "jpg"&& $type != "png") {
			echo '<span style="color:red;">图片格式不对！</span>';
			exit;
		}
		$rand = rand(100, 999);
		$pics = date("YmdHis") . $rand .".". $type;
		//上传路径
		$dir ='uploadfile/'.date("Y").'/'.date('md');
        @chmod($_SERVER['DOCUMENT_ROOT']."/".$dir,0777);//赋予权限
        if(!is_dir($_SERVER['DOCUMENT_ROOT']."/".$dir)){
        	mkdir($_SERVER['DOCUMENT_ROOT']."/".$dir,0777);
        }
		$pic_path = $_SERVER['DOCUMENT_ROOT']."/".$dir."/".$pics;
		move_uploaded_file($_FILES['mypic']['tmp_name'], $pic_path);
	}
	$size = round($picsize/1024,2);
	$image_size = getimagesize($pic_path);
	$arr = array(
		'name'=>$picname,
		'pic'=>$dir."/".$pics,
		'size'=>$size,
		'width'=>$image_size[0],
		'height'=>$image_size[1]
	);
	echo json_encode($arr);
}
?>