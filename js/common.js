$(function(){

var sl_base_height = $('.sl').height()
var sl_real_height = 0;
$(".sl a").each(function(){
	sl_real_height += $(this).get(0).offsetHeight;
})
	// $('.sl').bind('mouseenter', function(){
	// 	$(this).height(sl_base_height * $('.sl a').length)
	// 	$('.sl-ar').addClass('a')
	// })
	$('.sl').bind('mouseleave', function(){
		$(this).height(sl_base_height)
		$('.sl-ar').removeClass('a')
	})
	$('.sl a').bind('click', function(){
		var a = $(this)
		if(a.index() != 0){
			setTimeout(function(){
				a.insertBefore($('.sl a').eq(0))
			}, 200)
		}
		var val = a.attr('value')
		if ($(".sl").height() == sl_base_height) {
			$('.sl').height(sl_real_height);
		} else {
			$('.sl').height(sl_base_height);
		}
	})
	$(".zhiku-container, .bn,.h,.t,.z-schc form").click(function(){
		$('.sl').height(sl_base_height);
	})
	init()
	$('.box').each(function(i, e){
		var w = $(window).width()
		var h = $(window).height()
		var boxw = $(e).width()
		var boxh = $(e).height()
		var padding = parseInt($(e).css('padding-left').replace('px', ''))
		$(e).css('left', (w - boxw - padding)/2)
		$(e).css('top', (h - boxh - padding)/2)

	})
	$(".box-background").click(function(){
		$(this).hide();
		$(".box").hide();
		$("video").trigger("pause");
	})
	// // 出版物点击事件
	// $('.pub-l a').bind('click', function(){
	// 	$('.pub-l a').removeClass('a')
	// 	$(this).addClass('a')
	// 	var index = $(this).index()
	// 	$('.pub-r').height($('.pub-r').height())
	// 	var top = 0;
	// 	for(index+1;index>0;index--) {
	// 		top -= $('.pub-rc').eq(index).height();
	// 	}
	// 	$('.pub-rc').eq(0).stop().animate({marginTop:top}, 500)
	// }).eq(0).trigger('click')
	$(".pub-l a").click(function(){
		$('.pub-l a').removeClass('a')
		$(this).addClass('a')
		var index = $(this).index();
		$('.pub-r').height($('.pub-r').height())
		var top = 0;
		for(index;index>0;index--) {
			if($(window).width()>650){
				top -= $('.pub-rc').eq(index-1).get(0).offsetHeight;
			}else{
				top -= $('.pub-rc').eq(index-1+3).get(0).offsetHeight;
			}
		}
		$('.pub-r').stop().animate({marginTop:top}, 500)
	})
	$('.js_login').bind('click', function(){
		box_show(1)
	})

	$('.js_reg').bind('click', function(){
		box_show(2)
	})
	$('.js_cancelbox').bind('click', function(){
		box_show(0)
	})
	$('.fqa-b a').bind('click', function(){
		$('.fqa-b').removeClass('a').find('h2').height(0)
		var h = $(this).parent().find('p').height() + 45
		$(this).parent().addClass('a').find('h2').height(h)
	}).eq(0).trigger('click')
	$(".box-bg").click(function(){
		box_hide();
	})
	
	//  菜单点击事件
	$(".h-navm a").on('click', function(){
		var  index = $(this).index();
		var hasDisplay = $(".nav-menu").eq(index).css("display") === "none"
		$(".nav-menu").hide();
		if (hasDisplay) {
			$(".nav-menu").eq(index).show();
		}

	})

	$('.review-mb a').bind('click', function(){
		var index = $(this).index()
		$('.review-mb a').removeClass('a')
		$(this).addClass('a')
		var left = $('.review-m ul li').width() * index
		$('.review-m ul').stop().animate({marginLeft:-left}, 500)
	})
	//智库列表页瀑布流示例
	//方便与ajax结合使用，请将返回的数据打包成datalist参数相同的格式
	//完成后请删除示例相关代码
	
	//2015-08-12 gtt
	//报名 
	$(".gtt-ticket-type input").change(function(){
		if($(this).is(":checked")){
			$(this).parent("label").addClass("cur");
			$(this).parent("label").siblings("label").removeClass("cur");
		}
	});

	$(".gtt-radio1 label:last").css("color","#ff0000");
	
	if($(window).width()<680){
		$(".gtt-mes li:last input").css("width","90%");
		if($(this).is(":checked")){
			$(".gtt-ticket-type label").css("color","#333333");
		}
		$(".gtt-ticket-type label").css("color","#333333");
	}

	
	
	
	
})
function init() {
	if($('.bn-c').length > 0){
		$('.bn-c').css('left', ($('.bn').width() - $('.bn-c').width())/2 - $('.bn-c').css('padding-left').replace('px', ''))
	}
	var window_width = $(window).width();
	if( window_width >= 1150 ) {
		$('.z-schk').attr('value', '')
	} else if (window_width < 1150 && window_width > 720) {
		var liNum = 3;	
	} else if (window_width < 720 && window_width > 640) {
		var liNum = 2;
	} else {
		var liNum = 1;
	}

	if (window_width < 1150) {
		$('.z-schk').attr('value', '关键词')
		var html = '';
		for (var i = 0; i< liNum; i++) {
			html += '<div class="l z-wtn z-wtn-style-'+liNum+'"></div>'
		}
		$('.z-wt').html(html)
	}	
}
var hasLoad = new Array();
function waterfall_example( datalist) {
	var datalist = datalist;
	waterfall(datalist, '.z-wtn', function(){
		$(".loading").hide();
		stype = 1;
		// alert('callback:一页数据加载完成')
	})
}

//简单的瀑布流 by wanderwind
function waterfall(datalist, target, callback) {
	if($(target).length == 0){
		return
	}
	var tpl = function(data) {
		if (hasLoad[data['courseid']] == 1){
			return
		}
		imgPath = '/statics/css/top100/img/';
		if(data['courseid'].length < 1){
			data['url'] = 'javascript:void(0);'
		} else {
			data['url'] = '/think/'+data['courseid'];
		}
		hasLoad[data['courseid']] = 1;
		var html = ''
		html += '<a href="'+data['url']+'" target="_blank" class="z-wtc">'
		html += '<h2>'+data['title']+'</h2>'
		html += '<div class="z-wtc-m">'
		if (!data['courseLecturer']) {
			lecturers = data['lecturer'];
		} else {
			lecturers = data['courseLecturer'];
		}
		// 输出教练信息
		if (lecturers) {
			var lecturerHtml = '';
			var lecturerNum = lecturers.length;
			for(var i=0; i < lecturerNum; i++) {
				var lecturer = lecturers[i]['lecturer'];
				// 检查是否有教练
				if (lecturer==null) {
					break;	
				} 
				if (lecturer['thumbs']) {
					var thumb = JSON.parse(lecturer['thumbs']);
				} else {
					thumb = '';
				}
				if (thumb[0]['thumbnailUrl'] != undefined) 
				{
					var thumb = thumb[0]['thumbnailUrl'];
				} else if (thumb[0]['fileUrl']) {
					var thumb = thumb[0]['fileUrl'];
				}
				lecturerHtml += '<img src="'+thumb+'" class="l" />';

				lecturerHtml += '<div class="r lecturer-info"><p>'+lecturer['name']+'</p><p>'+lecturer['company']+'&nbsp;&nbsp;'+lecturer['position']+'</p></div>'
				lecturerHtml += '<div class="b"></div>';
			}
		}
		if (lecturerHtml=='') {
			return '';
		}	
		html += lecturerHtml;
		// html += '<h1>'+data['title']+'</h1>'
		if (!data['desc'] && data['content']) {
			html += '<p class="course_desc">'+data['content']+'</p>'
		} else if (data['desc']) {
			html += '<p class="course_desc">'+data['desc']+'</p>'
		} 
		// var comments = Math.floor(Math.random() * 1000)+parseInt(data['comments']);
		html += '</div>'
		html += '<div class="z-wtc-b">'
		// html += '<span><img src="'+imgPath+'z-icon1.png" />'+comments+'</span>'
		html += '<span><img src="'+imgPath+'z-icon2.png" />'+parseInt(data['praises'])+'</span>'
		if (data['auditionvideo'].length > 0) {
			html += '<span><img src="'+imgPath+'z-icon3.png" /></span>'
		}
		html += '</div>'
		html += '</a>'
		return html
	}
	var minh = function(t) {
		var minElem = $(t).eq(0)
		var min = minElem.height()
		$(t).each(function(i, e){
			var h = $(e).height()
			if(h < min) {
				min = h
				minElem = $(e)
			}
		})
		return minElem
	}
	var run = function() {

		if(datalist != undefined && datalist.length == 0){
			if(callback){
				callback()
			}
			return
		}

		var data = datalist.shift()
		var html = tpl(data);
		if (html!='' && html){
			minh(target).append(html).children().last().css('opacity', 0).stop().animate({opacity:1}, 500)
		}
		setTimeout(function(){
			run()
		}, 100)

	}
	run()
}

// 弹出框显示
var box_show_status = 0;
function box_show(n) {
	if(n == 0 || n == undefined) {
		n = '';
	}
	var bg = $('.box-bg');
	var box = $('.box'+n);
	var speed = 500;
	if(n == ''){
		bg.stop().animate({opacity:0},speed, function(){
			$(this).hide();
		});
		box.stop().animate({opacity:0},speed, function(){
			$(this).hide();
			box_show_status = 0;
		});
	}else{
		var a = function(){
			box.css('opacity', 0).show().stop().animate({opacity:1}, speed, function(){
				box_show_status = n;
			});
		};
		if(box_show_status != 0){
			$('.box' + box_show_status).stop().animate({opacity:0},speed, function(){
				$(this).hide();
				a();
			});
		}else{
			bg.css('opacity', 0).show().stop().animate({opacity:0.6}, speed);
			a();
		}
	}
}

function box_hide() {
	box_show(0);
}