<?php
pc_base::load_app_class('api', 'api');
class committee  extends api {
	public function __construct() {
		$this->curl = new curl();
	}
    public function changeVenue($old){
        $arr[5] = 2;
        $arr[1] = 4;
        $arr[3] = 3;
        $arr[4] = 5;
        $arr[2] = 1;
        return $arr[$old];
    }
	public function initOld(){
       $request = [
            'mm' => 'chupinren',
            'mw' => 'mpr_venue != 0 ',
            'mr' => [
                'huiyuanxinxi' => [
                    'mm' => 'huiyuanxinxi'
                ]
            ]
        ];
        $return = $this->curl->curl_action('api/index',$request);
        $list = $return['data'];
        $len = count($list);
        
        foreach ($list as $k => $v) {
            if($v['year'] == '2015' || $v['year'] == '2016'){
                // p($this->changeVenue($v['venue']));
                $v['venue'] = $this->changeVenue($v['venue']);
                $data[$v['venue']][] = $v;
            }else{
                $before[] = $v;
            }
        }
        // p($data);
		include template('content', 'list_chupinren');
	}
    public function init()
    {
        $request = [
            'mm' => 'chupinren',
            'mw' => 'mpr_venue != 0 ',
            'mr' => [
                'huiyuanxinxi' => [
                    'mm' => 'huiyuanxinxi'
                ]
            ]
        ];
        $return = $this->curl->curl_action('api/index',$request);
        $list = $return['data'];
        $len = count($list);
        $i = 0;
        foreach ($list as $k => $v) {
            if($v['year'] == '2018'){
                $result[$v['venue']][$i]['id'] = $v['id'];
                $result[$v['venue']][$i]["user_id"] = $v["user_id"];
                $result[$v['venue']][$i]["venue"] = $v["venue"];
                $result[$v['venue']][$i]["venueName"] = $v["venueName"];
                $result[$v['venue']][$i]["year"] = $v["year"];

                $info = $v["memberInfo"];
                $result[$v['venue']][$i]["name"] = $info['name'] ? : '';
                $result[$v['venue']][$i]["company"] = $info['company'] ? : '';
                $result[$v['venue']][$i]["position"] = $info['position'] ? : '';
                $thumb = $info['thumbs'] ?
                    json_decode($info['thumbs'])[0]->fileUrl :
                    'https://mrm.msup.com.cn/Public/Admin/images/default_headpic.png';
                $result[$v['venue']][$i]["thumb"] = $thumb;
                $desc = $v["description"] ? : $info["description"];
                if(mb_strlen($desc,'UTF8') > 110){
                    $desc =  mb_substr($desc,0,110,"utf-8").'...' ;
                }
                $result[$v['venue']][$i]["description"] = $desc ? : '';
                $i++;
            }else if($v['year'] != '' && $v['year'] != 0){
                $before[] = $v;
            }
        }
        if(!empty($before)){
            $before = $this->arrangeFun($before);
        }
//        echo '<pre>';
//        var_dump($result);die;
	    $result[1] = $result[1]?:[];
	    $result[2] = $result[2]?:[];
	    $result[3] = $result[3]?:[];
	    $result[4] = $result[4]?:[];
	    $result[5] = $result[5]?:[];
	    $result[1026] = $result[1026]?:[];







        if(!empty($result)){
            ksort($result); //按key排序
        }
        include template('content', 'list_committee');
    }
    public function arrangeFun($data){
        foreach($data as $k => $v){
            $result[$k]['id'] = $v['id'];
            $result[$k]["user_id"] = $v["user_id"];
            $result[$k]["venue"] = $v["venue"];
            $result[$k]["venueName"] = $v["venueName"];
            $result[$k]["year"] = $v["year"];

            $info = $v["memberInfo"];
            $result[$k]["name"] = $info['name'] ? : '';
            $result[$k]["company"] = $info['company'] ? : '';
            $result[$k]["position"] = $info['position'] ? : '';
            $thumb = $info['thumbs'] ?
                json_decode($info['thumbs'])[0]->fileUrl :
                'https://mrm.msup.com.cn/Public/Admin/images/default_headpic.png';
            $result[$k]["thumb"] = $thumb;
            $desc = $v["description"] ? : $info["description"];
            if(mb_strlen($desc,'UTF8') > 110){
                $desc =  mb_substr($desc,0,110,"utf-8").'...' ;
            }
            $result[$k]["description"] = $desc ?  $desc : '';

        }
        return $result;
    }
    public function newAll($value='')
    {   
       
        $request = [
            'mm' => 'chupinren',
            'mw' => 'mpr_venue != 0',
            'mr' => [
                'huiyuanxinxi' => [
                    'mm' => 'huiyuanxinxi'
                ]
            ]
        ];
        $return = $this->curl->curl_action('api/index',$request);
         $list = $return['data'];
        for ($i=0; $i < count( $list); $i++) { 
            $list_ImgArr = $list[$i]['memberInfo']['thumbs'];
            $list_ImgArr = json_decode($list_ImgArr);
            if($list_ImgArr[0]->thumbnailUrl !=""){
                $list[$i]['img'] = $list_ImgArr[0]->thumbnailUrl;
            }else if($list_ImgArr[0]->fileUrl!=""){
                $list[$i]['img'] = $list_ImgArr[0]->fileUrl;
            }else{
                $list[$i]['img'] = "https://mrm.msup.com.cn/Public/Admin/images/default_headpic.png";
            }
            if($list[$i]['year'] == '2015'){
                $list_data_2015[] = $list[$i];
            }else{
                $list_data_old[] = $list[$i];
            }
        }
        if($_GET['type'] == "2015"){
            unset($list_data_old);
        }else{
            unset($list_data_2015);
        }
        // $type  = ;

        include template('content', 'list_chupinren_all');

    }
    
}

?>
