<?php
pc_base::load_app_class('api', 'api');
pc_base::load_app_class("HttpService", 'api');
class activityapi  extends api {
    public function __construct() {
        $this->curl = new curl();
    }
    public function init() {
        $params = $this->initParams();
        // $params['mw'] .= ' AND startTime > '.time();
        $rows = $this->curl->curl_action('api/index', $params);
        if ($rows['errno'] == 0) {
            // 将教练信息归并到最上层的数组中
            foreach($rows['data'] as &$row) {
                $row['poster'] = $this->getAttachUrl($row['poster']);
                $row['lecturers'] = [];
                if (empty($row['schedulingVenueCourse'])) continue;
                $row['lecturers'] = $this->getLecturerFromCourseLecturer($row['schedulingVenueCourse']);
                unset($row['schedulingVenueCourse']);
            }
        } else {
            // showmessage($rows['errmsg'], '/index.php');
        }

        $position =$position = $this->getPosition(['mc_assignToTop100' => 1]);
        //以下代码是加入 技术开放日数据
        $openParams = $this->openParams();
        $openRows = $this->curl->curl_action('api/index', $openParams);

        foreach( $openRows['data'] as $val){
            $opening[$val['sid']] = $val;
        }

        $schedulings = $rows['data'];

//        echo '<pre>';
//	    var_dump($schedulings);die;

        foreach ($schedulings as $k => $v) {
            if($v['id'] <= 355) {//355之前的数据是排课功能里的,之后的数据取技术开放日功能
                $v['thumbnail'] = $v['thumbnail'] ? json_decode($v['thumbnail'], true)[0]['fileUrl'] : $v['poster'];
                $schedulings[$k] = $v;
                if ($v['startTime'] > time()) {
                    $lasted[] = $v;
                }
            }else{
                if($opening[$v['id']]){
                    $openThumb = $opening[$v['id']]['thumbnail'];
                    $openThumb = $openThumb ? json_decode($openThumb, true)[0]['fileUrl'] : $opening[$v['id']]['banner'];
                    $opening[$v['id']]['thumbnail'] = $openThumb;
                    $schedulings[$k] = $opening[$v['id']];
                    $schedulings[$k]['address'] = $opening[$v['id']]['address'];
                    $schedulings[$k]['title'] = $opening[$v['id']]['title'];
                    $schedulings[$k]['startTime'] = $opening[$v['id']]['startTime'];
                    $schedulings[$k]['endTime'] = $opening[$v['id']]['endTime'];
                    $schedulings[$k]['html'] = 'opening';
                    if ($opening[$v['id']]['startTime'] > time()) {
                        $lasted[] = $opening[$v['id']];
                        $lasted[]['address'] =$opening[$v['id']]['address'];
                        $lasted[]['title'] =$opening[$v['id']]['title'];
                        $lasted[]['startTime'] =$opening[$v['id']]['startTime'];
                        $lasted[]['endTime'] =$opening[$v['id']]['endTime'];
                        $lasted[]['html'] = 'opening';
                    }
                }else{
//                    echo '<pre>';
//                    var_dump($schedulings[$k]);die;
                    unset($schedulings[$k]);
                }
                // unset($schedulings[$k]);
            }
        }
        if(empty($schedulings)){

            echo $this->result($schedulings,false);die;

        }else{

            echo $this->result($schedulings,true);die;
        }
    }


    public function result($data,$type){
        if($type){
            $data = ['errno' => 0,'data' => $data];
        }else{
            $data = [
                'errno' => 1,
                'msg' => '没有数据'
            ];
        }
        if($_GET['callback']){
            return  trim(trim($_GET['callback'])."(".json_encode($data).")");

        }else{

            return  json_encode($data) ;
        }

    }
    public function open() { //走进案例测试页面
        $params = $this->initParams();
        // $params['mw'] .= ' AND startTime > '.time();
        $rows = $this->curl->curl_action('api/index', $params);
        if ($rows['errno'] == 0) {
            // 将教练信息归并到最上层的数组中
            foreach($rows['data'] as &$row) {
                $row['poster'] = $this->getAttachUrl($row['poster']);
                $row['lecturers'] = [];
                if (empty($row['schedulingVenueCourse'])) continue;
                $row['lecturers'] = $this->getLecturerFromCourseLecturer($row['schedulingVenueCourse']);
                unset($row['schedulingVenueCourse']);
            }
        } else {
            // showmessage($rows['errmsg'], '/index.php');
        }

        $position =$position = $this->getPosition(['mc_assignToTop100' => 1]);

        //技术开放日数据
        $openParams = $this->openParams();
        $openRows = $this->curl->curl_action('api/index', $openParams);
        //  $opening = $openRows['data'];
        foreach( $openRows['data'] as $val){
            $opening[$val['sid']] = $val;
        }

        $schedulings = $rows['data'];
        foreach ($schedulings as $k => $v) {
            if($v['id'] <= 355) {//355之前的数据是排课功能里的,之后的数据取技术开放日功能
                $v['thumbnail'] = $v['thumbnail'] ? json_decode($v['thumbnail'], true)[0]['fileUrl'] : $v['poster'];
                $schedulings[$k] = $v;
                if ($v['startTime'] > time()) {
                    $lasted[] = $v;
                }
            }else{
                if($opening[$v['id']]){
                    $openThumb = $opening[$v['id']]['thumbnail'];
                    $openThumb = $openThumb ? json_decode($openThumb, true)[0]['fileUrl'] : $opening[$v['id']]['banner'];
                    $opening[$v['id']]['thumbnail'] = $openThumb;

                    $schedulings[$k] = $opening[$v['id']];
                    $schedulings[$k]['address'] = $opening[$v['id']]['address'];
                    $schedulings[$k]['title'] = $opening[$v['id']]['title'];
                    $schedulings[$k]['startTime'] = $opening[$v['id']]['startTime'];
                    $schedulings[$k]['endTime'] = $opening[$v['id']]['endTime'];
                    $schedulings[$k]['html'] = 'opening';
                    if ($opening[$v['id']]['startTime'] > time()) {
                        $lasted[] = $opening[$v['id']];
                        $lasted[]['address'] =$opening[$v['id']]['address'];
                        $lasted[]['title'] =$opening[$v['id']]['title'];
                        $lasted[]['startTime'] =$opening[$v['id']]['startTime'];
                        $lasted[]['endTime'] =$opening[$v['id']]['endTime'];
                        $lasted[]['html'] = 'opening';
                    }
                }else{
                    unset($schedulings[$k]);
                }
            }
        }
        include template('api', 'activity');
    }
    public function showOpen()
    {
        if (empty($_GET['id']) || !preg_match("/^[0-9]*$/", $_GET['id']))  showmessage('您访问的页面不存在','index.php?m=api&c=activity');
        $opening = $this->getOpening();
        //日程 (包括会场)
        $scheduling = $opening["openingVenue"];
        //出品人
        $producer = $opening["openingProducer"];
        //赞助 (包括赞助栏目)
        $sponsor = $opening['openingColumn'];

        if(count($scheduling) > 1){//会场超过两个
            //获取会场名称
            foreach($scheduling as $venue){
                $venueName[] = $venue['venueName'];
            }
        }
        $banner = json_decode($opening['banner'], 1);
        include template('api', 'activity_open');
    }
    public function show()
    {
        if (empty($_GET['id']) || !preg_match("/^[0-9]*$/", $_GET['id']))  showmessage('您访问的页面不存在','index.php?m=api&c=activity');

        $scheduling = $this->getScheduling();
        $poster = json_decode($scheduling['poster'], 1);
        // p($poster);
        // $courses = [];//所有的课程集合
        // 提取出教练和课程，去除掉重复的教练和课程
        foreach($scheduling['schedulingVenueCourse'] as $course) {
            $course['course']['startTime'] = strtotime($course['startTime']);
            $course['course']['endTime'] = $course['endTime'];
            $course['course']['date'] = $course['date'];
            $course['course']['snid'] = $course['snid'];
            $courses[] = $course['course'];
        }
        $courses = array_values(arraySort($courses,"startTime"));
        $courses = $this->sortCourseBySnid($courses);
        $lecturers = $this->getLecturersNoRepeat($courses);
        $position = $this->getPosition(['mc_assignToTop100' => 1]);
        unset($scheduling['schedulingVenueCourse']);
        if($_GET['id'] == "295"){
            unset($courses[0]);
            unset($courses[3]);
            $courses = array_values($courses);
        }
        switch ($_GET['id']) {
            case '204':
                include template('api', 'activity_ant');
                break;
            case '245':
                include template('api', 'activity_sng');
                break;
            case '278':
                include template('api', 'activity_baidu');
                break;
            case '280':
                include template('api', 'activity_hpe');
                break;
            case '295':
                include template('api', 'activity_userFirends');
                break;
            case '314':
                include template('api', 'activity_guigu');
                break;
            case '319':
                include template('api', 'activity_top100');
                break;
            case '355':
                include template('api', 'activity_tencent');
                break;
            default:
                include template('api', 'showActivity');
                break;
        }
    }
    public function getScheduling(){
        $params = [
            'mm' => 'paike',
            'mw' => ['ms_type'=>5, 'ms_id' => $_GET['id']],
            'mr' => [
                'huichangkecheng' => [
                    'mm' => 'huichangkecheng',
                    'mp' => 'msvc_date asc,msvc_startTime asc',
                    'mr' => [
                        'kecheng' => [
                            'mm' => 'kecheng',
                            'ms' => 'mc_title,mc_content',
                            'mr' => [
                                'kechengjiaolian' => [
                                    'mm' => 'kechengjiaolian',
                                    'mr' => [
                                        'jiangshi' => [
                                            'mm' => 'jiangshi',
                                            'ms' => "ml_name,ml_thumbs,ml_company,ml_position,ml_description"
                                        ]
                                    ],
                                ]
                            ]
                        ],
                    ]
                ]
            ]
        ];
        $scheduling = $this->curl->curl_action('api/index', $params);
        $scheduling = $scheduling['data'][0];
        return $scheduling;
    }
    public function getOpening(){
        $params = [
            'mm' => 'kaifangri',
            'mw' => ['mo_display' => 1 ,'mo_id' => $_GET['id']],
            'mr' => [
                'kaifangrizanzhulanmu' => [
                    'mm' => 'kaifangrizanzhulanmu',
                    'mp' => 'moc_sort asc',
                    'mr' => [
                        'kaifangrizanzhu' => [
                            'mm' => 'kaifangrizanzhu',
                            'mp' => 'mos_sort asc'
                        ]],
                ],
                'kaifangrihuichang' => [
                    'mm' => 'kaifangrihuichang',
                    'mp' => 'mov_sort asc',
                    'mr' => [
                        'kaifangriricheng' => [
                            'mm' => 'kaifangriricheng',
                            'mp' => 'mosc_sort asc'
                        ]
                    ],
                ],
                'kaifangrichupinren' => [
                    'mm' => 'kaifangrichupinren',
                    'mp' => 'mop_sort asc'
                ]
            ],
        ];
        $opening = $this->curl->curl_action('api/index', $params);
        $opening = $opening['data'][0];
        return $opening;
    }
    /**
     * 按照时间排序之后 在按照会场创建顺寻排序
     * @wyq
     * @date  2016-07-06
     * @param string     $value [description]
     */
    public function sortCourseBySnid($courses)
    {
        foreach ($courses as $course) {
            $arr[$course['startTime']][] = $course;
        }
        foreach ($arr as $k=>$v) {
            if(count($v) > 1 ){
                $v = array_values(arraySort($v,"snid"));
                $arr[$k] = $v;
            }
        }
        foreach ($arr as $v) {
            $len = count($v);
            for($i=0;$i<$len;$i++){
                $newCourses[] = $v[$i];
            }
        }
        return $newCourses;
    }
    /**
     * 去掉沙龙活动重复的教练
     * @wyq
     * @date   2016-07-06
     * @param  string     $value [description]
     * @return [type]            [description]
     */
    public function getLecturersNoRepeat($courses)
    {
        foreach ($courses as $v) {
            $lecturers[$v['courseLecturer'][0]['lid']] = $v['courseLecturer'][0]['lecturer'];
        }
        return array_values($lecturers);
    }
    public function history(){
        $params = $this->initParams();
        $params['mw'] .= '  AND ms_startTime < '.time();
        $rows = $this->curl->curl_action('api/index', $params);
        if ($rows['errno'] == 0) {
            // 将教练信息归并到最上层的数中
            foreach($rows['data'] as &$row) {
                $row['poster'] = $this->getAttachUrl($row['poster']);
                $row['lecturers'] = [];
                if (empty($row['schedulingVenueCourse'])) continue;
                $row['lecturers'] = $this->getLecturerFromCourseLecturer($row['schedulingVenueCourse']);
                unset($row['schedulingVenueCourse']);
            }
        } else {
            showmessage($rows['errmsg'], '/index.php');
        }

        $position =$position = $this->getPosition(['mc_assignToTop100' => 1]);
        $schedulings = $rows['data'];
        include template('api', 'activity');
    }

    /**
     * 报名界面
     */
    public function signup(){

        if (empty($_GET['id']) || !preg_match("/^[0-9]*$/", $_GET['id']))  showmessage('您访问的页面不存在','index.php?m=api&c=activity');
        $params = [
            'mm' => 'paike',
            'ms' => 'ms_title,ms_content,ms_startTime,ms_endTime,ms_address,ms_clicks,ms_comments,ms_poster,ms_praises,ms_applicans,ms_celling',
            'mw' => ['ms_type'=>5, 'ms_id' => $_GET['id']],
            'mr' => [
                'huichangkecheng' => [
                    'mm' => 'huichangkecheng',
                    'mp' => 'msvc_date desc,msvc_startTime asc',
                    'mr' => [
                        'kechengjiaolian' => [
                            'mm' => 'kechengjiaolian',
                            'mr' => [
                                'jiangshi' => [
                                    'mm' => 'jiangshi',
                                    'ms' => "ml_name,ml_thumbs,ml_company,ml_position,ml_description"
                                ],
                                'kecheng' => [
                                    'mm' => 'kecheng',
                                    'ms' => 'mc_title,mc_content'
                                ]
                            ]
                        ]
                    ]
                ]
            ]

        ];

        $scheduling = $this->curl->curl_action('api/index', $params);
        $scheduling = $scheduling['data'][0];
        // echo json_encode($scheduling);exit;
        include template('api', 'signup');
    }

    public function signupSubmit(){

        $username = trim($_POST['username']);
        $usertel = trim($_POST['usertel']);
        $email = trim($_POST['email']);
        $company = trim($_POST['company']);
        $position = trim($_POST['position']);
        $desc = trim($_POST['desc']);
        $activityid = trim($_POST['activityid']);
        $messageSource = trim($_POST['messageSource']);
        $json = ["mm" => "yuyue",
            "mrq" => [
                "map_sid" => $activityid,
                "map_name" => $username,
                "map_phone" => $usertel,
                "map_company" => $company,
                "map_position" => $position,
                "map_type" => "2",
                "map_time" => "",
                "map_email" => $email,
                "map_address" => "",
                "map_source" => "salon",
                "map_messageSource" => $messageSource,
                'map_description'=>$desc
            ]
        ];
        $res = HttpService::http(HttpService::$create_appoint, $json,0);
        echo json_encode($res);
    }
    private function openParams($where){
        $params = [
            'mm' => 'kaifangri',
            'ms' => 'mo_id,mo_title,mo_startTime,mo_endTime,mo_address,mo_link,mo_thumbnail,mo_banner,mo_sid',
            'mw' => 'mo_display>0',
            'mp' => 'mo_id desc'
        ];
        if($where){
            $params['mw'] = $where;
        }
        return $params;
    }
    private function initParams($where){
        $pageNum = !empty($_GET['n']) ? $_GET['n'] : 1;
        $listNum = 20;
        $params = [
            'mm' => 'paike',
            'ms' => 'ms_title,ms_content,ms_startTime,ms_address,ms_clicks,ms_comments,ms_poster,ms_applicans,ms_celling,ms_createdAt,ms_thumbnail',
            'mw' => 'ms_type=5',
//            'mo' => ($pageNum-1)*$listNum,
//            'ml' => $listNum,
            'mp' => 'ms_id desc',
            'mr' => [
                'huichangkecheng' => [
                    'mm' => 'huichangkecheng',
                    'mr' => [
                        'kechengjiaolian' => [
                            'mm' => 'kechengjiaolian',
                            'mr' => [
                                'jiangshi' => [
                                    'mm' => 'jiangshi',
                                    'ms' => "ml_name,ml_company,ml_position"
                                ]
                            ]
                        ]
                    ]
                ]
            ]

        ];
        return $params;
    }
    private function getKeyFromCourseLecturer($scheduling, $key){
        $idName = substr($key, 0, 1).'id';
        $value = [];

        foreach($scheduling as $courses) {
            foreach($courses['courseLecturer'] as $courseLecturer) {
                if(!in_array( $courseLecturer[$idName], $$lecturers)) {
                    $value[$courseLecturer[$idName]] = $courseLecturer[$key];
                }
            }
        }
        return $value;
    }
    private function getCourseFromCourseLecturer($scheduling){
        return $this->getKeyFromCourseLecturer($scheduling, 'course');
    }
    private function getLecturerFromCourseLecturer($scheduling){
        return $this->getKeyFromCourseLecturer($scheduling, 'lecturer');

    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                              静态页面部分                                                  //
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * 蚂蚁金服课件静态页面
     * @author wonguohui
     * @Date   2016-03-10T13:39:03+0800
     */
    public function courseware()
    {
        include template('static', 'courseware');
    }
}

?>
