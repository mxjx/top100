<?php
pc_base::load_app_class( 'api', 'api' );
pc_base::load_app_class( "HttpService", 'api' );
pc_base::load_sys_class( 'param' );
pc_base::load_app_class( 'jssdk', 'api' );

class edm2018 extends api
{
	public $course = [
		'1' => [
			[
				'id'       => 13375,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/chanpin/likuan.png',
				'name'     => '李宽',
				'position' => '小米物流系统 高级产品经理',
				'title'    => '深度梳理B端产品经理完整工作流程及发展方向，帮你成为独当一面产品人',
				'content'  => '讲述了“单个产品管理流程”，以展示B 端产品经理的工作方法及B 端产品的设计方法。第一部分主要讲述的是单个产品管理流程，以及从事B 端产品经理的职业现状和规划，还包括设计B 端产品时需要了解的指导思想。第二部分讲述单个产品管理流程的框架，介绍B 端产品经理在工作时业务。第三部分是分享工作中的具体案例经验',
			],
			[
				'id'       => 13548,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/chanpin/tianyiming.png',
				'name'     => '田一鸣',
				'position' => '新浪微博 运营总监',
				'title'    => '微博如何实现头部用户规模连续三年快速增长，月阅读量过百亿的垂直领域达到25个',
				'content'  => '微博在2015年开始增速复苏、实现在互联网行业罕见的二次崛起。二次崛起的关键是运营战略的调整，其中最核心的工作是垂直开放运营。本案例将会分享，1)什么是垂直开放运营？2)怎么进行垂直开放运营？3)垂直开放运营带来了什么？',
			],
			[
				'id'       => 13427,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/chanpin/changlin.png',
				'name'     => '昌琳',
				'position' => '喜马拉雅 ued总监',
				'title'    => '随着用户数量的不断增加，如何高效处理进入app的流
量以及如何优化交互视觉效果',
				'content'  => '喜马拉雅最近几年在音频领域发展迅速，除了深耕声音内容之外，在产品体验上也做了很多优化。我们以喜马拉雅近期改版为例，讲述团队在体验设计和数据优化上的思考。',
			],
		],
		'2' => [
			[
				'id'       => 13379,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/tuandui/hemian.png',
				'name'     => '何勉',
				'position' => '阿里巴巴集团研发效能事业部资深技术专家',
				'title'    => "构建系统、快速的创新循环，打造先进的互联网产品交付和创新方法",
				'content'  => '为了最大化效能提升的实际成果，我们还必须回到产品和业务，将持续交付能力转化为有效的产品创新和业务成功。 本次分享将从持续交付和有效创新两个方面，阐述端到端的管理和技术创新实践，通过阿里巴巴天猫新零售、中间件、大文娱等产品的研发效能提升和创新实践，为你解读由理念、方法、实践和工具构成的有机系统',
			],
			[
				'id'       => 13397,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/tuandui/heyanhua.png',
				'name'     => '何燕华',
				'position' => '网易 项目经理',
				'title'    => '项目管理需要根据项目实际情况因时因地制宜，面对错综复杂的项目更是需要作出适当的调整和转变',
				'content'  => '在业务迅速发展，团队迅速扩充的情况下，我们遇到了诸多问题，例如需求无法完整交付，团队的沟通变得异常复杂，团队之间出现了信任危机。 此时的项目管理该如何破这个局？本次案例将详细为大家揭秘网易内部项目管理系统解决方案。',
			],
			[
				'id'       => 13470,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/tuandui/suni.png',
				'name'     => '苏妮',
				'position' => '招银网络科技 职员',
				'title'    => "如何在保证业务快速增长的同时关注工程师的成长，提升工程师幸福感并反哺到业务创新中",
				'content'  => "伴随招银网络科技的发展，组织规模迅速扩大，软件过程管理模式也从传统的CMMI转向精益敏捷，本案例分享通过“Best Brain”项目打造组织技术氛围的一些实践，让组织文化更开源、开放，培养了大批的技术分享者和实践者，促使团队更加融合，同时促进了公司产品研发和创新。",
			],
		],
		'3' => [
			[
				'id'       => 13498,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/jiagou/longmingkang.png',
				'name'     => "龙明康",
				'position' => '科大讯飞 云计算研究院副院长&首席架构师',
				'title'    => 'AI+IoT作为今年的爆款架构，讯飞以优势的AI为切入点构建AIoT，比传统IoT的构建更具挑战',
				'content'  => "主要分享AIoT是什么；在AIoT平台建设过程中，如何做架构分层；在设备接入层如何做协议的选择和优化；如何解决海量IoT设备连接问题；如何设计设备、数据复杂的访问权限策略；如何统一海量类别设备的差异化API；附带介绍我们当前的AI的架构，最后介绍我们是如何使用智能物描述抽象来衔接AI和IoT，并在语音智能家居场景中的落地。",
			],
			[
				'id'       => 13540,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/jiagou/Xin Hu.png',
				'name'     => "Xin Hu",
				'position' => 'LinkedIn 架构师',
				'title'    => '通过对基础设施的改善，对Feed serving stack的平台化改造，驱动产品迭代速度的提升',
				'content'  => "面对Feed这种核心业务，需要平台化大型复杂软件的核心组件，以扩大其演变，这意味着最大限度地重用现有组件和服务，而不是从头开始重复构建功能。“Platfomization”帮助我们加快了产品迭代，用技术杠杆撬动工程效能",
			],
			[
				'id'       => 13429,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/jiagou/zhangxiaolong.png',
				'name'     => "张小龙",
				'position' => '北京银行 核心系统架构设计',
				'title'    => '分布式架构帮助银行使硬件成本降低80%，高并发交易类系统处理能力提升500%',
				'content'  => '1、分布式NewSQL数据库与分布式应用体系建设初衷及历程；
2、行业内首发金融级NewSQL数据库评测体系；
3、全栈式Scale-out应用系统、数据库建设实践分享；
4、NewSQL数据库在金融级场景的应用实践分享；',
			],
		],
		'4' => [
			[
				'id'       => 13547,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/ai/chenyiming.png',
				'name'     => "陈一鸣",
				'position' => 'Amazon research science manager',
				'title'    => '如何通过spark 的运用去处理千万级别的商品建模',
				'content'  => "亚马逊第三方卖家平台自从2010 年登陆亚马逊后迅速壮大。至今已占亚马逊40%销量。但是大批的卖家质量参差不齐。其中侵权和欺诈的货品对客户伤害最大。商品存在多种欺诈行为：a.侵权 （版权和商标）b.欺诈 （拿单就撤）c.刷单我们需要通过模型来有效的将不良商品下架。我们团队主要解决2个难题：1.如何处理千万级的数据量 2.如何自动更新模型",
			],
			[
				'id'       => 13481,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/ai/zhaozhongzhou.png',
				'name'     => "赵中州",
				'position' => '阿里巴巴 小蜜 算法专家',
				'title'    => '前沿学术成果在适合的场景也能发挥重要价值，但在落地过程中必然面临一系列挑战。',
				'content'  => "阿里小蜜是阿里巴巴推出的围绕电商服务、导购以及任务助理为核心的智能人机交互产品，已经成为了阿里巴巴双十一期间服务的绝对主力。其所使用的问答技术也在经历着飞速的发展，近期将自然语言处理领域目前的学术热点——机器阅读理解技术成功运用在电商服务中，让机器具有如同人一般的阅读理解能力，使问答产品体现出真正的智能",
			],
			[
				'id'       => 13414,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/ai/luozhenxiao.png',
				'name'     => "罗震霄",
				'position' => 'Uber Engineering Manager, Big Data Department',
				'title'    => "与软件工程师，数据科学家和机器学习工程师共享大数据和深度学习架构体验",
				'content'  => "本演讲将分享优步的工程工作，我们将从我们的大数据和深度学习基础架构开始，特别是Tensorflow，Hadoop和Presto。然后我们将讨论优步如何使用Tensorflow作为深度学习引擎，以及Presto作为交互式SQL引擎。我们将重点介绍Uber如何从头开始构建Presto Tensorflow Connector，以支持深度学习的实时分析",
			],
		],
		'5' => [
			[
				'id'       => 13520,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/ceshi/wangchu.png',
				'name'     => "王楚",
				'position' => '中兴通讯测试开发工程师',
				'title'    => "技术改变一切，人工智能在企业中的探索和应用正在不断改变企业的生产模式和不同职位的工作方式",
				'content'  => "本案例引入了机器学习的算法和插桩技术，解决以下三个痛点:
1)模糊测试规则是人工增加，规则衍生慢，规则的 防护网范围、疏密程度无法衡量
2)用例爆炸：规则交叉、用例指数级增长，过亿的用例，执行时间长
3)无法度量模糊测试的深度和广度，绝少有效的依据支撑测试决策",
			],
			[
				'id'       => 13399,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/ceshi/lichaofeng.png',
				'name'     => "李超峰",
				'position' => '华为 高级工程师',
				'title'    => "各专项测试总体自动化率超过93%、通过率99.7%，支持团队迭代交付周期从四周缩短到一周",
				'content'  => "本案例重点讲述如何构建专业的测试平台来向测试人员提供全方位的测试服务能力，屏蔽测试人员无需关注的过多细节，降低复杂系统的测试开展难度，使得测试人员聚焦于测试活动本身，从而提升测试效率和质量，实现以效率提升带动质量改进",
			],
			[
				'id'       => 13512,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/ceshi/Scott McMaster.png',
				'name'     => "Scott McMaster",
				'position' => 'Grab Engineering Lead',
				'title'    => '讨论使用微服务架构时的单元和集成测试的各种技术',
				'content'  => "传统上Web应用程序是用单一编程语言开发的，代码是作为一个“单片”单元构建，部署和扩展的。微服务架构允许团队独立开发和部署更易于理解且更安全的小型软件服务。这些较小的服务不是通过代码编译而是通过在运行时发送远程过程调用来相互集成。因此，微服务架构具有更多的运行时依赖性和互连。这些互连为软件测试带来了新的挑战",
			],
		],
		'6' => [
			[
				'id'       => 13475,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/yunwei/hajingjing.png',
				'name'     => "哈晶晶",
				'position' => '百度 资深架构师',
				'title'    => "智能运维所带来的价值逐渐彰显。而百度云开创了国内智能运维从0到1的先河",
				'content'  => '百度运维经历了脚本&工具、自动化运维平台、开放运维平台阶段，在2014年开始智能化运维的探索，并且围绕可用性、成本和效率方向的运维目标在诸多运维场景落地。本次分享将以百度故障处理场景为例，介绍百度故障预防、故障发现、故障诊断和故障止损阶段的AIOps实践经验，同时也会分享百度成熟的智能运维产品和解决方案。',
			],
			[
				'id'       => 13479,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/yunwei/zhaocheng.png',
				'name'     => "赵成",
				'position' => '美丽联合 技术总监',
				'title'    => '当基础设置发生变化，当业务发展速度变快好几百倍时，如何在这种变化中体现运维价值',
				'content'  => '随着运维自动化和效率体系的不断完善，运维关注的重点和面临的挑战更多的聚焦在稳定性层面。近两年，随着Google SRE理念的传播和落地，对于稳定性体系建设起到了非常好的引导示范作用。 本议题主要分享，蘑菇街在SRE实践方面的经验，以及蘑菇街业务整体搬迁上公有云之后，与云厂商之间的CRE体系建设实践。',
			],
			[
				'id'       => 13511,
				'img'      => 'http://www.top100summit.com/statics/2018/top100tag/yunwei/tanghongshan.png',
				'name'     => "唐洪山",
				'position' => '京东金融 研发支持团队负责人',
				'title'    => "如何能够在工程效率提升的前提下，保证产品的质量，减少故障？",
				'content'  => "本次案例演讲京东金融将从建立研发协同平台进行高效研发、到微服务、CI/CD之分层自动化，自动化测试等方面帮助大家从理念、策略、实践、效果反馈等方面，全面深入的了解DevOps玩法，以及具体如何落地的思路。",
			],
		],
	];

	public function edm2018v1 ()
	{
		include template( 'edm', '2018edm' );
	}


	public function top100v1 ()
	{
		$imgPath = 'http://' . $_SERVER[SERVER_NAME] . '/statics/images/17edm';
		$con = ['mbc_listId' => 4];
		$request = [
			'mm' => 'bangdankecheng',
			'mw' => $con,
			'mp' => ''
		];

		$return_entry = $this->curl->curl_action( '/Top100/top100-api/index', $request );
		$data = $return_entry['data'];
		$lecturer = $this->getMembers();

		include template( 'edm', '2018top100' );
	}

	public function top100v2 ()
	{
		$imgPath = 'http://' . $_SERVER[SERVER_NAME] . '/statics/images/17edm';
		include template( 'edm', '2017top100v2' );
	}

	public function top100v3 ()
	{
		$imgPath = 'http://' . $_SERVER[SERVER_NAME] . '/statics/images/17edm/yaxin';
		include template( 'edm', '2017top100v3' );
	}

	public function top100v4 ()
	{
		$imgPath = 'http://' . $_SERVER[SERVER_NAME] . '/statics/images/17edm/v4';
		$data = $this->getCaseFromServer();
		include template( 'edm', '2017top100v4' );
	}

	public function top100v5 ()
	{
		$imgPath = 'http://' . $_SERVER[SERVER_NAME] . '/statics/images/17edm/v5';
		$clear = false;
		if ( isset( $_GET['clear'] ) ) {
			$clear = true;
		}
		$data = $this->getCaseFromServer( $clear );
		$producer = $this->getProducer();
		include template( 'edm', '2017top100v5' );
	}

	public function getCourseByTag ( $tag )
	{
		$con = [
			'mbc_listId'    => 4,
			'mbc_courseTag' => $tag
		];
		$request = [
			'mm' => 'bangdankecheng',
			'mw' => $con,
			'mp' => ''
		];
		$return_entry = $this->curl->curl_action( '/Top100/top100-api/index', $request );
		$data = $return_entry['data'];
		return $data;
	}

	public function tagv1 ()
	{
		$imgFile = 'http://www.top100summit.com/statics/2018/top100tag';
		$data = $this->getCourseByTag( 1 );
		$title = '产品创新/体验设计/运营增长';
		$companys = '来自阿里巴巴、网易、招银网络、微博、喜茶、有赞、GitLab、滴滴出行、喜马拉雅、美团、腾讯等国内外一线资深产品大咖的实战分享';
		$courses = $this->course['1'];
		include template( 'edm', '2018tag' );

	}

	public function tagv2 ()
	{
		$imgFile = 'http://www.top100summit.com/statics/2018/top100tag';
		$data = $this->getCourseByTag( 2 );
		$title = '组织发展/研发效能/团队管理';
		$companys = '来自平安银行、阿里巴巴、广联达、用友、普元、中兴、华为、美团旅行、诺基亚、好买等国内外一线团队带头人的实战分享';
		$courses = $this->course['2'];
		include template( 'edm', '2018tag' );
	}

	public function tagv3 ()
	{
		$imgFile = 'http://www.top100summit.com/statics/2018/top100tag';
		$data = $this->getCourseByTag( 3 );
		$title = '爆款架构/数据平台/工程实践';
		$companys = '来自Uber、LinkedIn、科大讯飞、微博、闲鱼、知乎、北京银行、饿了么等国内外一线团队带头人技术大咖的实战分享';
		$courses = $this->course['3'];
		include template( 'edm', '2018tag' );
	}

	public function tagv4 ()
	{
		$imgFile = 'http://www.top100summit.com/statics/2018/top100tag';
		$data = $this->getCourseByTag( 4 );
		$title = '人工智能/AI驱动/AI实践';
		$companys = '来自中国科学院、亚马逊、LG、阿里小蜜、第四范式、360、爱奇艺、蚂蚁金服等国内外一线团队技术带头人的实战分享';
		$courses = $this->course['4'];
		include template( 'edm', '2018tag' );
	}

	public function tagv5 ()
	{
		$imgFile = 'http://www.top100summit.com/statics/2018/top100tag';
		$data = $this->getCourseByTag( 5 );
		$title = '测试实践/测试工具链建设/大前端&移动端';
		$companys = '来自阿里妈妈、中兴、Grab、eBay、去哪儿、中国银行、陌陌、猫眼、阿里云、UC、中国移动等国内外一线资深技术大咖的实战分享';
		$courses = $this->course['5'];
		include template( 'edm', '2018tag' );
	}

	public function tagv6 ()
	{
		$imgFile = 'http://www.top100summit.com/statics/2018/top100tag';
		$data = $this->getCourseByTag( 6 );
		$title = '运维体系/AIOps&DevOps/区块链';
		$companys = '来自三七互娱、虎牙、Salesforce、京东金融、腾讯、蘑菇街、平安壹钱包等国内外一线团队带头人技术大咖的实战分享';
		$courses = $this->course['6'];
		include template( 'edm', '2018tag' );
	}


	private function getCaseFromServer ( $clear = false )
	{
		if ( !isset( $_SESSION ) ) {
			session_start();
		}
		if ( $clear ) {
			$_SESSION['edmv4'] = '';
		}
		if ( !empty( $_SESSION['edmv4'] ) ) {
			$data = $_SESSION['edmv4'];
		} else {
			$con = ['mbc_listId' => 3];
			$request = [
				'mm' => 'bangdankecheng',
				'mw' => $con,
				'mp' => ''
			];

			$return_entry = $this->curl->curl_action( '/Top100/top100-api/index', $request );
			$data = $return_entry['data'];
			foreach ( $data as $k => $v ) {
				if ( !empty( $v['companyThumbs'] ) ) {
					$companyThumbs = json_decode( $v['companyThumbs'], true );
					$data[$k]['companyThumbs'] = $companyThumbs[0]['fileUrl'];
					$data[$k]['lecturer'] = $v['courseLecturer'][0]['lecturer'];
				}
				if ( $data[$k]['lecturer']['name'] == '柳忠伟' ) {
					$data[$k]['lecturer']['position'] = '58到家技术平台部负责人';
				}
				if ( $data[$k]['lecturer']['name'] == '罗琼' ) {
					$data[$k]['lecturerCompany'] = '中国移动';
				}
				if ( $data[$k]['lecturer']['name'] == '茹忆' ) {
					$data[$k]['lecturer']['position'] = '人工智能实验室负责人';
				}
				if ( $data[$k]['lecturer']['name'] == '李双涛' ) {
					$data[$k]['lecturer']['position'] = '中间件团队首席架构师';
				}
				if ( $data[$k]['lecturer']['name'] == '陈宗志' ) {
					$data[$k]['lecturer']['position'] = 'web平台部基础架构技术经理';
				}
				if ( $data[$k]['lecturer']['name'] == '田霞' ) {
					$data[$k]['lecturerCompany'] = '畅捷通信息';
					$data[$k]['lecturer']['position'] = 'web平台部基础架构技术经理';
				}
				if ( $data[$k]['lecturer']['name'] == '刘杰' ) {
					$data[$k]['lecturer']['position'] = '大数据研究院平台研发主管';
				}
				if ( $data[$k]['lecturer']['name'] == '陈晓畅' ) {
					$data[$k]['lecturerCompany'] = '腾讯';
					$data[$k]['lecturer']['position'] = '设计中心总监';
				}
				if ( $data[$k]['lecturer']['name'] == '张晶华' ) {
					$data[$k]['lecturer']['position'] = '产品设计研究高级经理';
				}
				if ( $data[$k]['lecturer']['name'] == '熊志男' ) {
					$data[$k]['lecturerCompany'] = '京东商城';
				}
				if ( $data[$k]['lecturer']['name'] == '蔡震坤' ) {
					$data[$k]['lecturerCompany'] = '平安证券';
				}
				if ( $data[$k]['lecturer']['name'] == '李成银' ) {
					$data[$k]['lecturer']['position'] = '高级技术经理';
				}
			}
			$data = array_reverse( $data );
			$_SESSION['edmv4'] = $data;
		}
		return $data;
	}

	private function getProducer ()
	{
		$request = [
			'mm' => 'chupinren',
			'mw' => 'mpr_venue != 0 ',
			'mr' => [
				'huiyuanxinxi' => [
					'mm' => 'huiyuanxinxi'
				]
			]
		];
		$return = $this->curl->curl_action( 'api/index', $request );
		$list = $return['data'];
		$len = count( $list );
		$i = 0;
		foreach ( $list as $k => $v ) {
			if ( $v['year'] == '2017' ) {
				$result[$i]['id'] = $v['id'];
				$result[$i]["user_id"] = $v["user_id"];
				$result[$i]["venue"] = $v["venue"];
				$result[$i]["venueName"] = $v["venueName"];
				$result[$i]["year"] = $v["year"];

				$info = $v["memberInfo"];
				if ( $info['name'] == '郑宇' || $info['name'] == '耿杰森' || $info['name'] == '张溪梦' ) {
					continue;
				}
				// if($info['name'] == '耿杰森'){
				// 	$info['company'] = '数据应用学院';
				// }
				if ( $info['name'] == '钱蓓蕾' ) {
					$info['position'] = '测试总监';
				}
				if ( $info['name'] == '吴亮（月影）' ) {
					$info['position'] = '副总监、360技术委员会委员';
				}
				if ( $info['name'] == '唐沐' ) {
					$info['position'] = '小米影业总裁';
				}
				$result[$i]["name"] = $info['name'] ?: '';
				$result[$i]["company"] = $info['company'] ?: '';
				$result[$i]["position"] = $info['position'] ?: '';
				$thumb = $info['thumbs'] ? json_decode( $info['thumbs'] )[0]->fileUrl : 'https://mrm.msup.com.cn/Public/Admin/images/default_headpic.png';
				$result[$i]["thumb"] = $thumb;
				$desc = $v["description"] ?: $info["description"];
				if ( mb_strlen( $desc, 'UTF8' ) > 110 ) {
					$desc = mb_substr( $desc, 0, 110, "utf-8" ) . '...';
				}
				$result[$i]["description"] = $desc ?: '';
				$i++;
			} else if ( $v['year'] != '' && $v['year'] != 0 ) {
				$before[] = $v;
			}
		}
		return $result;
	}

	public function getMembers ()
	{
		return [
			[
				[
					'img'     => 'http://f.msup.com.cn/%E5%B4%94%E5%AE%9D%E7%A7%8B.png',
					'name'    => '崔宝秋',
					'content' => '小米首席架构师<br />人工智能与云平台副总裁',
				],
				[
					'img'     => 'http://f.msup.com.cn/%E7%8E%8B%E6%B6%9B.png',
					'name'    => '王涛',
					'content' => '爱奇艺高级科学家',
				],
				[
					'img'     => 'http://f.msup.com.cn/%E9%99%88%E7%A6%8F%E4%BA%91.png',
					'name'    => '陈福云',
					'content' => '新浪微博用户运营总经理',
				],
			],
			[
				[
					'img'     => 'http://f.msup.com.cn/%E9%99%88%E5%A6%8D.png',
					'name'    => '陈妍',
					'content' => '腾讯CDC总经理',
				],
				[
					'img'     => 'http://f.msup.com.cn/%E6%9D%8E%E6%B6%9B.png',
					'name'    => '李涛',
					'content' => '百度工程效率部总监',
				],
				[
					'img'     => 'http://f.msup.com.cn/%E6%9D%8E%E8%99%93.png',
					'name'    => '李虓',
					'content' => 'LinkedIn Director of SRE',
				],
			],
			[
				[
					'img'     => 'http://f.msup.com.cn/%E5%88%98%E6%B1%9F.png',
					'name'    => '刘江',
					'content' => '美团点评技术学院院长',
				],
				[
					'img'     => 'http://f.msup.com.cn/%E5%88%98%E5%BD%A6%E4%B8%9C.png',
					'name'    => '刘彦东',
					'content' => '网易传媒技术副总经理',
				],
				[
					'img'     => 'http://f.msup.com.cn/%E7%BF%81%E5%BF%97.png',
					'name'    => '翁志',
					'content' => '京东副总裁，AI与大数据负责人',
				]
			]
		];
	}

	public function caseSubmitEdm2018 ()
	{
		$return_data = $this->getPaikData( 1155 );
		//var_dump($return_data);die;
		$time = $return_data[0];
		$end_time = $return_data[1];
		$data_new = $return_data[2];
		include template( 'edm', 'caseSubmitEdm2018' );
	}

	public function getPaikData ( $msvc_sid = 127 )
	{
		$request = [
			'mm' => 'huichangkecheng',
			'mw' => ["msvc_sid" => $msvc_sid],
			'ms' => '*',
		];
		$return = $this->curl->curl_action( '/Top100/top100-api/scheduling', $request );
		$data = $return['data'];

		// p($data);
		$len = count( $data );
		for ( $i = 0; $i < $len; $i++ ) {
			$data[$i]['date'] = date( 'Y-m-d', $data[$i]['date'] );
			$data_new[$data[$i]['date']][$data[$i]['startTime']][] = $data[$i];
			$start_time[] = $data[$i]['startTime'];
			$end_time[] = $data[$i]['endTime'];
		}

		$time = array_unique( $start_time );
		$data_new = array_values( $data_new );
		//来该换数据顺序  来适配相对应的场次
		sort( $time );
		//日期
		for ( $i = 0; $i < count( $data_new ); $i++ ) {
			//时刻
			for ( $k = 0; $k < count( $data_new[$i] ); $k++ ) {
				$data = [];
				$map = ['0' => '产品创新', '1' => '团队管理', '2' => '架构设计', '3' => '工程实践', '4' => '测试管理', '5' => '运维发布'];
				// p($data_new[$i][$time[$k]]);
				$temp = [];
				foreach ( $map as $mk => $mv ) {
					foreach ( $data_new[$i][$time[$k]] as $kk => $vv ) {
						if ( $mv == $vv['venueName'] ) {
							// p($data_new[$i][$time[$k]][$kk]);
							$temp[$mk] = $data_new[$i][$time[$k]][$kk];
							// p($temp);
							//$data[] = $temp;

						}
					}

				}
				$data_new[$i][$time[$k]] = $temp;
			}
		}
		$end_time = array_unique( $end_time );
		sort( $end_time );
		$shuz = array_unique( $data_new[0][$time[1]] );
		$return_data[0] = $time;
		$return_data[1] = $end_time;
		$return_data[2] = $data_new;
		return $return_data;
	}

	public function foreign ()
	{

		$con = [
			'mbc_courseid' => [
				13574,
				13632,
				13617,
				13612,
				13558,
				13543,
				13483,
				13435,
				13512,
				13374,
				13602,
				13581,
				13570,
				13542,
				13414,
				13547,
				13504,
				13544,
				13539,
				13628,
				13545,
				13537,
				13633,
				13603,
			]
		];

		$request = [
			'mm' => 'bangdankecheng',
			'mw' => $con,
			'mp' => ''
		];

		$return_entry = $this->curl->curl_action( '/Top100/top100-api/index', $request );
		$data = $return_entry['data'];
//
//		echo '<pre>';
//		var_dump( $data );
//		die;

		include template( 'edm', 'foreign' );
	}

}

?>