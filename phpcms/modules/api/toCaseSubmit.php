<?php
pc_base::load_app_class('api', 'api');
class toCaseSubmit  extends api {
    const CURRENT_LISTID = 4;
	public function __construct() {
		$this->curl = new curl();
        $this->pageSize = 9;
	}

	public function init(){
		include template('api', 'submit');
	}
	public function caseSubmitEntryAct()
	{
		$typeLabel = $this->getHtmlTags2017();
		include template('api', 'caseSubmitEntry2018-act');
	}
	public function caseSubmitEntry()
	{
        $typeLabel = $this->getHtmlTags2017();
        include template('api', 'caseSubmitEntry2017');
//        //入围所有数据
//        $caseAll = $this->getCaseSubmitEntryData();
//        //入围案例的总数量
//        $caseNums = count($caseAll);
//        //榜单数据  20161101去掉榜单
//       // $caseList = $this->sortColumn($caseAll,'praises');
//
//        $htmlTags = $this->getHtmlTags();
//        include template('api', 'caseSubmitEntry');
	}
    // 获取分页 offset的值
    public function getPageStart(){

        $page = empty($_GET['page'])? 1 : $_GET['page'];
        return  ($page-1)*$this->pageSize;
    }
    public function search()
    {
        $tag = addslashes($_POST['tag']);
        $typeLabel = $this->getHtmlTags2017();
        //根据tag 搜索案例数据
        $start = $this->getPageStart();
        $caseArr = $this->getSearchCaseSubmitTagData($tag,$start);
//        echo '<pre>';
//        var_dump($caseArr);die;
        if (!empty($caseArr)) {
            foreach ($caseArr as $k => $v) {
                $data[$k]['courseId'] = $v['courseid'];
                $data[$k]['title'] = $v['title'];
                $data[$k]['desc'] = $v['courseDescription'] ? mb_substr($v['courseDescription'],0,48,"utf-8").'...' : '';
                $data[$k]['thumb'] = json_decode($v['companyThumbs'])[0] ?  json_decode($v['companyThumbs'])[0]->fileUrl : '';
                $data[$k]['courseTag'] = $v['courseTag'];
                $data[$k]['typeLabel'] = $typeLabel[$v['courseTag']];
             }
            echo json_encode($data);
        }

        echo false;
    }
    public function test()
    {
        $typeLabel = $this->getHtmlTags2017();
        include template('api', 'caseSubmitEntry2017');
    }
    public function getHtmlTags2017(){
        $typeLabel = [
            '0' => '总榜',
            '1' => "产品创新/体验设计/运营增长",
            '2' => "组织发展/研发效能/团队管理",
            '3' => "爆款架构/数据平台/工程实践",
            '4' => "人工智能/AI驱动/AI实践",
            '5' => "测试实践/测试工具链建设/大前端&移动端",
            '6' => "运维体系/AIOps&DevOps/区块链"
        ];
        return $typeLabel;
    }
    public function getCaseSubmitEntryNum(){
        return count($this->getCaseSubmitEntryData());
    }
    public function getCaseSubmitEntryData(){
        $request = [
            'mm' => 'bangdankecheng',
            'mw' => ['mbc_listId' => self::CURRENT_LISTID]
        ];
        $return_entry = $this->curl->curl_action('/Top100/top100-api/index',$request);
        return $return_entry['data'];
    }
    //通过搜索tag获取案例信息
    public function getSearchCaseSubmitTagData($tag = 0,$start){
        $con = ['mbc_listId' =>  self::CURRENT_LISTID];
        if(!empty($tag)){ //0为全部内容
            $con['mbc_courseTag'] = $tag;
        }
        $request = [
            'mm' => 'bangdankecheng',
            'mw' => $con
        ];
        $request['mo'] = $start;
        $request['ml'] = $this->pageSize;

        $return_entry = $this->curl->curl_action('/Top100/top100-api/index',$request);
        return $return_entry['data'] ? : null;
    }
    public function getHtmlTags(){
        return [
            '1' => ['label' => '产品创新/用户体验','img' => 'role_1.png'],
            '2' => ['label' => '工程文化/团队增长','img' => 'role_2.png'],
            '3' => ['label' => '架构设计/开发流程','img' => 'role_3.png'],
            '4' => ['label' => '数据驱动/机器学习','img' => 'role_4.png'],
            '5' => ['label' => '测试管理/交付运维','img' => 'role_5.png']
        ];
    }
public function ChangeString($str)
{
//str含有HTML标签的文本
    $str = str_replace("<","&lt;",$str);
    $str = str_replace(">","&gt;",$str);
    $str = str_replace(" ","&nbsp;",$str);
    $str = str_replace("\n","<br>",$str);
    $str = str_replace("&","&amp;",$str);
   return $str;
}
    public function getcaseSubmitEntry(){
        $num = $_POST['num'];
        $title = $_POST['title'];
        for ($i=1; $i <= 5 ; $i++) {
            unset($arr);
            $request = [
                'mm' => 'bangdankecheng',
                'mw' => [
                    'mbc_listId' =>  self::CURRENT_LISTID,
                    'mbc_courseTag'=>[$i],
                    'mbc_title' => $title,
                ],
                'mo' => 6*$num,
                'ml' => '6'
            ];
            $return_entry = $this->curl->curl_action('/Top100/top100-api/index',$request);

            $arr[] = $this->prepareValue($return_entry['data']);
              if($i == 1){
                $chanpin = $arr;
            }else if($i == 2){
                $tuandui = $arr;
            }else if($i == 3){
                $jiagou = $arr;
            }else if($i == 4){
                $kaifa = $arr;
            }else if($i == 5){
                $ceshi = $arr;
            }
        }
        //产品  团队 架构 开发 测试
        $data[0] =  $chanpin;
        $data[1] =  $tuandui;
        $data[2] =  $jiagou;
        $data[3] =  $kaifa;
        $data[4] =  $ceshi;
        echo json_encode($data);
    }
    function prepareValue($row){
        foreach($row as &$v) {
            $v['hits'] = viewed_num($v['created_at'], $v['hits']);
        }
        return $row;
    }

    /**
     * 将二维数组按某个字段排序
     * @param $list   排序数组
     * @param string $col   排序字段
     * @param string $sortTyle 排序类型
     */
    function sortColumn($list,$col = 'id',$sortTyle = SORT_DESC){
        foreach($list as $k=>$v) $data[$k]=$v[$col];
        array_multisort($data,$sortTyle,$list);
        return $list;
    }
    //对二维数据进行排序的算法
    function sksort(&$array, $subkey="id", $sort_ascending=false) {
    if (count($array))
        $temp_array[key($array)] = array_shift($array);
    foreach($array as $key => $val){
        $offset = 0;
        $found = false;
        foreach($temp_array as $tmp_key => $tmp_val)
        {
            if(!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey]))
            {
                $temp_array = array_merge(    (array)array_slice($temp_array,0,$offset),
                                            array($key => $val),
                                            array_slice($temp_array,$offset)
                                          );
                $found = true;
            }
            $offset++;
        }
        if(!$found) $temp_array = array_merge($temp_array, array($key => $val));
    }

    if ($sort_ascending) $array = array_reverse($temp_array);

    else $array = $temp_array;
}

      //这个是上榜的 更多的数据
	// public function caseSubmitEntry_more()
	// {  
 //        $type = $_GET['type'];
 //        if($type == 'list'){
          
 //            $request = [
 //                'mm' => 'anlitijiao',
 //                'ms' => '*',
 //                'mr' => [
 //                'huiyuanxinxi' => [
 //                            'mm' => 'huiyuanxinxi',
 //                            'ms' => 'mmi_name,mmi_userId,mmi_company,mmi_position,mmi_thumbs'
 //                        ]
 //                ]
 //            ];
 //            $return = $this->curl->curl_action('api/index',$request);
 //            $casesubmits = $return['data'];
 //        }
	// 	include template('api', 'caseSubmitEntryMore');
	// }
    //这个是入围的 更多的数据
	public function caseSubmitFinalists($value='')
    {
         $request = [
            'mm' => 'bangdankecheng',
            'mw' => ['mbc_listId' => self::CURRENT_LISTID]
        ];
        $return_entry = $this->curl->curl_action('/Top100/top100-api/index',$request);
        $casesubmits_entry = $return_entry['data'];
        $casesubmits_entry = $this->prepareValue($casesubmits_entry);
        include template('api', 'caseSubmitFinalists');
    }
    
}

?>
