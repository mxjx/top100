<?php
pc_base::load_app_class('api', 'api');
class member extends api {
	public function __construct() {
		$this->curl = new curl();
	}

	public function init(){
		include template('content', 'member');
	}
	public function register(){
		include template('content', 'member_register');
	}
	public function add($value='')
	{
		$data = $_POST['data'];
		$dataArr = explode("##",$data);
		$request = [
			'mm' => 'yuyue',
			'mrq' => [
				'map_company' => $dataArr[0],
				'map_industry' => $dataArr[1],
				'map_name' => $dataArr[2],
				'map_phone' => $dataArr[3],
				'map_email' => $dataArr[4],
				'map_position'=> $dataArr[5],
				'map_member_type' => $dataArr[6],
				'map_type' => '3',
				'map_source' => "top100官网",
				'map_description' => '企业会员'
			]

		];
		$return_entry = $this->curl->curl_action('/user-api/create-appoint',$request);
		$result = $return_entry['data'];
		if($result["appid"]){
			$message = "申请成功";
			$status = 200;
		}else{
			$message = "申请失败";
			$status = 0;
		}
		$res['status'] = $status;
		$res['message'] = $message;
		$res['data'] = "";
		echo json_encode($res);
	}
}
?>
