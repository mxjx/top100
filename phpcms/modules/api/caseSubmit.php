<?php
pc_base::load_app_class('api', 'api');
pc_base::load_app_class('common', 'api');
class caseSubmit  extends api{
    public $pageSize = 20;
    public function init(){
        if($userId = $this->common->getUserId()){
            $userinfos = $this->common->getUserInfoById($userId);
            $username = $this->common->getUserName();
        }
        include template('api', 'caseSubmit2017');
    }
    public function test(){
        if($userId = $this->common->getUserId()){
            $userinfos = $this->common->getUserInfoById($userId);
            $username = $this->common->getUserName();
        }
        include template('api', 'caseSubmit2017');
    }
    //案例下载文件页面
    public function upload(){
        include template('api', 'caseSubmitEmail');
    }
     
    public function upload_file($img,$upload_type)
    {
        $tmp_name = $img['tmp_name'];  // 文件上传后得临时文件名
        $name     = $img['name'];     // 被上传文件的名称
        $size     = $img['size'];    //  被上传文件的大小
        $type     = $img['type'];  
         // 被上传文件的类型  
        // if($upload_type == 'img'){
        //      $type_allow=array("image/png", "image/gif", 'image/jpeg','image/pjpeg');
        //     if(!in_array($type,$type_allow)){
        //         showmessage("请上传正确的图片格式(jpg,png,jpeg)");
        //     }
        // }else 
        $type = explode(".",$name);
        $type = $type[count($type)-1];
        $type = strtolower($type);
        if($upload_type == 'ppt'){
            $type_allow=array('ppt','pptx','pdf');
            if(!in_array($type,$type_allow)){
                showmessage("请上传正确的文件格式(ppt,pptx,pdf)");
            }
        }else if($upload_type == 'doc'){
            $type_allow=array('doc','txt');
            if(!in_array($type,$type_allow)){
                showmessage("请上传正确的文件格式(doc,txt)");
            }
        }
       
        $dir      = 'uploadfile/'.date("Y").'/'.date('md');
      
        @chmod($dir,0777);//赋予权限
        @is_dir($dir) or mkdir($dir,0777);
        $newFileName = $dir.'/'.$this->getRandChar(7).'.'.$type;
        move_uploaded_file($img['tmp_name'],$newFileName);
        return $newFileName;
    }

    public function caseSubmitData()
    {
        if(empty($_POST['src'])){
            showmessage('分享者照片不能为空');
        }
        $arr = $_POST;
        $MemberFace = $this->sliceBanner($arr);
        $_POST['anlitijiao']['mcs_userid'] = $this->user_id;
        // 保存提交人头像
        $_POST['anlitijiao']['mcs_lecturerThumbs'] = $this->formatUploadImg($MemberFace,$MemberFace);
        // 保存公司 LOGO
        if(!empty($_FILES['mcs_companyThumbs']['name'])){
            $mcs_companyThumbs = $this->upload_file($_FILES['mcs_companyThumbs'],'img'); 
            $_POST['anlitijiao']['mcs_companyThumbs'] = $this->formatUploadImg($mcs_companyThumbs, $_FILES['mcs_companyThumbs']['name']);
        }
        //保存案例文稿
        if(!empty($_FILES['caseWord']['name'])){
            $_POST['anlitijiao']['mcs_caseWord'] = $this->UpToQiniu($_FILES['caseWord']); 
        }
        //保存PPT稿件
         if(!empty($_FILES['casePPT']['name'])){
            $_POST['anlitijiao']['mcs_casePPT'] = $this->UpToQiniu($_FILES['casePPT']);
        }
        if($_POST['anlitijiao']['mcs_grantFullName'] == '注明是您本人还是您的团队'){
            $_POST['anlitijiao']['mcs_grantFullName'] = '';
        }
        if($_POST['anlitijiao']['mcs_shareInOtherMeeting'] == '如果曾经分享过，请注明哪个会议以及反馈分数'){
            $_POST['anlitijiao']['mcs_shareInOtherMeeting'] = '';
        }
         if($_POST['anlitijiao']['mcs_courseDescription'] == '提示：200字~300字的案例启示或者案例简述'){
            $_POST['anlitijiao']['mcs_courseDescription'] = '';
        }
        $request = [
            'mm' => 'huiyuanxinxi',
            'mw' => ['mmi_userId'=>$_POST['anlitijiao']['mcs_userid']],
            'ms' => '*',
        ];
        $userinfo = $this->curl->curl_action('api/index',$request);
        $_POST['anlitijiao']['mcs_lecturerName'] = $userinfo['data'][0]['name'];
        $_POST['anlitijiao']['mcs_lecturerPosition'] = $userinfo['data'][0]['position'];
        $return = $this->curl->curl_action('user-api/case-submit', ['anlitijiao'=>$_POST['anlitijiao']], 'post');
        if ($return['errno'] == 0) {
            showmessage('案例提交成功','/index.php?m=api&c=myCaseList&currPage=1');
        } else {
            showmessage($return['errmsg']);

        }    
    }
        //案例的word 和 ppt 上传到七牛
    public function UpToQiniu($file){
        require_once("Qiniu/io.php");
        require_once("Qiniu/rs.php");
        $bucket = "buzz-ppt-pic";
        $accessKey   = 'oNwsF7L_XxAe0ZfRjH6U374UsGGnn6Ovzsx-GGVV';
        $secretKey   = 'q2NR9UTmY8by1wiHwtQ10cL-DHtRbtfKUfETJNyx';

        Qiniu_SetKeys($accessKey, $secretKey);
        $putPolicy = new Qiniu_RS_PutPolicy($bucket);
        $upToken = $putPolicy->Token();
        $putExtra = new Qiniu_PutExtra();
        $putExtra->Crc32 = 1;
        //拼接11位长度的文件名
        $file_Name = time().rand(1000,9999).".".end(explode('.', $file['name']));
        list($ret, $err) = Qiniu_PutFile($upToken,$file_Name,$file['tmp_name'], $putExtra);
        if ($err !== null) {
            showmessage('案例上传失败');
        } else {
            $fileUrl = "http://source.buzz.cn/".$ret['key'];
             return json_encode(
                [
                    [
                        'fileName' => $file['name'],
                        'fileUrl'  => $fileUrl,
                        'thumbnailUrl' => "",
                    ]
                ]
            );
        }
    }
    public function getRandChar($length) {
        return random($length, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz');
    }

    public function formatUploadImg($fileUrl, $fileName = '', $thumbnailUrl ='') {
        if (!$fileName) $fileName = $this->getRandChar(8);
        return json_encode(
                [
                    [
                        'fileName' => $fileName,
                        'fileUrl'  => APP_PATH.$fileUrl,
                        'thumbnailUrl' => $thumbnailUrl,
                    ]
                ]
            );
    }
    public function sliceBanner($arr){
        $x = (int)$arr['x'];
        $y = (int)$arr['y'];
        $w = (int)$arr['w'];
        $h = (int)$arr['h'];
        $pic = $arr['src'];
        //剪切后小图片的名字
        $str = explode(".",$pic);//图片的格式
        $type = $str[1]; //图片的格式
        $filename = $this->getRandChar(7).".". $type; //重新生成图片的名字
        $uploadBanner = $pic;
        //创建文件夹
        $dir ='uploadfile/'.date("Y").'/'.date('md');
        @chmod($dir,0777);//赋予权限
        @is_dir($dir) or mkdir($dir,0777);
        $sliceBanner = $dir."/".$filename;
        //创建图片
        $src_pic = $this->getImageHander($uploadBanner);
        $dst_pic = imagecreatetruecolor($w, $h);
        imagecopyresampled($dst_pic,$src_pic,0,0,$x,$y,$w,$h,$w,$h);
        imagejpeg($dst_pic, $sliceBanner);
        imagedestroy(APP_PATH.$src_pic);
        imagedestroy(APP_PATH.$dst_pic);
        //删除已上传未裁切的图片
        if(is_file($_SERVER['DOCUMENT_ROOT'].$pic)){
            unlink($_SERVER['DOCUMENT_ROOT'].$pic);
        }
        //返回新图片的位置
        return $sliceBanner;
    }
    //初始化图片
    public function getImageHander ($url) {
        $url = APP_PATH.$url;
        $size=@getimagesize($url);

        switch($size['mime']){
            case 'image/jpeg': $im = imagecreatefromjpeg($url);break;
            case 'image/gif' : $im = imagecreatefromgif($url);break;
            case 'image/png' : $im = imagecreatefrompng($url);break;
            default: $im=false;break;
        }
        return $im;
    }
    public function caseSubmitDataUpdate(){
        $_POST['anlitijiao']['mcs_id'] = $_POST['case_id'] ;
         // 保存提交人头像
        if($_FILES['mcs_lecturerThumbs']['name'] != "" || $_FILES['mcs_lecturerThumbs']['name'] != null){
            $mcs_lecturerThumbs = $this->upload_file($_FILES['mcs_lecturerThumbs'],'img');
            $_POST['anlitijiao']['mcs_lecturerThumbs'] = $this->formatUploadImg($mcs_lecturerThumbs,$_FILES['mcs_lecturerThumbs']['name']);
        }
         // 保存公司 LOGO
        if($_FILES['mcs_companyThumbs']['name'] != "" || $_FILES['mcs_companyThumbs']['name'] != null){
            $mcs_companyThumbs = $this->upload_file($_FILES['mcs_companyThumbs'],'img'); 
            $_POST['anlitijiao']['mcs_companyThumbs'] = $this->formatUploadImg($mcs_companyThumbs, $_FILES['mcs_companyThumbs']['name']);
        } 
        //保存案例文稿
        if($_FILES['caseWord']['name'] != "" || $_FILES['caseWord']['name'] != null){
            $_POST['anlitijiao']['mcs_caseWord'] = $this->UpToQiniu($_FILES['caseWord']);
        } 
        //保存PPT稿件
         if(!empty($_FILES['casePPT']['name'])){
            $_POST['anlitijiao']['mcs_casePPT'] = $this->UpToQiniu($_FILES['casePPT']);
        }

         if($_POST['anlitijiao']['mcs_grantFullName'] == '注明是您本人还是您的团队'){
            $_POST['anlitijiao']['mcs_grantFullName'] = '';
        }
        if($_POST['anlitijiao']['mcs_shareInOtherMeeting'] == '如果曾经分享过，请注明哪个会议以及反馈分数'){
            $_POST['anlitijiao']['mcs_shareInOtherMeeting'] = '';
        }
         if($_POST['anlitijiao']['mcs_courseDescription'] == '提示：200字~300字的案例启示或者案例简述'){
            $_POST['anlitijiao']['mcs_courseDescription'] = '';
        }
        // p($_POST['anlitijiao']);
        $return = $this->curl->curl_action('user-api/case-submit-update', ['anlitijiao'=>$_POST['anlitijiao']],'post');
        showmessage('案例修改成功', APP_PATH.'index.php?m=api&c=myCaseList');
    }


    //发送邮件 
    public function sendEmail()
    {
       
        $arr = $_POST ;
        if($arr[role] == "产品经理"){
             $email = 'clement@buzz.com';
        }else if($arr[role] == "团队经理"){
             $email = 'speaker@msup.com.cn';
        }else if($arr[role] == "架构师"){
             $email = 'teng.ma@msup.com.cn';
        }else if($arr[role] == "开发经理"){
             $email = 'qiuyan@msup.com.cn';
        }else if($arr[role] == "测试经理"){
             $email = 'chaiying@msup.com.cn';
        }
        // $email = '751239183@qq.com';
        $msg="
                <p style='font-size:30px;'>需求帮助人的详细信息</p>
                <div style='witd:100px;'>
                    <p style='margin-left:10px;'>姓名：".$arr[name]."</p>
                    <p style='margin-left:10px;'>角色：".$arr[role]."</p>
                    <p style='margin-left:10px;'>手机号码：".$arr[phone]."</p>
                    <p style='margin-left:10px;'>电子邮件：".$arr[email]."</p>
                    <p style='margin-left:10px;'>公司名称：".$arr[companyName]."</p>
                    <p style='margin-left:10px;'>职位：".$arr[position]."</p>
                </div>
            ";
        $res = sedEmail($email,$arr,'Top100案例提交',$msg);
        if($res == true){
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
            echo '<script language="javascript" charset="gbk">';
            echo 'alert("恭喜你提交成功");';
            echo "location.href='/index.php?m=api&c=caseSubmit'";
            echo '</script>';
        }else{
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
            echo '<script language="javascript" charset="gbk">';
            echo 'alert("提交失败，请重新输入");';
            echo "location.href='/index.php?m=api&c=caseSubmit'";
            echo '</script>';
        }
    }    
}
?>
 