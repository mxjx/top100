<?php
pc_base::load_app_class('api', 'api');
pc_base::load_app_class("HttpService", 'api');
pc_base::load_sys_class('param');
pc_base::load_app_class('jssdk', 'api');

class edm extends api
{
	public function top100v1()
	{
		$imgPath = 'http://'.$_SERVER[SERVER_NAME].'/statics/images/17edm';
        include template('edm', '2017top100');
	}
	public function top100v2()
	{
		$imgPath = 'http://'.$_SERVER[SERVER_NAME].'/statics/images/17edm';
        include template('edm', '2017top100v2');
	}
	public function top100v3()
	{
		$imgPath = 'http://'.$_SERVER[SERVER_NAME].'/statics/images/17edm/yaxin';
        include template('edm', '2017top100v3');
	}
	public function top100v4()
	{
		$imgPath = 'http://'.$_SERVER[SERVER_NAME].'/statics/images/17edm/v4';
		$data = $this->getCaseFromServer();
        include template('edm', '2017top100v4');
	}
	public function top100v5()
	{
		$imgPath = 'http://'.$_SERVER[SERVER_NAME].'/statics/images/17edm/v5';
		$clear = false;
		if(isset($_GET['clear'])){
			$clear = true;
		}
		$data = $this->getCaseFromServer($clear);
		$producer = $this->getProducer();
        include template('edm', '2017top100v5');
	}
	private function getCaseFromServer($clear=false)
	{
		if(!isset($_SESSION)){
			session_start();
		}
		if($clear){
			$_SESSION['edmv4'] = '';
		}
		if(!empty($_SESSION['edmv4'])){
			$data = $_SESSION['edmv4'];
		}else{
			$con = ['mbc_listId' => 3];
	        $request = [
	            'mm' => 'bangdankecheng',
	            'mw' => $con,
	            'mp' => ''
	        ];

	        $return_entry = $this->curl->curl_action('/Top100/top100-api/index',$request);
	        $data = $return_entry['data'];
	        foreach ($data as $k => $v) {
	        	if(!empty($v['companyThumbs'])){
	        		$companyThumbs = json_decode($v['companyThumbs'],true);
	        		$data[$k]['companyThumbs'] = $companyThumbs[0]['fileUrl'];
	        		$data[$k]['lecturer'] = $v['courseLecturer'][0]['lecturer'];
	        	}
	        	if($data[$k]['lecturer']['name'] == '柳忠伟'){
	        		$data[$k]['lecturer']['position'] = '58到家技术平台部负责人';
	        	}
	        	if($data[$k]['lecturer']['name'] == '罗琼'){
	        		$data[$k]['lecturerCompany'] = '中国移动';
	        	}
	        	if($data[$k]['lecturer']['name'] == '茹忆'){
	        		$data[$k]['lecturer']['position'] = '人工智能实验室负责人';
	        	}
	        	if($data[$k]['lecturer']['name'] == '李双涛'){
	        		$data[$k]['lecturer']['position'] = '中间件团队首席架构师';
	        	}
	        	if($data[$k]['lecturer']['name'] == '陈宗志'){
	        		$data[$k]['lecturer']['position'] = 'web平台部基础架构技术经理';
	        	}
	        	if($data[$k]['lecturer']['name'] == '田霞'){
	        		$data[$k]['lecturerCompany'] = '畅捷通信息';
	        		$data[$k]['lecturer']['position'] = 'web平台部基础架构技术经理';
	        	}
	        	if($data[$k]['lecturer']['name'] == '刘杰'){
	        		$data[$k]['lecturer']['position'] = '大数据研究院平台研发主管';
	        	}
	        	if($data[$k]['lecturer']['name'] == '陈晓畅'){
	        		$data[$k]['lecturerCompany'] = '腾讯';
	        		$data[$k]['lecturer']['position'] = '设计中心总监';
	        	}
	        	if($data[$k]['lecturer']['name'] == '张晶华'){
	        		$data[$k]['lecturer']['position'] = '产品设计研究高级经理';
	        	}
	        	if($data[$k]['lecturer']['name'] == '熊志男'){
	        		$data[$k]['lecturerCompany'] = '京东商城';
	        	}
	        	if($data[$k]['lecturer']['name'] == '蔡震坤'){
	        		$data[$k]['lecturerCompany'] = '平安证券';
	        	}
	        	if($data[$k]['lecturer']['name'] == '李成银'){
	        		$data[$k]['lecturer']['position'] = '高级技术经理';
	        	}
	        }
	        $data = array_reverse($data);
	        $_SESSION['edmv4'] = $data;
		}
		return $data;
	}
	private function getProducer()
	{
		$request = [
            'mm' => 'chupinren',
            'mw' => 'mpr_venue != 0 ',
            'mr' => [
                'huiyuanxinxi' => [
                    'mm' => 'huiyuanxinxi'
                ]
            ]
        ];
        $return = $this->curl->curl_action('api/index',$request);
        $list = $return['data'];
        $len = count($list);
        $i = 0;
        foreach ($list as $k => $v) {
            if($v['year'] == '2017'){
                $result[$i]['id'] = $v['id'];
                $result[$i]["user_id"] = $v["user_id"];
                $result[$i]["venue"] = $v["venue"];
                $result[$i]["venueName"] = $v["venueName"];
                $result[$i]["year"] = $v["year"];

                $info = $v["memberInfo"];
                if($info['name'] == '郑宇' || $info['name'] == '耿杰森' || $info['name'] == '张溪梦'){
                	continue;
                }
                // if($info['name'] == '耿杰森'){
                // 	$info['company'] = '数据应用学院';
                // }
                if($info['name'] == '钱蓓蕾'){
                	$info['position'] = '测试总监';
                }
                if($info['name'] == '吴亮（月影）'){
                	$info['position'] = '副总监、360技术委员会委员';
                }
                if($info['name'] == '唐沐'){
                	$info['position'] = '小米影业总裁';
                }
                $result[$i]["name"] = $info['name'] ? : '';
                $result[$i]["company"] = $info['company'] ? : '';
                $result[$i]["position"] = $info['position'] ? : '';
                $thumb = $info['thumbs'] ?
                    json_decode($info['thumbs'])[0]->fileUrl :
                    'https://mrm.msup.com.cn/Public/Admin/images/default_headpic.png';
                $result[$i]["thumb"] = $thumb;
                $desc = $v["description"] ? : $info["description"];
                if(mb_strlen($desc,'UTF8') > 110){
                    $desc =  mb_substr($desc,0,110,"utf-8").'...' ;
                }
                $result[$i]["description"] = $desc ? : '';
                $i++;
            }else if($v['year'] != '' && $v['year'] != 0){
                $before[] = $v;
            }
        }
        return $result;
	}
	public function edmByTem()
	{
		$id = $_GET['id'];
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://mrm.msup.com.cn/admin.php/Top100/top100-edm/get-edm?id=".$id,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "无效的EDM链接" ;
		} else {
			echo $response;die;
		}
	}
}
?>