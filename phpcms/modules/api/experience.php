<?php
pc_base::load_app_class('api', 'api');
pc_base::load_app_class("HttpService", 'api');
pc_base::load_sys_class('param');
pc_base::load_app_class('jssdk', 'api');
class experience extends api {
	public function __construct() {
		$this->curl = new curl();
        $this->common = new common();
        $this->_session_start();
	}
    /**
     * 达到登陆用户的信息
     * @wyq
     * @date   2016-07-21
     * @return [type]     [description]
     */
    public function getUserInfo()
    {
        $use_id = empty(param::get_cookie("_userid"))?null:param::get_cookie("_userid");
        if(!empty($use_id)){
            $request = [
                'mm' => 'huiyuan',
                'mw' => ['mu_id'=>$use_id,'time'=>time()],
                'mr' => [
                    'huiyuanxinxi' => [
                         'mm' => 'huiyuanxinxi',
                    ]
                ]
            ];
            $return = $this->curl->curl_action('api/index',$request);
            return $return['data'][0]['memberInfo'];
        }
    }
	public function init() {
        $tickets = $this->common->getTicketTypes('1155');
        $userinfo = $this->getUserInfo();
        include template('api', 'apply-experience');
        // include template('apply', 'applyStepOne');//applyStepOne
	}
	public function step2(){
		include template('api', 'apply-experience-step2');
    }
    //2017 报名
    public function test() {
        $tickets = $this->common->getTicketTypes('1155');
        $userinfo = $this->getUserInfo();
        include template('api', 'apply2017');//apply1
        // include template('apply', 'applyStepOne');//applyStepOne
    }
    public function coupon() {
        $tickets = $this->common->getTicketTypes('1155');
        $userinfo = $this->getUserInfo();
        include template('api', 'coupon');
    }
    /**
     * 保存第一步输入的信息
     * @AuthorHTL
     * @DateTime  2016-05-23T13:54:36+0800
     * @return    [type]                   [description]
     */
    public function saveStep1($data)
    {
        $cookietime = SYS_TIME+114400;
        foreach ($data as $k => $v) {
            param::set_cookie($k, $v, $cookietime);
        }
       
    }
    /**
     * 购买票 第一步点击保存 
     */
    public function buyStep2()
    {
        $this->saveStep1($_POST);
        if($this->common->getUserId()){
            $this->userLoginCreateOrder($_POST['ticketType_day']);
        }else{
            include template('apply', 'applyStepTwo');
        }
    }
    /**
     *用户登录的时候 直接生成订单
     */
    public function userLoginCreateOrder($ticketType_day)
    {
        $memberInfo = $this->getUserInfo();

        $data['name'] = $memberInfo['name'];
        $data['sex'] = $memberInfo['sex'] == "男"?'0':'1';
        $data['phone'] = $memberInfo['phone'];
        $data['company'] = $memberInfo['company'];
        $data['position'] = $memberInfo['position'];
        $data['email'] = $memberInfo['email'];
        
        $this->createOneOrder($data,$ticketType_day);
        include template('apply', 'applyStepThree');
    }
    //用户没有登陆 填写信息数据
    public function getUserInputData($value='')
    {
        // $this->createOneOrder($_POST);
        include template('apply', 'applyStepThree');
    }
    //生成订单
    public function createOneOrder($data,$ticketType_day = ""){
        $tciketType = $ticketType_day ==""?$this->getTicket():$ticketType_day;
        $ticketPirce = $this->common->getPriceByTicketType($tciketType);
        $price = $ticketPirce['hasSaled'] == 0?$ticketPirce['price']:$ticketPirce['discount'];
        $request = [
            'goupiaoyonghuxinxi' =>[
                "mou_tid" =>$tciketType,
                "mou_name" =>$data['name'],
                "mou_sex" =>$data['sex'],
                "mou_phone" =>$data['phone'],
                "mou_company" =>$data['company'],
                "mou_position" =>$data['position'],
                "mou_email" =>$data['email'],
                "mou_isOwer" =>'1',
                "mou_discount" => $price,
                "mou_order_status" =>'0',
            ],
        ];

        $return = $this->curl->curl_action('order-api/create-one-order',$request);
        if($return['errno']){
            $errorArr = array_values($return['errmsg']);
            showmessage($errorArr[0]);
        }else{
            $cookietime = SYS_TIME+114400;
            param::set_cookie('orderId',$return['data']['info']['order_id']);
            showmessage("订单生成,请核对订单信息",'/index.php?m=api&c=apply&a=buyStep3');
        }
    }
    //获得订购的票种
    private function getTicket()
    {
        return param::get_cookie("ticketType_day");
    }
    //获得订单信息
    private function getOrderId($value='')
    {
        return param::get_cookie("orderId");   
    }
    /**
     * 核对订单
     */
    public function buyStep3($value='')
    {
        $orderId = $this->getOrderId();
        $orderUsers = $this->common->getOrderInfo($orderId);
        include template('apply', 'applyStepThree'); 
    }
    /**
     * 核对完成 到达支付页面
     */
    public function buyStep4($value='')
    {
        $orderId = $this->getOrderId();
        $orderUsers = $this->common->getOrderInfo($orderId);
        $weiXinPayRes = $this->getWeiXinPayQrCode($orderId);
        
        $input = $weiXinPayRes['input'];
        $payQrcode = $weiXinPayRes['res']['code_url'];

        $this->common->updateOrderOutTradeNo($orderId,$input->getOut_trade_no());

        include template('apply', 'applyStepFour'); 
    }

    /**
     * 获得支付的二维码
     */
    public function getWeiXinPayQrCode($orderId)
    {
        require 'wxpay/lib/WxPay.Api.php';
        require 'wxpay/example/WxPay.NativePay.php';
        $notify = new NativePay();
        $input = new WxPayUnifiedOrder();
        $input->SetBody("2016top100票务的票务");
        $input->SetAttach("2016上海站Mpd的票务信息test");
        $input->SetOut_trade_no(WxPayConfig::MCHID.date("YmdHis"));
        $input->SetTotal_fee("1");
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 600));
        $input->SetNotify_url("http://".$_SERVER['SERVER_NAME']."/wxpay/example/native_notify.php");
        $input->SetTrade_type("NATIVE");
        $input->SetProduct_id($orderId);
        $result = $notify->GetPayUrl($input);

        $data['res'] = $result;
        $data['input'] = $input;
        return $data;
    }
    /**
     * 支付完成
     */
    public function success()
    {
        $orderId = $this->getOrderId();
        include template('apply', 'applySuccess');
    }

    //查询订单是否完成
    public function isPaymentSuccess()
    {
        require_once "wxpay/lib/WxPay.Api.php";
        $out_trade_no = "1002154320160801160802";
        $input = new WxPayOrderQuery();
        $input->SetOut_trade_no($out_trade_no);
        p(WxPayApi::orderQuery($input));
    }
    /**
     *获得验证的字段 
     */
    public function getAttrLables()
    {
        return array(
                '姓名'=>'name',
                '性别'=>'sex',
                '手机号码'=>'phone',
                '电子邮件'=>'email',
                '公司名称'=>'company',
                '公司职位'=>'position',
                '购票类型'=>'ticketType_day',
                '信息来源'=>'ticketType_comeform',
                '验证码'=>'code'
            );
    }
	public function sendBuyTop100TicketEmail($value='')
	{
		$this->_session_start();

		foreach ($_POST as $key => $value) {
			if(empty($value) && $key != 'coupon'){
				showmessage(array_search($key, $this->getAttrLables())."不能为空");
			}
		}
        $arr = $_POST;

        if($arr['name'] == ""){
            showmessage("姓名不能为空");
        }else if($arr['phone'] == ""){
            showmessage("手机号码不能为空");  
        }else if($arr['email'] == ""){
            showmessage("电子邮件不能为空");  
        }else if($arr['position'] == ""){
            showmessage("公司职位不能为空");  
        }else if($arr['ticketType_day'] == ""){
            showmessage("购票类型不能为空");  
        }else if($arr['ticketType_comeform'] == ""){
            showmessage("信息来源不能为空");
        }else if($arr['communicate'] == ""){
            showmessage("请选择交流方式");
        }
		// if($_SESSION['code'] != strtolower($_POST['code'])){
		// 	showmessage(L('code_error'));
		// }

        $username = trim($_POST['name']);
        $usertel = trim($_POST['phone']);
        $email = trim($_POST['email']);
        $company = trim($_POST['company']);
        $position = trim($_POST['position']);
        $communicate =  trim($_POST['communicate']);
        //票务类型
        $activityid =  '1155';
        $messageSource = trim($_POST['ticketType_comeform']);
        $tickets = $this->common->getTicketTypes($activityid);
        
        foreach ($tickets as $v) {
            if($v['id'] == $_POST['ticketType_day']){
                $ticketTile = $v['title'];
            }
        }

        $coupon = trim($_POST['coupon']);
        $json = ["mm" => "yuyue", 
                    "mrq" => [
                        "map_sid" => $activityid,
                        "map_name" => $username,
                        "map_phone" => $usertel,
                        "map_company" => $company,
                        "map_position" => $position,
                        "map_type" => "2",
                        "map_time" => time(), 
                        "map_email" => $email, 
                        "map_address" => "",
                        "map_source" => "salon", 
                        "map_messageSource" => $messageSource,
                        'map_description'=>"",
                        'map_coupon' => $coupon,
                        'map_tid' => trim($_POST['ticketType_day']),
                        'map_communicate' => $communicate,
                    ]
                ];
        $res = HttpService::http(HttpService::$create_appoint, $json,0);
		//$email = 'market@msup.com.cn';
        //$emailArr = ['1441065160@qq.com','chuanqin.liu@msup.com.cn'];
        //','zhengying@msup.com.cn','dandan.zhao@msup.com.cn','chuanqin.liu@msup.com.cn'
        $emailArr = ['market@msup.com.cn'];
        $msg="
                <p style='font-size:30px;'>Top100购票人信息</p>
                <div style='witd:100px;'>
                    <p style='margin-left:10px;'>姓名：".$arr['name']."</p>
                    <p style='margin-left:10px;'>手机号码：".$arr['phone']."</p>
                    <p style='margin-left:10px;'>电子邮件：".$arr['email']."</p>
                    <p style='margin-left:10px;'>公司名称：".$arr['company']."</p>
                    <p style='margin-left:10px;'>公司职位：".$arr['position']."</p>
                    <p style='margin-left:10px;'>购票类型：".$ticketTile."</p>
                    <p style='margin-left:10px;'>是否期望企业内部交流：".$communicate."</p>
                </div>
            ";
        //数组发送邮件
        foreach($emailArr as $email){
            sedEmail($email,$arr,'Top100购票人信息',$msg);
        }
      //  $result = sedEmail($email,$arr,'Top100购票人信息',$msg);
        if($res == true){
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
            echo '<script language="javascript" charset="gbk">';
            echo 'alert("恭喜您提交成功，我们的工作人员会在第一时间与您取得联系。");';
            echo "location.href='/index.php?m=api&c=experience&a=step2'";
            echo '</script>';
        }else{
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
            echo '<script language="javascript" charset="gbk">';
            echo 'alert("提交失败，请重新输入");';
            echo "location.href='/index.php?m=api&c=experience'";
            echo '</script>';
        }
	}

    /**
     * 新的报名页面 提交数据
     * @param string $value
     */
    public function sendCouponBuyTop100TicketEmail($value='')
    {
        $this->_session_start();
        foreach ($_POST as $key => $value) {
            if(empty($value) && $key != 'coupon'){
                showmessage(array_search($key, $this->getAttrLables())."不能为空");
            }
        }
        $arr = $_POST;
        if($arr['name'] == ""){
            showmessage("姓名不能为空");
        }else if($arr['sex'] == ""){
            showmessage("性别不能为空");
        }else if($arr['phone'] == ""){
            showmessage("手机号码不能为空");
        }else if($arr['email'] == ""){
            showmessage("电子邮件不能为空");
        }else if($arr['position'] == ""){
            showmessage("公司职位不能为空");
        }else if($arr['ticketType_day'] == ""){
            showmessage("购票类型不能为空");
        }else if($arr['ticketType_comeform'] == ""){
            showmessage("信息来源不能为空");
        }else if($arr['coupon'] == ""){
            showmessage("优惠码不能为空");
        }else if($arr['code'] == ""){
            showmessage("验证码不能为空");
        }

        if($_SESSION['code'] != strtolower($_POST['code'])){
            showmessage(L('code_error'));
        }

        $username = trim($_POST['name']);
        $usertel = trim($_POST['phone']);
        $email = trim($_POST['email']);
        $company = trim($_POST['company']);
        $position = trim($_POST['position']);
        //票务类型
        $activityid =  '297';
        $messageSource = trim($_POST['ticketType_comeform']);
        $tickets = $this->common->getTicketTypes($activityid);

        foreach ($tickets as $v) {
            if($v['id'] == $_POST['ticketType_day']){
                $ticketTile = $v['title'];
            }
        }
        //$ticketTile = $ticketTile == '体验票(1天)' ? '体验票' : $ticketTile;
        $coupon = trim($_POST['coupon']);
        $json = ["mm" => "yuyue",
            "mrq" => [
                "map_sid" => $activityid,
                "map_name" => $username,
                "map_phone" => $usertel,
                "map_company" => $company,
                "map_position" => $position,
                "map_type" => "2",
                "map_time" => time(),
                "map_email" => $email,
                "map_address" => "",
                "map_source" => "salon",
                "map_messageSource" => $messageSource,
                'map_description'=>"",
                'map_coupon' => $coupon,
                'map_tid' => trim($_POST['ticketType_day']),
                'map_audit' => '1',
                'map_promotion' => '市场部'
            ]
        ];
        $res = HttpService::http(HttpService::$create_appoint, $json,0);
        $email = 'market@msup.com.cn';
        //$email = '1441065160@qq.com';
        $msg="
                <p style='font-size:30px;'>Top100购票人信息</p>
                <div style='witd:100px;'>
                    <p style='margin-left:10px;'>姓名：".$arr['name']."</p>
                    <p style='margin-left:10px;'>性别：".$arr['sex']."</p>
                    <p style='margin-left:10px;'>手机号码：".$arr['phone']."</p>
                    <p style='margin-left:10px;'>电子邮件：".$arr['email']."</p>
                    <p style='margin-left:10px;'>公司名称：".$arr['company']."</p>
                    <p style='margin-left:10px;'>公司职位：".$arr['position']."</p>
                    <p style='margin-left:10px;'>购票类型：".$ticketTile."</p>
                    <p style='margin-left:10px;'>报名页面：使用优惠码报名页</p>
                </div>
            ";

        $res = sedEmail($email,$arr,'Top100购票人信息',$msg);
        if($res == true){
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
            echo '<script language="javascript" charset="gbk">';
            echo 'alert("恭喜您提交成功，我们的工作人员会在第一时间与您取得联系。");';
            echo "location.href='/index.php?m=api&c=apply&a=coupon'";
            echo '</script>';
        }else{
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
            echo '<script language="javascript" charset="gbk">';
            echo 'alert("提交失败，请重新输入");';
            echo "location.href='/index.php?m=api&c=apply&a=coupon'";
            echo '</script>';
        }
    }
    /**
     * 赞助发送邮件 给sissi
     * @wyq
     * @date  2016-09-18
     * @param string     $value [description]
     */
    public function sendSupportEmail($value='')
    {
        $data = $_POST['data'];
        $dataArr = explode("##",$data);
        if(!empty($dataArr[0])) {
            $msg = "
                <p style='font-size:30px;'>Top100赞助申请信息</p>
                <div style='witd:100px;'>
                    <p style='margin-left:10px;'>姓名：" . $dataArr[0] . "</p>
                    <p style='margin-left:10px;'>性别：" . $dataArr[1] . "</p>
                    <p style='margin-left:10px;'>邮箱：" . $dataArr[2] . "</p>
                    <p style='margin-left:10px;'>公司：" . $dataArr[3] . "</p>
                    <p style='margin-left:10px;'>电话：" . $dataArr[4] . "</p>
                    <p style='margin-left:10px;'>职位：" . $dataArr[5] . "</p>
                    <p style='margin-left:10px;'>留言：" . $dataArr[6] . "</p>
                </div>";
            //$email = "1441065160@qq.com";
            $email = "market@msup.com.cn";
            $dataArr['email'] = $dataArr[2];
            $result = sedEmail($email, $dataArr, 'Top100赞助申请信息', $msg);

            if ($result == true) {
                $message = "申请成功";
                $status = 200;
            } else {
                $message = "申请失败";
                $status = 0;
            }
        }else{
            $message = "申请失败";
            $status = 0;
        }
        $res['status'] = $status;
        $res['message'] = $message;
        $res['data'] = "";
        echo json_encode($res);
    }
    /**
     * 到达蚂蚁金服志愿者招聘页面
     * @wyq
     * @DateTime  2016-02-17T16:00:03+0800
     * @return    [type]                   [description]
     */
    public function toVolunteerAntHtml(){
        include template('content', 'list_volunteer_ant');
    }
    /**
     * 发送志愿者的个人信息给Msup志愿者负责人
     * @wyq
     * @DateTime  2016-02-17T16:06:41+0800
     * @return    [type]                   [description]
     */
    public function sendVolunteerAntToLeading(){
        foreach ($_POST['data'] as $k => $v) {
            $arr[$v['name']] = $v['value'];
        }
        if($arr['ajax']){
            $email = 'assistant@msup.com.cn';
            // $email = '751239183@qq.com';
            $msg="
                <p style='font-size:30px;'>Top100志愿者信息</p>
                <div style='witd:100px;'>
                    <p style='margin-left:10px;'>姓名：".$arr['name']."</p>
                    <p style='margin-left:10px;'>性别：".$arr['sex']."</p>
                    <p style='margin-left:10px;'>手机号码：".$arr['phone']."</p>
                    <p style='margin-left:10px;'>电子邮件：".$arr['email']."</p>
                    <p style='margin-left:10px;'>学校/院系：".$arr['school']."</p>
                </div>";
            $res = sedEmail($email,$arr,'Top100志愿者信息',$msg);
        }
        echo $res;
    }
	private function _session_start() {
        $session_storage = 'session_'.pc_base::load_config('system','session_storage');
        pc_base::load_sys_class($session_storage);
        session_start();
	}
    //edm的日志安排
    public function edm(){
        $return_data = $this->getPaikData();
        $time = $return_data[0];
        $end_time = $return_data[1];
        $data_new = $return_data[2];
        include template('edm', 'index');
    }
     //日程安排
    public function scheduling_program_1(){
        $return_data = $this->getPaikData();
        $time = $return_data[0];
        $end_time = $return_data[1];
        $data_new = $return_data[2];
        // p($data_new);
        include template('api', 'caseSubmit_anpai_1');
    }
      public function scheduling_program_2(){
        include template('api', 'caseSubmit_anpai_2');
    } 
     //日程安排
    public function scheduling_program(){
        $return_data = $this->getPaikData();
        $time = $return_data[0];
        $end_time = $return_data[1];
        $data_new = $return_data[2];
        // p($data_new);
        include template('api', 'caseSubmit_anpai');
    }
    //日程安排by2016
    public function scheduling_program_2016(){
        $return_data = $this->getPaikData(297);
        //var_dump($return_data);die;
        $time = $return_data[0];
        $end_time = $return_data[1];
        $data_new = $return_data[2];

        $jssdk = new JSSDK("wx86c83315ed369afc", "16c89e4c8f978d063c8104be06334d42");
        $signPackage = $jssdk->GetSignPackage();
        include template('api', 'caseSubmit_anpai_2016');
    }
    //日程安排by2017
    public function scheduling_program_2017(){
        $return_data = $this->getPaikData(663);
        //var_dump($return_data);die;
        $time = $return_data[0];
        $end_time = $return_data[1];
        $data_new = $return_data[2];
        $ceremony = $this->ceremony();
        $jssdk = new JSSDK("wx86c83315ed369afc", "16c89e4c8f978d063c8104be06334d42");
        $signPackage = $jssdk->GetSignPackage();
        include template('api', 'caseSubmit_anpai_2017');
    }
    private function ceremony()
    {
        $data = [
            [
                'title' => '构建具有国际竞争力的技术组织',
                'teacher' => '苏光明',
                'desc1' => '中国国家外国专家局',
                'desc2' => '中国国际人才交流基金会主任'
            ],
            [
                'title' => '人工智能时代下的研发战略演进',
                'teacher' => '刘付强',
                'desc1' => '全球软件案例研究峰会主席',
                'desc2' => 'msup创始人兼CEO'
            ],
            [
                'title' => '百度云ABC Inspire：为行业变革而来',
                'teacher' => '傅徐军',
                'desc1' => '百度云副总经理',
                'desc2' => ''
            ],
            [
                'title' => '新时代下的开放生态',
                'teacher' => '李晓璐',
                'desc1' => '小米开发者生态负责人',
                'desc2' => ''
            ],
            [
                'title' => '硅谷数据团队能力模型',
                'teacher' => '李仁杰',
                'desc1' => 'Riot大数据和人工智能负责人',
                'desc2' => ''
            ],
            [
                'title' => 'AI视频大数据应用创新',
                'teacher' => '王涛博士',
                'desc1' => '爱奇艺公司首席科学家',
                'desc2' => ''
            ],
            [
                'title' => '融合知识图谱的机器学习方法',
                'teacher' => '张铭',
                'desc1' => '北京大学信息科学技术学院教授 ',
                'desc2' => ''
            ],
            [
                'title' => 'AI，互联网能力的进化',
                'teacher' => ' 许式伟',
                'desc1' => '七牛云CEO',
                'desc2' => ''
            ],[
                'title' => '演讲主题确定中',
                'teacher' => '徐昊',
                'desc1' => 'ThoughtWorks中国区CTO',
                'desc2' => ''
            ],
            [
                'title' => '精益创新与快速交付价值',
                'teacher' => '姚冬',
                'desc1' => 'msup研究院院长',
                'desc2' => ''
            ],
            [
                'title' => '演讲主题确定中',
                'teacher' => '任道远',
                'desc1' => 'VMware中国研发中心总经理',
                'desc2' => ''
            ],
            [
                'title' => '360推动AI技术平台化变革',
                'teacher' => '邓雄',
                'desc1' => '360智能工程中心总经理',
                'desc2' => ''
            ],
            [
                'title' => 'A.I.时代职场趋势洞察',
                'teacher' => '王迪',
                'desc1' => 'LinkedIn领英中国技术副总裁',
                'desc2' => ''
            ]
        ];
        return $data;
    }
    //日程安排by20161209
    public function scheduling_edm(){
        $return_data = $this->getPaikData(1155);
        //var_dump($return_data);die;
        $time = $return_data[0];
        $end_time = $return_data[1];
        $data_new = $return_data[2];
        include template('api', 'caseSubmit_edm_2016');
    }
    //2017 top100 edm v2
    public function scheduling_edmv2(){
        $return_data = $this->getPaikData(1155);
        //var_dump($return_data);die;
        $time = $return_data[0];
        $end_time = $return_data[1];
        $data_new = $return_data[2];
        include template('api', 'top100edm_2017');
    }
    //日程安排by20161209 同上,只是宽度是900px
    public function scheduling_edm_900(){
        $return_data = $this->getPaikData(1155);
        $time = $return_data[0];
        $end_time = $return_data[1];
        $data_new = $return_data[2];
        include template('api', 'caseSubmit_edm_2016_900');
    }

    public function getPaikDataNew($msvc_sid = 127){
        $request = [
            'mm' => 'huichangkecheng',
            'mw' => [ "msvc_sid" => $msvc_sid],
            'ms' => '*',
        ];
        $return = $this->curl->curl_action('/Top100/top100-api/scheduling',$request);
        $data = $return['data'];
       // var_dump($data);die;
        $len = count($data);
        for($i= 0;$i<$len;$i++){
            $data[$i]['date'] = date('Y-m-d',$data[$i]['date']);
            $data_new[$data[$i]['date']][$data[$i]['startTime']][] = $data[$i];
            $start_time[] = $data[$i]['startTime'];
            $end_time[] = $data[$i]['endTime'];
        }

        $time = array_unique($start_time);
        $data_new = array_values($data_new);
        //来该换数据顺序  来适配相对应的场次
        sort($time);
        var_dump($data_new);die;
    }
    public function getPaikData($msvc_sid = 127){
            $request = [
                'mm' => 'huichangkecheng',
                'mw' => [ "msvc_sid" => $msvc_sid],
                'ms' => '*',
            ];
        $return = $this->curl->curl_action('/Top100/top100-api/scheduling',$request);
        $data = $return['data'];
        // p($data);
        $len = count($data);
        for($i= 0;$i<$len;$i++){
            $data[$i]['date'] = date('Y-m-d',$data[$i]['date']);
            $data_new[$data[$i]['date']][$data[$i]['startTime']][] = $data[$i];
            $start_time[] = $data[$i]['startTime'];
            $end_time[] = $data[$i]['endTime'];
        }

        $time = array_unique($start_time);
        $data_new = array_values($data_new);
        //来该换数据顺序  来适配相对应的场次
        sort($time);
        //日期
        for ($i=0; $i <count($data_new) ; $i++) {
         //时刻
         for ($k=0; $k <count($data_new[$i]) ; $k++) {
            $data = [];
            $map = ['0'=>'产品创新','1'=>'团队管理','2'=>'架构设计','3'=>'工程实践','4'=>'测试管理'];
            // p($data_new[$i][$time[$k]]);
            $temp = [];
            foreach ($map as $mk => $mv) {
                foreach ($data_new[$i][$time[$k]] as $kk => $vv) {
                    if($mv == $vv['venueName']) {
                        // p($data_new[$i][$time[$k]][$kk]);
                        $temp[$mk] = $data_new[$i][$time[$k]][$kk];
                        // p($temp);
                    //$data[] = $temp;

                    }
                }

            }
            $data_new[$i][$time[$k]]  = $temp;
            }
        }
        $end_time = array_unique($end_time);
        sort($end_time);
        $shuz = array_unique($data_new[0][$time[1]]);
        $return_data[0] = $time;
        $return_data[1] = $end_time;
        $return_data[2] = $data_new;
        return $return_data;
    }
}
?>
