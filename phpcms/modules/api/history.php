<?php
pc_base::load_app_class('api', 'api');
class history  extends api {
	public function __construct() {
		$this->curl = new curl();
	}
	/**
	 * 到达top100历史回顾页面
	 * @AuthorHTL
	 * @DateTime  2016-05-23T11:36:51+0800
	 * @return    [type]                   [description]
	 */
	public function tohistory()
	{
		include template('content', 'history');
	}
	/**
	 * 到达历史回顾top100的详细页面
	 * @AuthorHTL
	 * @DateTime  2016-05-23T16:29:46+0800
	 * @return    [type]                   [description]
	 */
	public function detail()
	{
		if (!$_GET['top100Num']) {
			showmessage('错误的请求', '/history');
		}
		$num = $_GET['top100Num'];
		//获得教练
		$request = [
            'mm' => 'bangdankecheng',
            'mw' => ['mbc_listId' => 1],
        ];
        $return_entry_res = $this->curl->curl_action('/Top100/top100-api/index',$request);
        for ($i=0; $i < 20 ; $i++) { 
        	$data[] = $return_entry_res['data'][$i];
        	$courseIds[] = $return_entry_res['data'][$i]['courseid'];
        }
        $courseIds = array_values($courseIds);
        //获得top100反馈
        $request = [
                'mm' => 'kechengfankui',
                'mw' => ['fb_courseId'=>$courseIds],
                'ml' => '10'
        ];
        $return = $this->curl->curl_action('lecturer-api/get-lecturer-feed-answer',$request);
         //获得反馈中 最长的那
        $answers = $return['data']; 
        foreach ($answers as $k => $v) {
        	if($answerData = $this->dealAnswerArray($v['answer'])){
        		$v['answer'] = $answerData;
        		$answerAllData[] = $v;
        	}
        }
        
		include template('content', 'history_detail');
	}
	public  function dealAnswerArray($answer){
        $len = count($answer);
        if(!$answer){
            return ;
        }
        foreach ($answer as $k => $v) {
            $answerArray[] = mb_strlen($v['answer']);
        }
        $max = max($answerArray);
        $index = array_search($max, $answerArray);
        $maxAnswer = $answer[$index]['answer'];
        // p($maxAnswer);
        if(mb_strlen($maxAnswer) < 3){
            return "";
        }else{
            if($maxAnswer == '是否希望案例走进企业'){
                $maxAnswer = '希望案例走进企业';
            }
            return $maxAnswer;
        }
    }
	
}