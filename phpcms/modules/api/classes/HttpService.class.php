<?php

class HttpService {
    
    public static $apiurl = 'https://mrm.msup.com.cn/admin.php/api/index';
    public static $searchurl = 'https://mrm.msup.com.cn/admin.php/api/search-course';
//     public static $searchurl = 'http://manage.msup.com.cn/admin.php/api/search-course';
    public static $tagurl = 'https://mrm.msup.com.cn/admin.php/api/search';
    public static $get_cate_tags_order = 'https://mrm.msup.com.cn/admin.php/api/get-cate-tags-order';
    public static $lecturers_courses = 'https://mrm.msup.com.cn/admin.php/api/lecturers-courses';
    public static $lencture_current_course = 'https://mrm.msup.com.cn/admin.php/api/lecturer-recent-schedule';
    public static $GetTheNumberOfTagsInSearchCourse = 'https://mrm.msup.com.cn/admin.php/api/get-the-number-of-tags-in-search-course';
    public static $HotCourseOnMonth = 'https://mrm.msup.com.cn/admin.php/api/hot-course-on-mounth';
    public static $create_appoint = 'https://mrm.msup.com.cn/admin.php/user-api/create-appoint';
    public static $create_intention_email = 'https://mrm.msup.com.cn/admin.php/user-api/create-intention-email';
    public static $error = "";
    
    /**
     * 调取接口，获取数据信息
     * @param string $url 接口地址
     * @param array $data 发送的数据
     */
    public static function http($url,$data,$debug = FALSE){
//         $data = array('data'=>json_encode($data));
        if($debug == 1){
            echo json_encode($data);exit();
        }
        $url = $url.'?data='.urlencode(json_encode($data));
        $res = self::request($url);
        if($debug == 2){
            echo $url;exit();
        }
        $res = trim($res);
        $res = ltrim($res,"(");
        $res = rtrim($res,")");
        if($debug == 3){
            echo($res);exit();
        }
//                 
    
        if(empty($res)){
            self::$error = '接口调用出错';
            return false;
        }
//                 echo($res);exit;
        $result = json_decode($res,true);
        if(empty($result) && !is_array($result)){
            self::$error = '获取的数据错误';
            return false;
        }
//                 var_dump($result);exit;
        if($result['errno'] !== "0"){
            self::$error = $result['errmsg'];
            return false;
        }
    
        return $result['data'];
    
    }
    
    /**
     * curl
     */
    public static function request($url,$data = array()){
        self::$error = "";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT,15);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data))
        );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         
        $status = curl_exec($ch);
         
        if (curl_errno($ch)) {
            $msg = curl_error($ch);
            return json_encode(array('errno'=>0,'errmsg'=>$msg));
        }
        curl_close ($ch);
        return $status;
    }
    
    
    
}