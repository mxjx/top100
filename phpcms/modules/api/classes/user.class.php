<?php 
pc_base::load_app_class('api','api');
class user extends api {
	public $user_id = '';
	public function __construct() {
		parent::__construct();
	    $this->user_id = param::get_cookie('_userid');
        $this->role = param::get_cookie('_role');
        if(empty($this->user_id)){
            showmessage("请先登录","/index.php?m=member&c=index&a=login");
        }
	}

	protected function isProducer(){
		return $this->role === '出品人';
	}
}

?>