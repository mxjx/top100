<?php 
class curl{
	public $baseUrl = 'https://mrm.msup.com.cn/admin.php/';
	
	// public $baseUrl = 'http://mrmwyq.com/admin.php/';
	/**
	 * 通过 CURL 发送请求
	 * @param  [type] $url    [访问地址]
	 * @param  [type] $params [请求参数]
	 * @param  string $type   [请求方式 可选：get,post]
	 * @return [type]         [description]
	 */
	public function curl_action($url, $params, $type = 'get'){
		$url = ($type == 'get') ? $this->parseUrl($url, $params) : $this->parseUrl($url);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_REFERER, 'www.top100summit.com');
		// POST 方式发送
		if ($type == 'post') {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $this->parseParams($params));
		}
		$row = curl_exec($ch);
		curl_close($ch);
		return $this->formatCurlOut($row);
	} 
	public function parseParams($params) {
		return ['data' => json_encode($params)];
	}
	public function post($url, $params){

		$url = $this->parseUrl($url);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		$row = curl_exec($ch);
		curl_close($ch);
		return $this->formatCurlOut($row);

	}
	public function test($url, $params){
		echo $this->parseUrl($url, $params);exit;
	}
	private function parseUrl($url, array $request = array()){
		if (empty($request)) {
			return rtrim($this->baseUrl, '/').'/'.$url;
		}
		return $this->baseUrl.$url.'?data='.json_encode($request);
	}
	public function formatCurlOut($data){
		if (substr(trim($data),0,1) != '(') {
			return $data;
		}
		return json_decode(rtrim(ltrim(trim($data),'('),')'), true);
	}
}

?>