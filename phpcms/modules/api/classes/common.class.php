<?php 
pc_base::load_app_class('curl','api');
pc_base::load_sys_class('param');
class common {
	public function __construct() {
		$this->curl = new curl();
	}
    /**
     * 调用接口的统一方法 获得对应的数据
     * @wyq
     * @date  2016-10-09
     * @param string     $value [description]
     */
    public function getData($params)
    {
        return  $this->curl->curl_action('api/index', $params);
    }
    public function getSchedulingId($value='')
    {
        return  297;
    }
	/**
	 * 得到登陆用户id
	 */
	public function getUserId()
	{
		return param::get_cookie('_userid');
	}
    /**
     * 得到用户的姓名
     */
    public function getUserName()
    {
        $params = [
            'mm' => 'huiyuanxinxi',
            'mw' => ['mmi_userId'=>$this->getUserId()],
            'ms'=>'name',
        ];
        return $this->curl->curl_action('api/index', $params)['data'][0];
    }
	/**
	 * 获得会员信息
	 */
	public function getUserInfoById($userId)
	{
		$params = [
            'mm' => 'huiyuan',
            'mw' => ['mu_id'=>$userId],
            'ms'=>'phone,email,lecturerId',
        ];
        return $this->curl->curl_action('api/index', $params)['data'][0];
	}
	/**
     * 根据mpd的id 获得mpd的票务类型
     * @AuthorHTL
     * @DateTime  2016-06-12T10:07:15+0800
     * @param     [type]                   $mpdId [description]
     * @return    [type]                          [description]
     */
    public function getTicketTypes($mpdId){
        $request = [
            'piaowuleixing' => [
                'mpw_sid' => $mpdId,
            ]
        ];
        $res = $this->curl->curl_action('user-api/get-tickets-id-by-scheduling-id',$request);
        $ticketTypes = $res['data'];
        $ticketTypes = array_values($ticketTypes);
        $freeTicket = array();
        // foreach ($ticketTypes as $k => $v) {
        //     if($v['price'] == '0'){
        //         $freeTicket = $ticketTypes[$k];
        //         unset($ticketTypes[$k]);
        //     }
        // }
        return $ticketTypes;
    }
    /**
     * 通过票务Id 来获得票务的价格信息
     */
    public function getPriceByTicketType($tid)
    {
        $schedulingId = $this->getSchedulingId(); 
        $tickets =  $this->getTicketTypes($schedulingId);
        foreach ($tickets as $v) {
            if($v['id'] == $tid){
                $price = $v['price'];
                break;
            }
        }
        $discount = $this->checkTicketTypeIsHashDiscount($tid);
        $hasSaled = 0;
        if($discount){
            $hasSaled = 1;
            $salePrice = $discount;
        }
        $data['hasSaled'] = $hasSaled;
        $data['salePrice'] = $discount;
        $data['price'] = $price;
        return $data;     
    }
    /**
     * 检查这个票务类型现在是否有优惠
     */
    public function checkTicketTypeIsHashDiscount($tid)
    {
        $request = [
            'mm' => 'zhekou',
            'mw' => "mtd_tid = ".$tid." and mtd_endTime >= '".date('Y-m-d',time())."' ",
            'ms' => '*',
            'mp' => 'mtd_endTime',
            'ml' => '1',       
        ];
        $res = $this->curl->curl_action('api/index',$request);
        if(empty($res['data'])){
            return false;
        }else{
            return $res['data'][0]['discount'];
        }
    }
    /**
     * 获得订单信息
     */
    public function getOrderInfo($orderId)
    {
        $request = [
            'mm' => 'goupiaoyonghuxinxi',
            'mw' => ['mou_order_id'=>$orderId],
        ];
        $res = $this->curl->curl_action('api/index',$request);
        return $res['data'];
    }
    /**
     * 订单生成二维码的时候 更新订单对应的微信支付订单号
     */
    public function updateOrderOutTradeNo($orderId,$outTradeNo){
        $request = [
            'goupiaoyonghuxinxi' =>[
                "mou_order_id" =>$orderId,
                'mou_outTradeNo' => $outTradeNo,
            ],
        ];
        $res = $this->curl->curl_action('order-api/update-order-out-trade-no',$request);
        return $res;
    }
    /**
     * 获得该案例的点赞数据
     */
    public function getCourseParises($courseId)
    {
        $request = [
            'mm' => 'dianzan',
            'mw' => ['mp_pkId'=>$courseId,'time'=>time()],
            'ms' => '*',
        ];
        return $this->getData($request)['data'];
    }
    /**
     *获得相关用户的信息
     */
    public function getUserMemberInfo($userIds)
    {
        $params = [
            'mm' => 'huiyuanxinxi',
            'mw' => ['mmi_userId'=>$userIds],
            'ms'=>'*',
        ];
        return $this->getData($params)['data'];
    }
    /**
     *通过上传的案例获得案例的类型
     */
    public function getCaseSubmitByCourseId($courseId)
    {
        $params = [
            'mm' => 'anlitijiao',
            'mw' => ['mcs_courseid'=>$courseId],
            'ms'=>'*',
        ];
        return $this->getData($params)['data']['0'];
    }
}

?>