<?php
pc_base::load_app_class('api', 'api');
pc_base::load_app_class("HttpService", 'api');
pc_base::load_sys_class('param');
class apply  extends api {
    private $userId;
	public function __construct() {
		$this->curl = new curl();
        $this->_session_start();
        $this->userId = empty(param::get_cookie("_userid"))?null:param::get_cookie("_userid");
	}
	public function init() {
        if(!$this->userId ){
            showmessage("请先登录","/login");
        }
		include template('api', 'apply');
	}
    /**
     * 保存第一步输入的信息
     * @AuthorHTL
     * @DateTime  2016-05-23T13:54:36+0800
     * @return    [type]                   [description]
     */
    public function saveStep1($data)
    {
        $cookietime = SYS_TIME+114400;
        foreach ($data as $k => $v) {
            param::set_cookie($k, $v, $cookietime);
        }
    }
    /**
     * 获得登陆者信息
     * @AuthorHTL
     * @DateTime  2016-05-23T16:42:21+0800
     * @return    [type]                   [description]
     */
    public function getUserInfo()
    {
      $request = [
            'mm' => 'huiyuan',
            'mw' => ['mu_id'=>$this->userId ],
            'ms' => '*',
            'mr' => [
                'huiyuanxinxi' => [
                     'mm' => 'huiyuanxinxi',
                     'ms'=>'*'
                ]
            ]
        ];
        $return = $this->curl->curl_action('api/index',$request);
        return $return['data'][0]['memberInfo'];

    }
    public function buyStep2()
    {   
        $this->saveStep1($_POST);
        $ticketType = $_POST['ticketType_day'];
        $data = $this->getUserInfo();
        include template('api', 'apply_2');
    }
    /**
     * 支付完成的界面
     * @AuthorHTL
     * @DateTime  2016-05-24T08:37:39+0800
     * @return    [type]                   [description]
     */
    public function buyStep3()
    {
        include template('api', 'apply_success');
    }
	public function snedBuyTop100TicketEmail($value='')
	{	
		$this->_session_start();
		$attrLables = array(
				'姓名'=>'name',
				'性别'=>'sex',
				'手机号码'=>'phone',
				'电子邮件'=>'email',
				'公司名称'=>'company',
				'公司职位'=>'position',
				'购票类型'=>'ticketType_day',
				'信息来源'=>'ticketType_comeform',
				'验证码'=>'code'
			);
		foreach ($_POST as $key => $value) {
			if(empty($value)){
				showmessage(array_search($key, $attrLables)."不能为空");
			}
		}
        $arr = $_POST;
        if($arr['name'] == ""){
            showmessage("姓名不能为空");
        }else if($arr['sex'] == ""){
            showmessage("性别不能为空");  
        }else if($arr['phone'] == ""){
            showmessage("手机号码不能为空");  
        }else if($arr['email'] == ""){
            showmessage("电子邮件不能为空");  
        }else if($arr['position'] == ""){
            showmessage("公司职位不能为空");  
        }else if($arr['ticketType_day'] == ""){
            showmessage("购票类型不能为空");  
        }else if($arr['ticketType_comeform'] == ""){
            showmessage("信息来源不能为空");  
        }else if($arr['code'] == ""){
            showmessage("验证码不能为空"); 
        }	
		if($_SESSION['code'] != strtolower($_POST['code'])){
			showmessage(L('code_error'));
		}

        $username = trim($_POST['name']);
        $usertel = trim($_POST['phone']);
        $email = trim($_POST['email']);
        $company = trim($_POST['company']);
        $position = trim($_POST['position']);
        $desc = trim($_POST['ticketType_day']);
        $activityid =  ' 127';
        $messageSource = trim($_POST['ticketType_comeform']);
        $json = ["mm" => "yuyue", 
                    "mrq" => [
                        "map_sid" => $activityid,
                        "map_name" => $username,
                        "map_phone" => $usertel,
                        "map_company" => $company,
                        "map_position" => $position,
                        "map_type" => "2",
                        "map_time" => "", 
                        "map_email" => $email, 
                        "map_address" => "",
                        "map_source" => "salon", 
                        "map_messageSource" => $messageSource,
                        'map_description'=>$desc
                    ]
                ];
        $res = HttpService::http(HttpService::$create_appoint, $json,0);
		$email = 'market@msup.com.cn';
        $msg="
                <p style='font-size:30px;'>Top100购票人信息</p>
                <div style='witd:100px;'>
                    <p style='margin-left:10px;'>姓名：".$arr['name']."</p>
                    <p style='margin-left:10px;'>性别：".$arr['sex']."</p>
                    <p style='margin-left:10px;'>手机号码：".$arr['phone']."</p>
                    <p style='margin-left:10px;'>电子邮件：".$arr['email']."</p>
                    <p style='margin-left:10px;'>公司名称：".$arr['company']."</p>
                    <p style='margin-left:10px;'>公司职位：".$arr['position']."</p>
                    <p style='margin-left:10px;'>购票类型：".$arr['ticketType_day']."</p>
                </div>
            ";
        
        $res = sedEmail($email,$arr,'Top100购票人信息',$msg);
        if($res == true){
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
            echo '<script language="javascript" charset="gbk">';
            echo 'alert("恭喜你提交成功");';
            echo "location.href='/index.php?m=api&c=apply'";
            echo '</script>';
        }else{
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
            echo '<script language="javascript" charset="gbk">';
            echo 'alert("提交失败，请重新输入");';
            echo "location.href='/index.php?m=api&c=apply'";
            echo '</script>';
        }
	}
    /**
     * 到达蚂蚁金服志愿者招聘页面
     * @wyq
     * @DateTime  2016-02-17T16:00:03+0800
     * @return    [type]                   [description]
     */
    public function toVolunteerAntHtml(){
        include template('content', 'list_volunteer_ant');
    }
    /**
     * 发送志愿者的个人信息给Msup志愿者负责人
     * @wyq
     * @DateTime  2016-02-17T16:06:41+0800
     * @return    [type]                   [description]
     */
    public function sendVolunteerAntToLeading(){
        foreach ($_POST['data'] as $k => $v) {
            $arr[$v['name']] = $v['value'];
        }
        if($arr['ajax']){
            $email = 'assistant@msup.com.cn';
            // $email = '751239183@qq.com';
            $msg="
                <p style='font-size:30px;'>Top100志愿者信息</p>
                <div style='witd:100px;'>
                    <p style='margin-left:10px;'>姓名：".$arr['name']."</p>
                    <p style='margin-left:10px;'>性别：".$arr['sex']."</p>
                    <p style='margin-left:10px;'>手机号码：".$arr['phone']."</p>
                    <p style='margin-left:10px;'>电子邮件：".$arr['email']."</p>
                    <p style='margin-left:10px;'>学校/院系：".$arr['school']."</p>
                </div>";
            $res = sedEmail($email,$arr,'Top100志愿者信息',$msg);
        }
        echo $res;
    }
	private function _session_start() {
        $session_storage = 'session_'.pc_base::load_config('system','session_storage');
        pc_base::load_sys_class($session_storage);
        session_start();
	}
    //edm的日志安排
    public function edm(){
        $return_data = $this->getPaikData();
        $time = $return_data[0];
        $end_time = $return_data[1];
        $data_new = $return_data[2];
        include template('edm', 'index');
    }
     //日程安排
    public function scheduling_program_1(){
        $return_data = $this->getPaikData();
        $time = $return_data[0];
        $end_time = $return_data[1];
        $data_new = $return_data[2];
        // p($data_new);
        include template('api', 'caseSubmit_anpai_1');
    }
      public function scheduling_program_2(){
        include template('api', 'caseSubmit_anpai_2');
    } 
     //日程安排
    public function scheduling_program(){
        $return_data = $this->getPaikData();
        $time = $return_data[0];
        $end_time = $return_data[1];
        $data_new = $return_data[2];
        // p($data_new);
        include template('api', 'caseSubmit_anpai');
    } 

    public function getPaikData(){
            $request = [
                'mm' => 'huichangkecheng',
                'mw' => [ "msvc_sid" => 127],
                'ms' => '*',
            ];
        $return = $this->curl->curl_action('/Top100/top100-api/scheduling',$request);
        $data = $return['data'];
        // p($data);
        $len = count($data);
        for($i= 0;$i<$len;$i++){
            $data[$i]['date'] = date('Y-m-d',$data[$i]['date']);
            $data_new[$data[$i]['date']][$data[$i]['startTime']][] = $data[$i];
            $start_time[] = $data[$i]['startTime'];
            $end_time[] = $data[$i]['endTime'];
        }

        $time = array_unique($start_time);
        $data_new = array_values($data_new);
        //来该换数据顺序  来适配相对应的场次
        sort($time);
        //日期
        for ($i=0; $i <count($data_new) ; $i++) {
         //时刻
         for ($k=0; $k <count($data_new[$i]) ; $k++) {
            $data = [];
            $map = ['0'=>'产品创新','1'=>'团队管理','2'=>'架构设计','3'=>'设计匠艺','4'=>'测试管理'];
            // p($data_new[$i][$time[$k]]);
            $temp = [];
            foreach ($map as $mk => $mv) {
                foreach ($data_new[$i][$time[$k]] as $kk => $vv) {
                    if($mv == $vv['venueName']) {
                        // p($data_new[$i][$time[$k]][$kk]);
                        $temp[$mk] = $data_new[$i][$time[$k]][$kk];
                        // p($temp);
                    //$data[] = $temp;

                    }
                }

            }
            $data_new[$i][$time[$k]]  = $temp;
            }
        }
        $end_time = array_unique($end_time);
        sort($end_time);
        $shuz = array_unique($data_new[0][$time[1]]);
        $return_data[0] = $time;
        $return_data[1] = $end_time;
        $return_data[2] = $data_new;
        return $return_data;
    }
}
?>
