<?php 
pc_base::load_app_class('api', 'api');
pc_base::load_app_class('jssdk', 'api');
class think  extends api
{
	public function __construct()
	{
		$this->curl = new curl();
		$this->common = new common();
		$this->_session_start();
		$this->pageSize = 20;
		$comeform = $_SERVER['HTTP_REFERER'];
		$comeform = explode("?", $comeform);
		$userId = param::get_cookie("_userid");
		if ($comeform[count($comeform) - 1] == "m=api&c=toCaseSubmit&a=caseSubmitEntry") {
			$request = [
				'kechengdianji' => [
					'mch_pkId' => $_GET['cs'],
					'mch_userId' => $userId,
				]
			];
			$row = $this->curl->curl_action('user-api/add-hit', $request);
		}
	}

	public function init()
	{
		$this->search();
	}
    public function search(){



		$keyword = addslashes($_POST['keyword']);
		$topYear = addslashes($_POST['topYear']);
		$tag = addslashes($_POST['tag']);
		// 组织搜索语句
		$params = $this->initParams2017();
		$time = strtotime("2018-01-01 00:00:00");
		$params['mw'] = "(mc_title like '%" . $keyword . "%' or mc_lecturerId like '%" . $keyword . "%')and mc_assignToTop100=1  and mc_createdAt < " . $time;
		if ($topYear) {
			$auth = 1 << ($topYear - 2012);
			$params['mw'] .= " and mc_top100Year %26  " . $auth . " >0";
		}else{
			$params['mw'] .= " and mc_top100Year >0 ";
		}
		if ($tag) {
			$params['mw'] .= " and mc_tag = " . $tag;
		}
		$start = $this->getPageStart();
		$params['mo'] = $start;
		$params['ml'] = 20;
		$row = $this->curl->curl_action('api/index', $params);
		$row = $this->prepareValue($row);
        $res = $this->arrangeData($row['data']);
		$data = $res ? json_encode($res) : '';
		if (!$_GET['isAjax']) {
			include template('api', 'think2018');
		} else {
			echo $data;
		}
	}
	public function arrangeData($data){
		if(empty($data)){
			return null;
		}
		foreach($data as $k => $v){
            $result[$k]['courseid'] = $v['courseid'];
			$result[$k]['title'] = $v['title'] ? : '';
			$desc = $v['content'] ? mb_substr($v['content'],0,60,"utf-8").'...' : '';
			$result[$k]['desc'] = $desc;
			$lecturer = $v['courseLecturer'][0]['lecturer'];
			$result[$k]['name'] = $lecturer['name'] ? : '';
			$result[$k]['company'] = $lecturer['company'] ? : '';
			$result[$k]['position'] = $lecturer['position'] ? : '';
			$thumb = $v['caseSubmit'][0]['companyThumbs'];
			$result[$k]['thumb'] = $thumb ? json_decode($thumb)[0]->fileUrl : '/statics/2017/images/company-logo.jpg';
		}
		return $result;
	}
	// 初始化查询参数
	public function initParams()
	{
		$params = [
			'mm' => 'kecheng',
			'mr' => [
				'kechengjiaolian' => [
					'mm' => 'kechengjiaolian',
					'mr' => [
						'jiaolian' => [
							'mm' => 'jiaolian',
							'ms' => 'ml_id,ml_name,ml_description,thumbs,ml_company,ml_position'
						]
					]
				]
			],
			'mo' => 'mc_courseid desc,mc_createdAt desc',
		];
		return $params;
	}
	// 初始化查询参数
	public function initParams2017()
	{
		$params = [
			'mm' => 'kecheng',
			'mr' => [
				'kechengjiaolian' => [
					'mm' => 'kechengjiaolian',
					'mr' => [
						'jiaolian' => [
							'mm' => 'jiaolian',
							'ms' => 'ml_id,ml_name,ml_description,thumbs,ml_company,ml_position'
						]
					]
				],
				'anlitijiao' => [
					'mm' => 'anlitijiao',
					'ms'=>'mcs_companyThumbs,mcs_courseid',
				]
			],
			'mo' => 'mc_courseid desc,mc_createdAt desc',
		];
		return $params;
	}

	// 显示 ID 单个案例
	public function show()
	{
		if (!$_GET['cs']) {
			showmessage('错误的请求', '/think');
		}
		$courseid = $_GET['cs'];
		$params = $this->initParams();
		$params['mw'] = ['mc_courseid' => $courseid];
		$row = $this->curl->curl_action('api/index', $params);
		extract($row['data'][0]);
		// 右侧推荐案例
		//$position = $this->getPosition(['mc_assignToTop100' => 1]);
		//$file = $this->getAttachUrl($file);
		//$speech = $this->getAttachUrl($speech);

		//点赞
		$parises = $this->common->getCourseParises($courseid);

		//判断该用户是否已经点过赞
		$userId = $this->common->getUserId();
		if ($userId) {
			$isParised = $this->userIsParised($parises, $userId);
		} else {
			$isParised = false;
		}
		//最近点赞的人
		//$pariseUserId = $this->getPariseUserId($parises);
		//$userMember = $this->common->getUserMemberInfo($pariseUserId);

		//是否显示下载的ppt
//		$dataFile = json_decode($row['data'][0]['file']);
//		if (is_array($dataFile)) {
//			$file = $dataFile[0]->fileUrl;
//		} else {
//			$file = $dataFile->fileUrl;
//		}

		// p($file);
		$request = [
			'mm' => 'huichangkecheng',
			'mw' => ["msvc_sid" => 1155, 'msvc_courseId' => $courseid],
			'ms' => 'courseTitle',
		];
		$return = $this->curl->curl_action('/api/index', $request);
		if (count($return['data']) > 0) {
			$ppt = 1;
		}
		$caseData = $row['data'][0];

		//如果是top100课程 显示top100的扩展字段
		if ($caseData['top100Year']) {
			$caseSubmit = $this->common->getCaseSubmitByCourseId($courseid);
			// p($data);
		}
		//获得课程类型
		if ($caseData['type'] == '1') {
			$caseType = $this->common->getCaseSubmitByCourseId($courseid)['courseTag'];
			if ($caseType) {
				$caseData['type'] = $caseType;
			}
		}
		//获取本场其他数据 (并除去 当前数据$courseid)
		$requestRelation = [
			'mm' => 'bangdankecheng',
			'mw' => [
				'mbc_listId' => 4,
				'mbc_courseTag' => $caseSubmit['courseTag'],
			]
		];
		$relation = $this->curl->curl_action('/Top100/top100-api/index', $requestRelation);
		$relationData = $relation['data'];

		$relationData = $this->setRelationData($relationData, $courseid);
		//随机获取四条本场其他信息
		$randomData = array_rand($relationData,4);

		$jssdk = new JSSDK("wx86c83315ed369afc", "16c89e4c8f978d063c8104be06334d42");
		$signPackage = $jssdk->GetSignPackage();
		include template("api", "showThink2017");
	}
	//自定义函数，在二位数组中去掉 一个指定的$courseid值
	function setRelationData($arr, $courseid)
	{
//		echo '<pre>';
//		var_dump($arr);die;
		$result = [];
		foreach($arr as $v) {
			if ($v['courseid'] != $courseid) {
				$result[$v['courseid']] = $v;
			}
		}
		return $result;
	}

	/**
	 *判断该用户是否已经点赞
	 */
	public function userIsParised($parises,$userId)
	{
		foreach ($parises as $v) {
			if($v['userId'] == $userId){
				return true;
			}
		}
		return false;
	}
	/**
	 *获得点赞
	 */
	public function getPariseUserId($parises)
	{
		$len = count($parises);
		if($len == 0){
			return array();
		}
		foreach ($parises as $v) {
			$userIds[] = $v['userId'];
		}
		return array_unique($userIds);
	}
	//用来隐藏真实的下载文件
	public function hideRealUrlDown(){
		if (empty($_GET['cs']) || !preg_match("/^[0-9]*$/", $_GET['cs']))  showmessage('您访问的页面不存在','/think/');
		$params = $this->initParams();
		$params['mw'] = ['mc_courseid' => $_GET['cs']];
		$row = $this->curl->curl_action('api/index', $params);
		$dataFile = json_decode($row['data'][0]['file']);
		if(is_array($dataFile)){
			$file = $dataFile[0]->fileUrl;
		}else{
			$file = $dataFile->fileUrl;
		}
		//20151214 lcq修改,因为有些pdf会被修改为ppt 所以被注释了
//		$fileArr = explode(".", $file);
//		$filePPT = substr($file,0,strlen($file)-strlen($fileArr[count($fileArr)-1]))."ppt";
//		$res = file_get_contents($filePPT, NULL, NULL, 20, 14);
//		if(!empty($res)){
//			$file = $filePPT;
//		}

		header('Content-type: application/pdf'); 
		header('Content-Disposition: attachment; filename='.basename($file));
		readfile($file);
		exit;
	}

	// 按标签搜索课程
	public function listCourses(){
		if (!$_GET['t'] && !is_int($_GET['t']))showmessage('你访问的页面找不到');
		$tagId = $_GET['t'];
		$start = $this->getPageStart();
		$response = $this->getCoursesByTag($tagId, $start, $this->pageSize);
		$data = json_encode($response);
		if (empty($_GET['isAjax']) || !$_GET['isAjax']) { 
			include template("api", "listThink");
		} else {
			echo $data;
		}
	}
	public function prepareValue($row)
	{
		foreach($row['data'] as &$v) {
			$v['content'] = str_cut($v['content'], 400);
			$v['desc'] = str_cut($v['desc'], 400);
			$v['praises'] = viewed_num($v['created_at'], $v['praises']);
		}
		return $row;
	}
	// 获取分页 offset的值 
	public function getPageStart(){

		$page = empty($_GET['page'])? 1 : $_GET['page'];

		return  ($page-1)*$this->pageSize;

	}
	public function getCourseNum(){
		$num = $this->countCourseNum('mc_assignToTop100=1');
		echo $_GET['callback'].'('.json_encode($num).')';
	}
	public function getSalonNum(){
		$num = $this->countCourseNum('mc_assignToSalon=1');
		echo $_GET['callback'].'('.json_encode($num).')';
	}
	public function countCourseNum($where){
		$params = [
		'mm' => 'kecheng',
		'ms' => 'count(mc_courseid) as num',
		'mw' => $where,
		];
		$row = $this->curl->curl_action('api/index', $params);
		return $row;
	}
	// 通过标签获取课程
	public function getCoursesByTag($tagId, $start=0, $limit=20){
		$where = 'c.assignToTop100=1';
		if ($tagId) {
			$where .= ' AND tr.tagId = '.$tagId;
		}
		$getParams = [
		'mm' => 'kecheng',
		'ms' => 'c.praises,c.content,c.auditionvideo,c.title,c.courseid,c.desc,c.comments,c.hits,c.created_at',
		'mw' => $where,
		'mo' => $start,
		"ml" => $limit
		];
		$row = $this->curl->curl_action('api/search-course', $getParams);
		$row = $this->prepareValue($row);
		return $row['data'];
	}
	/**
	 * 用户开始点赞  pc mobile
	 */
	
	public function memberPraise()
	{	
		$date = time();
		$user_id = param::get_cookie('_userid');
		$isParised = $this->isParised($_GET[data][pkId],$user_id);
		if($isParised){
			$status = 0;
			$message = "你已经点过赞了";
		}else{
			$url = str_replace('&', 'and',$_GET[data][url]);
			$res = $this->parise($_GET[data][pkId],$user_id,$url);
			if($res){
				$status = 1;
				$message = "点赞成功";
			}else{
				$status = 0;
				$message = "点赞失败";
			}
		}
		$dataArr = 0;
		$data['status'] = $status;
		$data['message'] = $message;
		$data['data'] = $dataArr;
		echo json_encode($data);
	}
	/**
	 * 用户开始点赞
	 */
	public function parise($courseId,$userId,$url)
	{
		$data = [
		'dianzan' => [
			'mp_pkId'=>$courseId,
			'mp_userId'=>$userId,
			'mp_time'=>time(),
			'mp_modelId'=>4,
			'mp_url'=>htmlspecialchars($url)
			]	
		];
		$res = $this->curl->curl_action('user-api/add-user-praise',$data);
		return $res['errno']=='0'?true:false;
	}
	/**
	 * 判断是否已经点过赞了
	 */
	public function isParised($courseId,$userId)
	{
		$request = [
            'mm' => 'dianzan',
            'mw' => ['mp_pkId'=>$courseId,'mp_userId'=>$userId],
            'ms' => '*',
	    ];
	    $res = $this->curl->curl_action('api/index', $request);
	    return count($res['data']);
	}
	/**
	 * 用户开始点赞  weixin
	 */
	public function memberPraise_weixin(){
		$date = time();
		$user_id = "35";
		$url = str_replace('&', 'and',$_POST[url]);
		$data = [
		'dianzan' => [
		'mp_pkId'=>$_POST[case_id],
		'mp_userId'=>$user_id,
		'mp_time'=>time(),
		'mp_modelId'=>4,
		'mp_url'=>htmlspecialchars($url)
		]	
		];
		$return = $this->curl->curl_action('user-api/add-user-praise',$data);
		if($return[errno]=='0'){
			$_SESSION['courseid'][] = $_GET[data][pkId];
			echo 1;
		}else{
			echo 0;
		}
	}

	private function _session_start() {
		$session_storage = 'session_'.pc_base::load_config('system','session_storage');
		pc_base::load_sys_class($session_storage);
	}
	public function footerClose()
	{
		$_SESSION['footerBar'] = 'close';
		echo $_SESSION['footerBar'];
	}


}
?>