<?php
pc_base::load_app_class('user', 'api');
class myCaseList  extends user {
  
    public $pageSize = 10;
    public function init() {
        $user_id = $this->user_id;
        $request = [
            'mm' => 'anlitijiao',
            'mw' => ['mcs_userid'=>$user_id],
            'ms' => '*',
            'mr' => [
            'huiyuanxinxi' => [
                        'mm' => 'huiyuanxinxi',
                        'mw' => ['mmi_userId' => $user_id],
                        'ms' => 'mmi_name,mmi_userId,mmi_company,mmi_position,mmi_thumbs'
                    ]
            ]
        ];
        $return = $this->curl->curl_action('api/index',$request);
        $dataResult = $this->page($return['data'], $this->pageSize);
        $courseLecturer = $dataResult['data'];
        foreach ($courseLecturer[0]['memberInfo'] as $k => $v) {
            if($k != 'id'){
                   $courseLecturer[0][$k] = $v;
                   unset($courseLecturer[0]['memberInfo'][$k]);
            }
         
        }
       if(empty($courseLecturer[0])){
          unset($courseLecturer);
       }else{
            $html = $dataResult['html'];
       }
    
        $canReview  = $this->isProducer();
        // 右侧推荐案例
        $position = $this->getPosition(['assignToTop100' => 1]);
        
        include template("api","myCaseList");
    }
    
    //到达案例评审
    public function toCaseReview(){
        $data['anlitijiao']['mcs_userid'] =  $this->user_id;
        $return_data = $this->curl->curl_action('user-api/get-case-submit-review-list',$data);
        for ($i=0; $i <count($return_data['data']) ; $i++) { 
            if($return_data['data'][$i]['caseStatus'] == 4){
                $return['data'][] = $return_data['data'][$i];
            }
        }
        if($_GET['currPage'] == ""){
             $currPage = 1;
        }else{
            $currPage = $_GET['currPage'];
        }

        if(empty($return['data'])){
            $courseLecturer = null ;
        }else{
              $courseLecturer = $return['data'];
               $dataResult = $this->page($return['data'], $this->pageSize);
              $dataResult['data'] = array_slice($return['data'],($currPage-1)*$this->pageSize,$this->pageSize);
                $courseLecturer = $dataResult['data'];
                $html = $dataResult['html'];                
        }
        $canReview  = $this->isProducer();
        // 右侧推荐案例
        $position = $this->getPosition(['assignToTop100' => 1]);
        include template("api","myCaseList");
    }
    //这个是出品人 已选案例
    public function selected()
    {
        $user_id = $this->user_id;
        $time = time();
         $request = [
                'mm' => 'anlitijiao',
                'mw' => ['mcs_producerId'=>$user_id,'mcs_createdat'=>'<'.$time],
                'mr' => [
                    'huiyuanxinxi' => [
                                'mm' => 'huiyuanxinxi',
                                'ms' => 'mmi_name,mmi_userId,mmi_company,mmi_position,mmi_thumbs'
                            ]
                    ]
            ];
        $return = $this->curl->curl_action('api/index',$request);
        $len = count($return['data']);
        for ($i=0; $i < $len; $i++) { 
            $return['data'][$i]['name'] = $return['data'][$i]['memberInfo']['name'];
            $return['data'][$i]['company'] = $return['data'][$i]['memberInfo']['company'];
            $return['data'][$i]['position'] = $return['data'][$i]['memberInfo']['position'];
        }
        if(empty($return['data'])){
            $courseLecturer = null ;
        }else{
              $courseLecturer = $return['data'];
               $dataResult = $this->page($return['data'], $this->pageSize);
                $courseLecturer = $dataResult['data'];
                $html = $dataResult['html'];                
        }
        $canReview  = $this->isProducer();
        // 右侧推荐案例
        $position = $this->getPosition(['assignToTop100' => 1]);
        include template("api","myCaseList");
    }
    //出品人查看自己选择的案例
    public function MySelectCaseSubmitDetail(){
       if (!$_GET['case_id']) {
            showmessage('错误的请求', '/index.php?m=content&c=index&a=lists&catid=11');
        }
          $user_id = $this->user_id;
          $request = [
                'mm' => 'anlitijiao',
                'mw' => ['mcs_id'=>$_GET['case_id']],
                'ms' => '*',
                'mr' => [
                    'shenpiyijian' => [
                         'mm' => 'shenpiyijian',
                         'mw' => ['myj_caseId'=>$_GET['case_id']],
                         'ms' => 'myj_caseId,myj_content,myj_adviceDate',
                         'mr' => [
                            'huiyuanxinxi' => [
                                'mm' => 'huiyuanxinxi',
                                'ms' => 'mmi_name,mmi_userId,mmi_company,mmi_position,mmi_thumbs'
                            ]
                         ]     
                    ]
                ]
            ];
        $return = $this->curl->curl_action('api/index',$request);
        // p( $return);
        $courseLecturer = $return[data];
        //获得这个案例所属人的相关消息
        $case_user_id = $courseLecturer[0]['user_id'];
        $params = [
                    'mm' => 'huiyuanxinxi',
                    'mw' => ['mmi_userId'=>$case_user_id],
                    'ms'=>'id,name,company,position'
            ];
        $row = $this->curl->curl_action('api/index', $params);
        $courseLecturer[0]['memberInfo'] = $row['data'][0];
        // p($courseLecturer);

        //获得当前用户的相关信息
        $user_id = $this->user_id;
        $params = [
                    'mm' => 'huiyuanxinxi',
                    'mw' => ['mmi_userId'=>$user_id],
                    'ms'=>'*'
            ];
        $row = $this->curl->curl_action('api/index', $params);

        $courseLecturer[0]['name'] = $row['data'][0]['name'];
        $courseLecturer[0]['company'] = $row['data'][0]['company'];
        $courseLecturer[0]['position'] = $row['data'][0]['position'];
   
        // 右侧推荐案例
        $position = $this->getPosition(['assignToTop100' => 1]);
        //这个用来获得出品人还有多少名额
        $time = time();
         $request = [
                'mm' => 'anlitijiao',
                'mw' => ['mcs_producerId'=>$user_id,'mcs_createdat'=>'< '.$time],
            ];
        $caseSubmitData = $this->curl->curl_action('api/index', $request);
        $userd_num = count($caseSubmitData['data']);
        $left_num = 7 - $userd_num;
        include template("api","MySelectCaseSubmitDetail");
    }

    //进行评审
    public function caseReview(){
       if (!$_GET['case_id']) {
            showmessage('错误的请求', '/index.php?m=content&c=index&a=lists&catid=11');
        }
          $user_id = $this->user_id;
          $request = [
                'mm' => 'anlitijiao',
                'mw' => ['mcs_id'=>$_GET['case_id']],
                'ms' => '*',
                'mr' => [
                    'shenpiyijian' => [
                         'mm' => 'shenpiyijian',
                         'mw' => ['myj_caseId'=>$_GET['case_id']],
                         'ms' => 'myj_caseId,myj_content,myj_adviceDate',
                         'mr' => [
                            'huiyuanxinxi' => [
                                'mm' => 'huiyuanxinxi',
                                'ms' => 'mmi_name,mmi_userId,mmi_company,mmi_position,mmi_thumbs'
                            ]
                         ]     
                    ]
                ]
            ];
        $return = $this->curl->curl_action('api/index',$request);
        p($return);
        $courseLecturer = $return[data];
        
        //获得这个案例所属人的相关消息
        $case_user_id = $courseLecturer[0]['user_id'];
        $params = [
                    'mm' => 'huiyuanxinxi',
                    'mw' => ['mmi_userId'=>$case_user_id],
                    'ms'=>'id,name,company,position'
            ];

        $row = $this->curl->curl_action('api/index', $params);

        $courseLecturer[0]['memberInfo'] = $row['data'][0];
        // p($courseLecturer);

        //获得当前用户的相关信息
        $user_id = $this->user_id;
        $params = [
            'mm' => 'huiyuanxinxi',
            'mw' => ['mmi_userId'=>$user_id],
            'ms'=>'*',
        ];
        $row = $this->curl->curl_action('api/index', $params);
        $courseLecturer[0]['name'] = $row['data'][0]['name'];
        $courseLecturer[0]['company'] = $row['data'][0]['company'];
        $courseLecturer[0]['position'] = $row['data'][0]['position'];
        // 右侧推荐案例
        $position = $this->getPosition(['assignToTop100' => 1]);
        //这个用来获得出品人还有多少名额
        $time = time();
        $request = [
            'mm' => 'anlitijiao',
            'mw' => ['mcs_producerId'=>$user_id,'mcs_createdat'=>'< '.$time],
        ];
        $caseSubmitData = $this->curl->curl_action('api/index', $request);
        
        $request = [
            'mm' => 'chupinren',
            'mw' => ['mpr_venueName'=>"全部会场"],
            'ms' =>'user_id'
        ];
        $fullProducer = $this->curl->curl_action('api/index', $request);
        for ($i=0; $i < count($fullProducer['data']); $i++) { 
            $fullProducerUser_id [] = $fullProducer['data'][$i]['user_id'];
        }
        if(!in_array($user_id,$fullProducerUser_id)){
            $isFullProduer = "1";
        }
        $userd_num = count($caseSubmitData['data']);
        $left_num = 7 - $userd_num;
        include template("api","caseReview");
    }
    //查看自己的案例
    public function caseSee(){
        if (!$_GET[case_id]) {
            showmessage('错误的请求', '/index.php?m=content&c=index&a=lists&catid=11');
        }
        $request = [
            'mm' => 'anlitijiao',
            'mw' => ['mcs_id'=>$_GET[case_id]],
            'ms' => '*',
            'mr' => [
                'shenpiyijian' => [
                     'mm' => 'shenpiyijian',
                     'mw' => ['myj_caseId'=>$_GET[case_id]],
                     'ms' => 'myj_caseId,myj_content,myj_adviceDate',
                     'mr' => [
                        'huiyuanxinxi' => [
                            'mm' => 'huiyuanxinxi',
                            'ms' => 'mmi_name,mmi_userId,mmi_company,mmi_position,mmi_thumbs'
                        ]
                     ]
                ]
            ]
        ];  
        $return = $this->curl->curl_action('api/index',$request);
        $courseLecturer = $return[data];
        //获得当前用户的相关信息
        $user_id = $this->user_id;
        $params = [
                    'mm' => 'huiyuanxinxi',
                    'mw' => ['mmi_userId'=>$this->user_id],
                    'ms'=>'*',
            ];
        $row = $this->curl->curl_action('api/index', $params);

        $courseLecturer[0]['name'] = $row['data'][0]['name'];
        $courseLecturer[0]['company'] = $row['data'][0]['company'];
        $courseLecturer[0]['position'] = $row['data'][0]['position'];
        
        //得到出品人的相关信息
         $params = [
                    'mm' => 'huiyuanxinxi',
                    'mw' => ['mmi_userId'=>$return[data][0]['puser_id']],
                    'ms'=>'*',
        ];
         $produceData = $this->curl->curl_action('api/index', $params);
         $produce = $produceData['data'][0];
        // 右侧推荐案例
        $position = $this->getPosition(['assignToTop100' => 1]);
       
        include template("api","caseSee");
    }
    // 提交反馈意见
    public function addCaseAdvice($value='')
    {
        $review_time = time();
     
         $data = [
            'shenpiyijian' => [
                        'myj_content'=> htmlspecialchars($_POST['caseAdvice']),
                        'myj_caseId'=>$_POST['case_id'],
                        'myj_userId'=>$this->user_id,
                        'myj_adviceDate'=>$review_time
                    ]
        ];
         $return = $this->curl->curl_action('user-api/add-case-submit-advice',$data);
          if($return['data'] = 'succ'){
            showmessage('案例评审成功','/index.php?m=api&c=myCaseList&a=toCaseReview&currPage=1');
          }else{
                showmessage('案例评审失败','/index.php?m=api&c=myCaseList&a=case_review&case_id='.$_POST['case_id']);
          }
    }
    //到达编辑案例的页面
    public function toUpdateCaseSubmit($value='')
    {
        if (!$_GET[case_id]) {
            showmessage('错误的请求', '/index.php?m=content&c=index&a=lists&catid=11');
        }
        $data = [
            'anlitijiao' => [
                        'mcs_userid'=>$this->user_id,
                       'mcs_id'=>$_GET[case_id]
                ]
        ];
         $return = $this->curl->curl_action('user-api/get-case-submit-detail-by-id',$data);
         $courseLecturer = $return['data']['caseSumbit'];
          include template("api","caseUpdate");
    }
    //出品人获得讲师的案例
    public function producerGetCaseSubmit(){
         //获得当前用户的相关信息
        $user_id = $this->user_id;
        $case_id = $_POST['case_id'];
        $request = [
                        'anlitijiao' => [
                            'mcs_id'=>$case_id,
                            'mcs_producerId'=>$user_id,
                            'mcs_caseStatus'=>'3'
                    ]
        ];
        $return = $this->curl->curl_action('user-api/producer-get-case-submit',$request);
        if($return['errno'] == 0){
           echo 1;
        }else{
            echo 0;
        }
    }

    /**
      *@param $originData:全部的数据
      *@param $dataPerPage:每页显示的数据条数
      */
    public function page($originData,$dataPerPage){

        $data = array();
        $page = $dataPerPage   ;// 每页显示的数据条数
        $totalData = count($originData);
        $totalPage = ceil($totalData/$page); //总分页数
        $currentPage = (int)$_GET['currPage'];
        $currentPage = empty($scurrentPage)?1:$currentPage;
        //截取url
        $t = $_SERVER['REQUEST_URI'];
       
        $urlSub = strpos($t,'currPage');
        $url = substr($t,0,$urlSub+9); 
        //每页显示的第一个数据的数组下标
        $data_start = ($currentPage - 1) * $page;
        $count = $totalData;
        //判断是否最后一页
        if($currentPage == $totalPage){
            $page = $totalData-(($currentPage-1)*$page);
        }
        for($i=0;$i<$page;$i++){

            $data[$i] = $originData[$data_start++];
        }
        //显示按钮
        $html = array();
        for($j=1;$j<=$totalPage;$j++){

            if($_GET['currPage']==$j){
                $html[$j]="<span>{$j}</span>";
            }else{
                $html[$j]="<a href='{$url}{$j}'>{$j}</a>";
            }
        }
        if($_GET['currPage']!=1){
            $prev=$_GET['currPage']-1;
            array_unshift($html,"<a href='{$url}{$prev}'>« 上一页</a>");
        }
        if($_GET['currPage']!=$totalPage){
            $next=$_GET['currPage']+1;
            array_push($html,"<a href='{$url}{$next}'>下一页 »</a>");
        }
        $dataResult = array();
        $dataResult = [
            'data'=>$data,
            'html'=>$html
        ];
        return $dataResult;
    } 
}

?>