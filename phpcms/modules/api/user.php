<?php 
/**
 *
 * @link   www.msup.com.cn
 * @author stromKnight <410345759@qq.com>
 * @since  1.0 
 */

// defined('IN_PHPCMS') or exit('No permission resources.');
pc_base::load_app_class('api', 'api');
pc_base::load_sys_class('param');
include_once($_SERVER['DOCUMENT_ROOT'].'/cas/classes/phpCAS.php');
class user extends api{
	public function __construct(){
		parent::__construct();
		$this->_session_start();
	}

	//检测用户是否登录
	public function isLogin()
	{
		//判断用户是否登录
		$use_id = param::get_cookie("_userid");
		if(empty($use_id)){
			$isLogin = $use_id;
		}else{
			$isLogin = "11";
		}
		echo $isLogin ;
	}
	/**
	 * 设置单点登陆的客户端
	 * @AuthorHTL
	 * @DateTime  2016-04-15T16:43:04+0800
	 */
	public function setClient()
	{
		phpCAS::client(CAS_VERSION_2_0, 'vip.msup.com.cn', 443,'index.php/passport');
	}
	/**
	 * 到达登陆页面
	 * @AuthorHTL
	 * @DateTime  2016-04-15T16:46:01+0800
	 * @return    [type]                   [description]
	 */
	public function toLogin(){
		$ticket = $_GET['ticket'];
		if($ticket){
			$url = "http://vip.msup.com.cn/index.php/passport/service-validate?ticket=".$ticket;
//			$res = file_get_contents($url);
//			$res = substr(trim($res),1,strlen($res)-3);
//			$userInfo = json_decode($res,true)['data'];




			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => "http://vip.msup.com.cn/index.php/passport/service-validate?ticket=".$ticket,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			$userInfo = json_decode(substr($response,1,strlen($response)-2),'array')['data'];
			$userInfoArr = json_decode($userInfo,true);
			$this->saveUserInfo($userInfoArr);
			$getComeFromUrl = param::get_cookie('comeFromUrl');
			param::set_cookie('comeFromUrl', $comeFromUrl, SYS_TIME-114400);
			$getComeFromUrl = $getComeFromUrl == ""?"http://".$_SERVER['SERVER_NAME']:$getComeFromUrl;

			echo "<script language=\"javascript\">parent.location.href='".$getComeFromUrl."';</script>";
		}else{
			
			$comeFromUrl = $_SERVER['HTTP_REFERER'];
			if(strstr($comeFromUrl, 'ticket')){
				$comeFromUrl = "http://".$_SERVER['SERVER_NAME'];
			}
			$comeFromUrl = $comeFromUrl == ""?"http://".$_SERVER['SERVER_NAME']:$comeFromUrl;
			param::set_cookie('comeFromUrl', $comeFromUrl, SYS_TIME+114400);
			
			phpCAS::setDebug();
			$this->setClient();
			phpCAS::setServerLoginUrl("");
	       	phpCAS::setNoCasServerValidation();
	       	phpCAS::handleLogoutRequests();
			if(phpCAS::checkAuthentication()){
				$username=phpCAS::getUser();
			}else{
				phpCAS::forceAuthentication();
			}
		}
	}
	//保存用户登陆信息
	public function saveUserInfo($data)
	{
		$cookietime = SYS_TIME+114400;
		param::set_cookie('_userid', $data['id'], $cookietime);
		param::set_cookie('_username', $data['username'], $cookietime);
		param::set_cookie('_name', $data['name'], $cookietime);
		param::set_cookie('thumbs', $data['thumbs'], $cookietime);
	}
	//用户退出
	public function logout() {
		$this->clearUserInfo();
		$this->setClient();
		phpCAS::logout(['service' => "http://".$_SERVER['SERVER_NAME']]);
	}
	//用户退出操作
	public function clearUserInfo($value='')
	{
		param::set_cookie('_userid', '');
		param::set_cookie('_username', '');
		param::set_cookie('_name', '');
		param::set_cookie('thumbs', "");
	}
	// 处理服务端退出登录请求
    public function handleLogoutRequest() 
    {
        phpCAS::setDebug();
        $this->setClient();
        phpCAS::setServerLoginUrl("http://vip.msup.com.cn/index.php/passport/login-popup");
       	phpCAS::setNoCasServerValidation();
       	phpCAS::handleLogoutRequests();
        phpCAS::handleLogoutRequests(true, ['121.42.46.217']);
    }
	//异步请求 获得用户的信息
	public function getUserInfo(){
        $request = [
                'mm' => 'anlitijiao',
                'mw' => ['mcs_courseid'=>$_POST['case_id'],'time'=>time()],
                'ms' => 'mcs_courseDescription,mcs_courseTitle,mcs_lecturerName,mcs_lecturerDescription,mcs_lecturerThumbs,mcs_lecturerPosition,mcs_companyName',
            ];
        $return = $this->curl->curl_action('api/index',$request);
        $content = $return['data'][0]['courseDescription'];
        if(mb_strlen($content,'utf-8')>150){
        	$content =  mb_substr($content,0,150,'utf-8')."...";
        }
        $return['data'][0]['courseDescription'] = $content;
      	$request = [
            'mm' => 'dianzan',
            'mw' => ['mp_pkId'=>$_POST['case_id'],'time'=>time()],
            'ms' => '*',
        ];
        $zan_data = $this->curl->curl_action('api/index', $request);
        $zan_count = count($zan_data[data]);
 		$return['data'][0]['dianzan'] = $zan_count;
        echo json_encode($return['data'][0]);
	}

	public function phone_is_exist()
	{
		$phone = $_GET[data];
		$request = [ 'huiyuan' => ['mu_phone' => $phone]];
		$return = $this->curl->curl_action('user-api/check-user-is-exsited',$request);
		if($return[errno] != 0){
			//这个是手机还被注册过了
			echo 0;
		}else{
			//手机号没被注册
			echo 1;
		}
	}
	public function personMessage($value='')
	{	
		$cookietime = SYS_TIME+114400;
		param::set_cookie('registerPhone', $_POST['phone'], $cookietime);
		param::set_cookie('smsCode', $_POST['smsCode'], $cookietime);
		include template("member", "personMessage");
	}
	// 注册
	public function register() {

		if ($_POST['doSubmit']) {
			$_POST['huiyuan']['mu_phone'] = param::get_cookie('registerPhone');
			$_POST['huiyuan']['mu_smsCode'] = param::get_cookie('smsCode');
			$memberInfo = array_merge($_POST['huiyuan'],$_POST['huiyuanxinxi']);
			$return = $this->registerMember($memberInfo);
			if (!$return['errno']) {
				$get_cookietime = param::get_cookie('cookietime');
				$_cookietime =  $get_cookietime ? $get_cookietime : 0;
				$cookietime = $_cookietime ? SYS_TIME + $_cookietime : 0;
				param::set_cookie('_userid', $return['data']['id'], $cookietime);
				param::set_cookie('_username', $return['data']['username'], $cookietime);
				showmessage('注册成功，请登录', '/index.php?m=member&c=index&a=login');
			} else {
				showmessage($return['errmsg']);
			}
		} else {
			include template("member", "register");
		}
	}
	/**
     * 到达修改个人案例分享页面
     * @param   [description]
     * @return        [description]
     */
	public function toShare(){
		// index.php?m=api&c=user&a=toShare&userId=138
		if (empty($_GET['userId']) || !preg_match("/^[0-9]*$/", $_GET['userId']))  showmessage('您访问的页面不存在','index.php');
		$request = [
                'mm' => 'anlitijiao',
                'mw' => ['mcs_userid'=>$_GET['userId']],
                'ms' => 'mcs_courseContent,mcs_courseTitle,mcs_lecturerName,mcs_lecturerDescription,mcs_lecturerThumbs,mcs_lecturerPosition,mcs_courseid',
            ];
        $return = $this->curl->curl_action('api/index',$request);
        $data = $return['data'][0];
        $imgArr = $data['lecturerThumbs'];
        $imgArr = json_decode($imgArr);
        $data['img'] = $imgArr[0]->fileUrl;
        // cutstr
        if(strlen($data['courseContent'])>200){
        	$data['courseContent'] =   mb_substr($data['courseContent'],0,200,'utf-8')."....";
        }
        if(strlen($data['lecturerDescription'])>90){
	        $data['lecturerDescription'] = mb_substr($data['lecturerDescription'],0,90,'utf-8')."....";
        }
        // p($data);
		include template("member", "share");

	}
	 /**
     * 到达修改个人资料页面
     * @param   [description]
     * @return        [description]
     */
    public function toChangeMessage(){
    	$user_id =  param::get_cookie('_userid');
    	$params = [
                'mm' => 'huiyuan',
                'mw' => ['mu_id'=>$user_id,'time'=>time()],
                'ms'=>'id,phone,email',
                'mr'=>[
                	'huiyuanxinxi'=>[
                		'mm' => 'huiyuanxinxi',
                		'ms'=>'id,name,company,position,sex,phone,thumbs',
                	]
                ]
        ];
        $row = $this->curl->curl_action('api/index', $params);
    	$data = $row['data'][0]; 
    	$data['memberInfo']['thumbs'] = json_decode($data['memberInfo']['thumbs']);
    	// $cookietime = SYS_TIME+114400;
    	// param::set_cookie('name', $data['memberInfo']['name'],$cookietime);
    	$data['memberInfo']['img'] = $data['memberInfo']['thumbs'][0]->fileUrl;
    	if($data['memberInfo']['img']==""){
    		$data['memberInfo']['img'] = '/statics/css/top100/img/shangchuang200.jpg';
    	}
        include template("member", "changeMessage");
    }
    /**
     * 修改后台个人数据
     * @param   [description]
     * @return        [description]
     */
    public function changeMessage(){
    	// p($_POST);
    	$memberInfo = array_merge($_POST['huiyuan'],$_POST['huiyuanxinxi']);
    	$attributes = $this->attributeLabelsBychangMessage();
		$postInfo = [ 'huiyuan' => [], 'huiyuanxinxi' => [] ];
		// 检查错误信息
		foreach($attributes as $attribute => $label) {
			
			if (empty($memberInfo[$attribute])) {
				$return['errmsg'] = $label.'不能为空';
				$return['errno'] = 1;
				showmessage($label.'不能为空');
			} else {
				$pos = substr($attribute,0,strpos($attribute, '_'));
				switch($pos) {
					case 'mu':
						$postInfokey = 'huiyuan';
					break;
					case 'mmi':
						$postInfokey = 'huiyuanxinxi';
					break;
					default:
					break;
				}
				$postInfo[$postInfokey][$attribute] = $memberInfo[$attribute];
			}
		}
		$postInfo['huiyuanxinxi']['mmi_sex'] =	$_POST['huiyuanxinxi']['mmi_sex'];
		$postInfo['huiyuan']['mu_phone'] =	$_POST['huiyuan']['mu_phone'];
		if(!empty($_POST['src'])){
			$arr = $_POST;
        	$MemberFace = $this->sliceBanner($arr);
        	$postInfo['huiyuanxinxi']['mmi_thumbs'] = $this->formatUploadImg($MemberFace,$MemberFace);
        }
        $return = $this->curl->curl_action('user-api/edit',$postInfo);
        if($return['errno'] == 0){
        	showmessage('修改成功', '/index.php?m=api&c=user&a=toChangeMessage');
        }else{
        	showmessage('修改失败');
        }
    }

    public function formatUploadImg($fileUrl, $fileName = '', $thumbnailUrl ='') {
        if (!$fileName) $fileName = $this->getRandChar(8);
        return json_encode(
                [
                    [
                        'fileName' => $fileName,
                        'fileUrl'  => APP_PATH.$fileUrl,
                        'thumbnailUrl' => $thumbnailUrl,
                    ]
                ]
            );
    }
    public function sliceBanner($arr){
        $x = (int)$arr['x'];
        $y = (int)$arr['y'];
        $w = (int)$arr['w'];
        $h = (int)$arr['h'];
        $pic = $arr['src'];
        //剪切后小图片的名字
        $str = explode(".",$pic);//图片的格式
        $type = $str[1]; //图片的格式
        $filename = $this->getRandChar(7).".". $type; //重新生成图片的名字
        $uploadBanner = $pic;
        //创建文件夹
        $dir ='uploadfile/'.date("Y").'/'.date('md');
        @chmod($dir,0777);//赋予权限
        @is_dir($dir) or mkdir($dir,0777);
        $sliceBanner = $dir."/".$filename;
        //创建图片
        $src_pic = $this->getImageHander($uploadBanner);
        $dst_pic = imagecreatetruecolor($w, $h);
        imagecopyresampled($dst_pic,$src_pic,0,0,$x,$y,$w,$h,$w,$h);
        imagejpeg($dst_pic, $sliceBanner);
        imagedestroy(APP_PATH.$src_pic);
        imagedestroy(APP_PATH.$dst_pic);
        //删除已上传未裁切的图片
        if(is_file($_SERVER['DOCUMENT_ROOT'].$pic)){
            unlink($_SERVER['DOCUMENT_ROOT'].$pic);
        }
        //返回新图片的位置
        return $sliceBanner;
    }
    public function getRandChar($length) {
        return random($length, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz');
    }
    function  substr_cn($string_input,$start,$length) 
{ 
    /* 功能: 
     * 此算法用于截取中文字符串 
     * 函数以单个完整字符为单位进行截取,即一个英文字符和一个中文字符均表示一个单位长度 
     * 参数: 
     * 参数$string为要截取的字符串, 
     * 参数$start为欲截取的起始位置, 
     * 参数$length为要截取的字符个数(一个汉字或英文字符都算一个) 
     * 返回值: 
     * 返回截取结果字符串 
     * */ 
    $str_input=$string_input; 
    $len=$length; 
    $return_str=""; 
    //定义空字符串 
    for ($i=0;$i<2*$len+2;$i++) 
        $return_str=$return_str." "; 
    $start_index=0; 
    //计算起始字节偏移量 
    for ($i=0;$i<$start;$i++) 
    { 
        if (ord($str_input{$start_index}>=161))          //是汉语      
        { 
            $start_index+=2; 
        } 
        else                                          //是英文 
        { 
            $start_index+=1; 
        }         
    }     
    $chr_index=$start_index; 
    //截取 
    for ($i=0;$i<$len;$i++) 
    { 
        $asc=ord($str_input{$chr_index}); 
        if ($asc>=161) 
        { 
            $return_str{$i}=chr($asc); 
            $return_str{$i+1}=chr(ord($str_input{$chr_index+1})); 
            $len+=1; //结束条件加1 
            $i++;    //位置偏移量加1 
            $chr_index+=2; 
            continue;             
        } 
        else  
        { 
            $return_str{$i}=chr($asc); 
            $chr_index+=1; 
        } 
    }     
    return trim($return_str); 
}//end of substr_cn

    //初始化图片
    public function getImageHander ($url) {
        $url = APP_PATH.$url;
        $size=@getimagesize($url);

        switch($size['mime']){
            case 'image/jpeg': $im = imagecreatefromjpeg($url);break;
            case 'image/gif' : $im = imagecreatefromgif($url);break;
            case 'image/png' : $im = imagecreatefrompng($url);break;
            default: $im=false;break;
        }
        return $im;
    }
    /**
	 * 注册会员
	 * @param  array  $memberInfo [description]
	 * @return [type]             [description]
	 */
	public function registerMember(array $memberInfo) {
		$attributes = $this->attributeLabels();
		$postInfo = [ 'huiyuan' => [], 'huiyuanxinxi' => [] ];
		// 检查错误信息
		foreach($attributes as $attribute => $label) {
			
			if (empty($memberInfo[$attribute])) {
				$return['errmsg'] = $label.'不能为空';
				$return['errno'] = 1;
				showmessage($label.'不能为空');
			} else {
				$pos = substr($attribute,0,strpos($attribute, '_'));
				switch($pos) {
					case 'mu':
						$postInfokey = 'huiyuan';
					break;
					case 'mmi':
						$postInfokey = 'huiyuanxinxi';
					break;
					default:
					break;
				}
				$postInfo[$postInfokey][$attribute] = $memberInfo[$attribute];
			}
		}
		$return = $this->curl->curl_action('user-api/register',$postInfo);
		return $return;
	}
	private function attributeLabels($attribute = null){
		$attributeLabels = [
			'mu_phone' => '手机号',
			'mu_email' => '邮箱',
			'mu_smsCode'=>'手机验证码',
			'mu_password' => '登录密码',
			'mmi_name' => '姓名',
			'mmi_company' => '所在公司',
			'mmi_position' => '所在职位',
		];
		if ($attribute && in_array($attribute, array_keys($attributeLabels))){
			return $attributeLabels[$attribute];
		} else {
			return $attributeLabels;
		}

	}
	private function attributeLabelsBychangMessage($attribute = null){
		$attributeLabels = [
			'mmi_phone' => '手机号',
			'mu_email' => '邮箱',
			'mmi_name' => '姓名',
			'mmi_company' => '所在公司',
			'mmi_position' => '所在职位',
		];
		if ($attribute && in_array($attribute, array_keys($attributeLabels))){
			return $attributeLabels[$attribute];
		} else {
			return $attributeLabels;
		}

	}
	private function _session_start() {
		$session_storage = 'session_'.pc_base::load_config('system','session_storage');
		pc_base::load_sys_class($session_storage);
		session_start();
	}
	/**
	 * 只获得头部
	 */
	public function header($value='')
	{
		include template("content", "header");
	}
}	


?>
