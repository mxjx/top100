<?php
defined( 'IN_PHPCMS' ) or exit( 'No permission resources.' );
//模型缓存路径
define( 'CACHE_MODEL_PATH', CACHE_PATH . 'caches_model' . DIRECTORY_SEPARATOR . 'caches_data' . DIRECTORY_SEPARATOR );
pc_base::load_app_func( 'util', 'content' );
pc_base::load_app_class( 'api', 'api' );
pc_base::load_app_class( 'jssdk', 'api' );
pc_base::load_app_class( "HttpService", 'api' );

class index extends api
{
	private $db;

	function __construct ()
	{
		$this->curl = new curl();
		$this->db = pc_base::load_model( 'content_model' );
		$this->_userid = param::get_cookie( '_userid' );
		$this->_username = param::get_cookie( '_username' );
		$this->_groupid = param::get_cookie( '_groupid' );
	}

	function prepareValue ( $row )
	{
		foreach ( $row as &$v ) {
			$v['hits'] = viewed_num( $v['created_at'], $v['hits'] );
		}
		return $row;
	}

	/**
	 * 得到首页的案例数据
	 *
	 * @AuthorHTL
	 * @DateTime  2016-04-14T14:15:03+0800
	 * @return    [type]                   [description]
	 */
	public function getCaseData ()
	{
		$request = [
			'mm' => 'bangdankecheng',
			'mw' => ['mbc_listId' => 4]
		];
		$return_entry_res = $this->curl->curl_action( '/Top100/top100-api/index', $request );
		$nums = array_rand( $return_entry_res['data'], 9 );
		for ( $i = 0; $i < 9; $i++ ) {
			$cases[] = $return_entry_res['data'][$nums[$i]];
		}
		return $cases;
	}

	public function getProducerLists ( $year = null )
	{
		$con = 'mpr_venue != 0';
		if ( !empty( $year ) ) {
			$con .= ' AND mpr_year =' . $year;
		}
		$request = [
			'mm' => 'chupinren',
			'mw' => $con,
			'mr' => [
				'huiyuanxinxi' => [
					'mm' => 'huiyuanxinxi'
				]
			]
		];
		$res = $this->curl->curl_action( 'api/index', $request );
		$data = $res['data'];
		if ( empty( $data ) ) {
			return null;
		}
		foreach ( $data as $k => $v ) {
			$result[$k]['id'] = $v['id'];
			$result[$k]["user_id"] = $v["user_id"];
			$result[$k]["venue"] = $v["venue"];
			$result[$k]["venueName"] = $v["venueName"];
			$result[$k]["year"] = $v["year"];

			$info = $v["memberInfo"];
			$result[$k]["name"] = $info['name'] ?: '';
			$result[$k]["company"] = $info['company'] ?: '';
			$result[$k]["position"] = $info['position'] ?: '';
			$thumb = $info['thumbs'] ? json_decode( $info['thumbs'] )[0]->fileUrl : 'https://mrm.msup.com.cn/Public/Admin/images/default_headpic.png';
			$result[$k]["thumb"] = $thumb;
			$desc = $v["description"] ?: $info["description"];
			$result[$k]["description"] = $desc ?: '';

		}
		return $result;
	}

	/**
	 * 得到文章列表
	 */
	public function getArticles ()
	{
		$request = [
			'mm' => 'article',
			'ms' => 'ma_id,ma_digest,ma_title'
		];
		$return = $this->curl->curl_action( 'api/index', $request );
		return $return['data'];
	}

	public function init ()
	{
		$cases = $this->getCaseData();
		$producers = $this->getProducerLists( 2018 );
		$articles = $this->getArticles();
		// p($articles);
		if ( isset( $_GET['siteid'] ) ) {
			$siteid = intval( $_GET['siteid'] );
		} else {
			$siteid = 1;
		}
		$siteid = $GLOBALS['siteid'] = max( $siteid, 1 );
		define( 'SITEID', $siteid );
		$_userid = $this->_userid;
		$_username = $this->_username;
		$_groupid = $this->_groupid;
		//SEO
		$SEO = seo( $siteid );
		$sitelist = getcache( 'sitelist', 'commons' );
		$default_style = $sitelist[$siteid]['default_style'];
		$CATEGORYS = getcache( 'category_content_' . $siteid, 'commons' );

		$htmlTags = $this->getHtmlTags();
		include template( 'content', 'index', $default_style );
	}

	public function test ()
	{
		$cases = $this->getCaseData();
		$producers = $this->getProducerLists( 2017 );
		$articles = $this->getArticles();
		// p($articles);
		if ( isset( $_GET['siteid'] ) ) {
			$siteid = intval( $_GET['siteid'] );
		} else {
			$siteid = 1;
		}
		$siteid = $GLOBALS['siteid'] = max( $siteid, 1 );
		define( 'SITEID', $siteid );
		$_userid = $this->_userid;
		$_username = $this->_username;
		$_groupid = $this->_groupid;
		//SEO
		$SEO = seo( $siteid );
		$sitelist = getcache( 'sitelist', 'commons' );
		$default_style = $sitelist[$siteid]['default_style'];
		$CATEGORYS = getcache( 'category_content_' . $siteid, 'commons' );

		$htmlTags = $this->getHtmlTags();
		include template( 'content', 'index2017', $default_style );
	}

	private function _session_start ()
	{
		$session_storage = 'session_' . pc_base::load_config( 'system', 'session_storage' );
		pc_base::load_sys_class( $session_storage );
		session_start();
	}

	/**
	 *获得验证的字段
	 */
	public function getAttrLables ()
	{
		return [
			'姓名'   => 'name',
			'性别'   => 'sex',
			'手机号码' => 'phone',
			'电子邮件' => 'email',
			'公司名称' => 'company',
			'公司职位' => 'position',
			'购票类型' => 'ticketType_day',
			'信息来源' => 'ticketType_comeform',
			'验证码'  => 'code'
		];
	}

	//首页发送邮件
	public function send ()
	{
		$this->_session_start();
		foreach ( $_POST as $key => $value ) {
			if ( empty( $value ) ) {
				showmessage( array_search( $key, $this->getAttrLables() ) . "不能为空" );
			}
		}
		$arr = $_POST;
		if ( $_SESSION['code'] != strtolower( $_POST['code'] ) ) {
			showmessage( L( 'code_error' ) );
		}
		$email = trim( $arr['email'] );
		$title = '壹佰案例';
		$intention = 'Top100';
		$msg = $this->getEdm();
		$res = sedEmail( $email, $arr, $title, $msg );
		if ( $res == true ) {
			$this->setIntentionEmail( $title, $intention, $email );//入库
			echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
			echo '<script language="javascript" charset="gbk">';
			echo "location.href='http://www.top100summit.com';";
			echo 'alert("发送成功!请注意查收");';
			echo '</script>';
		} else {
			echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
			echo '<script language="javascript" charset="gbk">';
			echo "location.href='http://www.top100summit.com';";
			echo 'alert("发送失败!");';
			echo '</script>';
		}

	}

	public function setIntentionEmail ( $title, $intention, $email )
	{
		$json = [
			"mm"  => "intention_email",
			"mrq" => [
				"ie_title"     => $title,
				"ie_intention" => $intention,
				"ie_email"     => $email,
			]
		];
		return HttpService::http( HttpService::$create_intention_email, $json, 0 );
	}

	public function getEdm ()
	{
		$url = 'http://www.top100summit.com/edm/2017/top100v1';
		$html = file_get_contents( $url );
		return $html;
	}

	//内容页
	public function show ()
	{
		$catid = intval( $_GET['catid'] );
		$id = intval( $_GET['id'] );

		if ( !$catid || !$id ) showmessage( L( 'information_does_not_exist' ), 'blank' );
		$_userid = $this->_userid;
		$_username = $this->_username;
		$_groupid = $this->_groupid;

		$page = intval( $_GET['page'] );
		$page = max( $page, 1 );
		$siteids = getcache( 'category_content', 'commons' );
		$siteid = $siteids[$catid];
		$CATEGORYS = getcache( 'category_content_' . $siteid, 'commons' );

		if ( !isset( $CATEGORYS[$catid] ) || $CATEGORYS[$catid]['type'] != 0 ) showmessage( L( 'information_does_not_exist' ), 'blank' );
		$this->category = $CAT = $CATEGORYS[$catid];
		$this->category_setting = $CAT['setting'] = string2array( $this->category['setting'] );
		$siteid = $GLOBALS['siteid'] = $CAT['siteid'];

		$MODEL = getcache( 'model', 'commons' );
		$modelid = $CAT['modelid'];

		$tablename = $this->db->table_name = $this->db->db_tablepre . $MODEL[$modelid]['tablename'];
		$r = $this->db->get_one( ['id' => $id] );
		if ( !$r || $r['status'] != 99 ) showmessage( L( 'info_does_not_exists' ), 'blank' );

		$this->db->table_name = $tablename . '_data';
		$r2 = $this->db->get_one( ['id' => $id] );
		$rs = $r2 ? array_merge( $r, $r2 ) : $r;

		//再次重新赋值，以数据库为准
		$catid = $CATEGORYS[$r['catid']]['catid'];
		$modelid = $CATEGORYS[$catid]['modelid'];

		require_once CACHE_MODEL_PATH . 'content_output.class.php';
		$content_output = new content_output( $modelid, $catid, $CATEGORYS );
		$data = $content_output->get( $rs );
		extract( $data );

		//检查文章会员组权限
		if ( $groupids_view && is_array( $groupids_view ) ) {
			$_groupid = param::get_cookie( '_groupid' );
			$_groupid = intval( $_groupid );
			if ( !$_groupid ) {
				$forward = urlencode( get_url() );
				showmessage( L( 'login_website' ), APP_PATH . 'index.php?m=member&c=index&a=login&forward=' . $forward );
			}
			if ( !in_array( $_groupid, $groupids_view ) ) showmessage( L( 'no_priv' ) );
		} else {
			//根据栏目访问权限判断权限
			$_priv_data = $this->_category_priv( $catid );
			if ( $_priv_data == '-1' ) {
				$forward = urlencode( get_url() );
				showmessage( L( 'login_website' ), APP_PATH . 'index.php?m=member&c=index&a=login&forward=' . $forward );
			} elseif ( $_priv_data == '-2' ) {
				showmessage( L( 'no_priv' ) );
			}
		}
		if ( module_exists( 'comment' ) ) {
			$allow_comment = isset( $allow_comment ) ? $allow_comment : 1;
		} else {
			$allow_comment = 0;
		}
		//阅读收费 类型
		$paytype = $rs['paytype'];
		$readpoint = $rs['readpoint'];
		$allow_visitor = 1;
		if ( $readpoint || $this->category_setting['defaultchargepoint'] ) {
			if ( !$readpoint ) {
				$readpoint = $this->category_setting['defaultchargepoint'];
				$paytype = $this->category_setting['paytype'];
			}

			//检查是否支付过
			$allow_visitor = self::_check_payment( $catid . '_' . $id, $paytype );
			if ( !$allow_visitor ) {
				$http_referer = urlencode( get_url() );
				$allow_visitor = sys_auth( $catid . '_' . $id . '|' . $readpoint . '|' . $paytype ) . '&http_referer=' . $http_referer;
			} else {
				$allow_visitor = 1;
			}
		}
		//最顶级栏目ID
		$arrparentid = explode( ',', $CAT['arrparentid'] );
		$top_parentid = $arrparentid[1] ? $arrparentid[1] : $catid;

		$template = $template ? $template : $CAT['setting']['show_template'];
		if ( !$template ) $template = 'show';
		//SEO
		$seo_keywords = '';
		if ( !empty( $keywords ) ) $seo_keywords = implode( ',', $keywords );
		$SEO = seo( $siteid, $catid, $title, $description, $seo_keywords );

		define( 'STYLE', $CAT['setting']['template_list'] );
		if ( isset( $rs['paginationtype'] ) ) {
			$paginationtype = $rs['paginationtype'];
			$maxcharperpage = $rs['maxcharperpage'];
		}
		$pages = $titles = '';
		if ( $rs['paginationtype'] == 1 ) {
			//自动分页
			if ( $maxcharperpage < 10 ) $maxcharperpage = 500;
			$contentpage = pc_base::load_app_class( 'contentpage' );
			$content = $contentpage->get_data( $content, $maxcharperpage );
		}
		if ( $rs['paginationtype'] != 0 ) {
			//手动分页
			$CONTENT_POS = strpos( $content, '[page]' );
			if ( $CONTENT_POS !== false ) {
				$this->url = pc_base::load_app_class( 'url', 'content' );
				$contents = array_filter( explode( '[page]', $content ) );
				$pagenumber = count( $contents );
				if ( strpos( $content, '[/page]' ) !== false && ($CONTENT_POS < 7) ) {
					$pagenumber--;
				}
				for ( $i = 1; $i <= $pagenumber; $i++ ) {
					$pageurls[$i] = $this->url->show( $id, $i, $catid, $rs['inputtime'] );
				}
				$END_POS = strpos( $content, '[/page]' );
				if ( $END_POS !== false ) {
					if ( $CONTENT_POS > 7 ) {
						$content = '[page]' . $title . '[/page]' . $content;
					}
					if ( preg_match_all( "|\[page\](.*)\[/page\]|U", $content, $m, PREG_PATTERN_ORDER ) ) {
						foreach ( $m[1] as $k => $v ) {
							$p = $k + 1;
							$titles[$p]['title'] = strip_tags( $v );
							$titles[$p]['url'] = $pageurls[$p][0];
						}
					}
				}
				//当不存在 [/page]时，则使用下面分页
				$pages = content_pages( $pagenumber, $page, $pageurls );
				//判断[page]出现的位置是否在第一位 
				if ( $CONTENT_POS < 7 ) {
					$content = $contents[$page];
				} else {
					if ( $page == 1 && !empty( $titles ) ) {
						$content = $title . '[/page]' . $contents[$page - 1];
					} else {
						$content = $contents[$page - 1];
					}
				}
				if ( $titles ) {
					list( $title, $content ) = explode( '[/page]', $content );
					$content = trim( $content );
					if ( strpos( $content, '</p>' ) === 0 ) {
						$content = '<p>' . $content;
					}
					if ( stripos( $content, '<p>' ) === 0 ) {
						$content = $content . '</p>';
					}
				}
			}
		}
		$this->db->table_name = $tablename;
		//上一页
		$previous_page = $this->db->get_one( "`catid` = '$catid' AND `id`<'$id' AND `status`=99", '*', 'id DESC' );
		//下一页
		$next_page = $this->db->get_one( "`catid`= '$catid' AND `id`>'$id' AND `status`=99" );

		if ( empty( $previous_page ) ) {
			$previous_page = [
				'title' => L( 'first_page' ),
				'thumb' => IMG_PATH . 'nopic_small.gif',
				'url'   => 'javascript:alert(\'' . L( 'first_page' ) . '\');'
			];
		}

		if ( empty( $next_page ) ) {
			$next_page = [
				'title' => L( 'last_page' ),
				'thumb' => IMG_PATH . 'nopic_small.gif',
				'url'   => 'javascript:alert(\'' . L( 'last_page' ) . '\');'
			];
		}
		$api = pc_base::load_app_class( 'api', 'api' );
		$position = $api->getPosition( ['assignToTop100' => 1] );
		include template( 'content', $template );
	}

	//列表页
	public function lists ()
	{
//		var_dump(1);die;

		$catid = $_GET['catid'] = intval( $_GET['catid'] );
		$_priv_data = $this->_category_priv( $catid );
		if ( $_priv_data == '-1' ) {
			$forward = urlencode( get_url() );
			showmessage( L( 'login_website' ), APP_PATH . 'index.php?m=member&c=index&a=login&forward=' . $forward );
		} elseif ( $_priv_data == '-2' ) {
			showmessage( L( 'no_priv' ) );
		}
		$_userid = $this->_userid;
		$_username = $this->_username;
		$_groupid = $this->_groupid;

		if ( !$catid ) showmessage( L( 'category_not_exists' ), 'blank' );
		$siteids = getcache( 'category_content', 'commons' );
		$siteid = $siteids[$catid];
		$CATEGORYS = getcache( 'category_content_' . $siteid, 'commons' );
		if ( !isset( $CATEGORYS[$catid] ) ) showmessage( L( 'category_not_exists' ), 'blank' );
		$CAT = $CATEGORYS[$catid];
		$siteid = $GLOBALS['siteid'] = $CAT['siteid'];
		extract( $CAT );
		$setting = string2array( $setting );
		//SEO
		if ( !$setting['meta_title'] ) $setting['meta_title'] = $catname;
		$SEO = seo( $siteid, '', $setting['meta_title'], $setting['meta_description'], $setting['meta_keywords'] );
		define( 'STYLE', $setting['template_list'] );
		$page = intval( $_GET['page'] );

		$template = $setting['category_template'] ? $setting['category_template'] : 'category';
		$template_list = $setting['list_template'] ? $setting['list_template'] : 'list';
		if ( $type == 0 ) {
			$template = $child ? $template : $template_list;
			$arrparentid = explode( ',', $arrparentid );
			$top_parentid = $arrparentid[1] ? $arrparentid[1] : $catid;
			$array_child = [];
			$self_array = explode( ',', $arrchildid );
			//获取一级栏目ids
			foreach ( $self_array as $arr ) {
				if ( $arr != $catid && $CATEGORYS[$arr][parentid] == $catid ) {
					$array_child[] = $arr;
				}
			}
			$arrchildid = implode( ',', $array_child );
			//URL规则
			$urlrules = getcache( 'urlrules', 'commons' );
			$urlrules = str_replace( '|', '~', $urlrules[$category_ruleid] );
			$tmp_urls = explode( '~', $urlrules );
			$tmp_urls = isset( $tmp_urls[1] ) ? $tmp_urls[1] : $tmp_urls[0];
			preg_match_all( '/{\$([a-z0-9_]+)}/i', $tmp_urls, $_urls );
			if ( !empty( $_urls[1] ) ) {
				foreach ( $_urls[1] as $_v ) {
					$GLOBALS['URL_ARRAY'][$_v] = $_GET[$_v];
				}
			}
			define( 'URLRULE', $urlrules );
			$GLOBALS['URL_ARRAY']['categorydir'] = $categorydir;
			$GLOBALS['URL_ARRAY']['catdir'] = $catdir;
			$GLOBALS['URL_ARRAY']['catid'] = $catid;
			include template( 'content', $template );
		} else {
			//单网页
			$this->page_db = pc_base::load_model( 'page_model' );
			$r = $this->page_db->get_one( ['catid' => $catid] );
			if ( $r ) extract( $r );
			$template = $setting['page_template'] ? $setting['page_template'] : 'page';
			$arrchild_arr = $CATEGORYS[$parentid]['arrchildid'];
			if ( $arrchild_arr == '' ) $arrchild_arr = $CATEGORYS[$catid]['arrchildid'];
			$arrchild_arr = explode( ',', $arrchild_arr );
			array_shift( $arrchild_arr );
			$keywords = $keywords ? $keywords : $setting['meta_keywords'];
			$SEO = seo( $siteid, 0, $title, $setting['meta_description'], $keywords );
			include template( 'content', $template );
		}

	}

	public function downloadSupport ()
	{
		$fileName = 'TOP 100 Case Study Of 2018v4.pdf';
		$file_path = dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) . '/statics/2017/doc/' . $fileName;
		if ( file_exists( $file_path ) ) {
			header( 'Content-Description: File Transfer' );
			header( 'Content-Type: application/octet-stream' );
			header( 'Content-Disposition: attachment; filename="' . $fileName . '"' );
			header( 'Expires: 0' );
			header( 'Cache-Control: must-revalidate' );
			header( 'Pragma: public' );
			header( 'Content-Length: ' . filesize( $file_path ) );
			readfile( $file_path );
			exit;
		} else {
			echo "404 file not found";
		}
	}

	//JSON 输出
	public function json_list ()
	{
		if ( $_GET['type'] == 'keyword' && $_GET['modelid'] && $_GET['keywords'] ) {
			//根据关键字搜索
			$modelid = intval( $_GET['modelid'] );
			$id = intval( $_GET['id'] );

			$MODEL = getcache( 'model', 'commons' );
			if ( isset( $MODEL[$modelid] ) ) {
				$keywords = safe_replace( new_html_special_chars( $_GET['keywords'] ) );
				$keywords = addslashes( iconv( 'utf-8', 'gbk', $keywords ) );
				$this->db->set_model( $modelid );
				$result = $this->db->select( "keywords LIKE '%$keywords%'", 'id,title,url', 10 );
				if ( !empty( $result ) ) {
					$data = [];
					foreach ( $result as $rs ) {
						if ( $rs['id'] == $id ) continue;
						if ( CHARSET == 'gbk' ) {
							foreach ( $rs as $key => $r ) {
								$rs[$key] = iconv( 'gbk', 'utf-8', $r );
							}
						}
						$data[] = $rs;
					}
					if ( count( $data ) == 0 ) exit( '0' );
					echo json_encode( $data );
				} else {
					//没有数据
					exit( '0' );
				}
			}
		}

	}

	public function getHtmlTags ()
	{
		return [
			'1' => ['label' => '产品创新/用户体验', 'img' => 'role_1.png'],
			'2' => ['label' => '工程文化/团队增长', 'img' => 'role_2.png'],
			'3' => ['label' => '架构设计/开发流程', 'img' => 'role_3.png'],
			'4' => ['label' => '数据驱动/机器学习', 'img' => 'role_4.png'],
			'5' => ['label' => '测试管理/交付运维', 'img' => 'role_5.png']
		];
	}

	/**
	 * 检查支付状态
	 */
	protected function _check_payment ( $flag, $paytype )
	{
		$_userid = $this->_userid;
		$_username = $this->_username;
		if ( !$_userid ) return false;
		pc_base::load_app_class( 'spend', 'pay', 0 );
		$setting = $this->category_setting;
		$repeatchargedays = intval( $setting['repeatchargedays'] );
		if ( $repeatchargedays ) {
			$fromtime = SYS_TIME - 86400 * $repeatchargedays;
			$r = spend::spend_time( $_userid, $fromtime, $flag );
			if ( $r['id'] ) return true;
		}
		return false;
	}

	/**
	 * 检查阅读权限
	 *
	 */
	protected function _category_priv ( $catid )
	{
		$catid = intval( $catid );
		if ( !$catid ) return '-2';
		$_groupid = $this->_groupid;
		$_groupid = intval( $_groupid );
		if ( $_groupid == 0 ) $_groupid = 8;
		$this->category_priv_db = pc_base::load_model( 'category_priv_model' );
		$result = $this->category_priv_db->select( ['catid' => $catid, 'is_admin' => 0, 'action' => 'visit'] );
		if ( $result ) {
			if ( !$_groupid ) return '-1';
			foreach ( $result as $r ) {
				if ( $r['roleid'] == $_groupid ) return '1';
			}
			return '-2';
		} else {
			return '1';
		}
	}


	public function sponsorship ()
	{

		$curl = curl_init();

		curl_setopt_array( $curl, [
			CURLOPT_URL            => "https://mrm.msup.com.cn/admin.php/Top100/top100-api/get-sponsorship?id=15", //Recommend::id
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING       => "",
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 30,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => "GET",
		] );

		$response = curl_exec( $curl );
		$err = curl_error( $curl );

		curl_close( $curl );

		if (!$err){
			$datas = json_decode($response,true);
		}

		include template( 'content', 'category_member_comany' );
	}
}
