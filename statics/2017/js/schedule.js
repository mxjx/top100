$(document).ready(function(){
	
	// Variables
	var clickedTab = $(".schedule_tabs > .active");
	var tabWrapper = $(".schedule_tab__content");
	var activeTab = tabWrapper.find(".active");
	var activeTabHeight = activeTab.outerHeight();
	
	// Show tab on page load
	activeTab.show();
	
	// Set height of wrapper on page load
	tabWrapper.height(activeTabHeight);
	
	$(".schedule_tabs > li").on("click", function() {
		
		// Remove class from active tab
		$(".schedule_tabs > li").removeClass("active");
		
		// Add class active to clicked tab
		$(this).addClass("active");
		
		// Update clickedTab variable
		clickedTab = $(".schedule_tabs .active");
		
		// fade out active tab
		activeTab.fadeOut(0, function() {
			
			// Remove active class all tabs
			$(".schedule_tab__content > li").removeClass("active");
			
			// Get index of clicked tab
			var clickedTabIndex = clickedTab.index();

			// Add class active to corresponding tab
			$(".schedule_tab__content > li").eq(clickedTabIndex).addClass("active");
			
			// update new active tab
			activeTab = $(".schedule_tab__content > .active");
			
			// Update variable
			activeTabHeight = activeTab.outerHeight();
			
			// Animate height of wrapper to new tab height
			tabWrapper.stop().delay(0).animate({
				height: activeTabHeight
			}, 0, function() {
				
				// Fade in active tab
				activeTab.delay(0).fadeIn(0);
				
			});
		});
	});
});