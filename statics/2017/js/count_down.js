$(function(){
	$(".count-down").countdown("2018/12/01", function(event) {
		$('.days .number').text(
			event.strftime('%-D')
		);
		$('.hours .number').text(
			event.strftime('%-H')
		);
		$('.minutes .number').text(
			event.strftime('%-M')
		);
		$('.seconds .number').text(
			event.strftime('%-S')
		);
	});
})