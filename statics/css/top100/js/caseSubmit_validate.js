/**
 * Created by Administrator on 15-7-7.
 */
$(function(){
    var bro=$.browser;
    var binfo="";
    if(bro.msie) {binfo="Microsoft Internet Explorer "+bro.version;}
    var arr = [ "Microsoft Internet Explorer 6.0", "Microsoft Internet Explorer 7.0", "Microsoft Internet Explorer 8.0", "Microsoft Internet Explorer 9.0" ];  
    var grantFullName = "注明是您本人还是您的团队";
    show_input_message(".grantFullName",grantFullName);
    var shareInOtherMeeting = '如果曾经分享过，请注明哪个会议以及反馈分数';
    show_input_message("#shareInOtherMeeting",shareInOtherMeeting);
    var case_desc = '提示：200字~300字的案例启示或者案例简述';
    show_input_message("#case_desc",case_desc);
    show_input_message(".case_desc",case_desc);
    var team_nums = "提示：请输入整数，如14，78，96";
    show_input_message("#team_nums",team_nums);
    var tishi = "提示：\n找出案例背后哲理、方法论等。\n\n１.案例目标：当初启动此案例时（或实施后）达到的目标。\n\n２.成功（或教训）要点：成功经验总结，即哪些技术或其他地方做好了才促使项目成功。\n\n３.案例ROI分析：可能的话，进行投入产出分析。\n\n４.案例启示：提炼出该案例（或项目）的哲理、方法论。";
    var tishi_mobile = "提示：\n找出案例背后哲理、方法论等。\n１.案例目标：当初启动此案例时（或实施后）达到的目标。\n２.成功（或教训）要点：成功经验总结，即哪些技术或其他地方做好了才促使项目成功。\n３.案例ROI分析：可能的话，进行投入产出分析。\n４.案例启示：提炼出该案例（或项目）的哲理、方法论。";
    show_input_message("#case_50",tishi);
    show_input_message(".case_50",tishi_mobile);
    show_input_notice("#case_name","案例提案名称不能为空");
    // show_input_notice("#share_img","分享者照片不能为空");
    // show_input_notice("#team_nums","所在研发团队规模不能为空");
    // show_input_notice("#case_desc","案例简述不能为空");
    show_input_notice("#case_50","案例解读大纲不能为空");
    $("#subtn_pc").click(function(){
    if($("#case_name").val()=="" || $("#case_name").val()==null){
        $("#case_name_message").html("案例提案名称不能为空");
        alert("案例提案名称不能为空");
        return false;
    }
    var res1 = upload("share_img",1);
    var res2 = upload("team_logo",0);
    if(res1==false || res2 == false){
        return false;
    }
   
    if($("#casePPT").size()>0){
        if($("#casePPT").val()=="" || $("#casePPT").val()==null){
        }else{
            if($.inArray(binfo, arr) != "-1" ){
                var f = document.getElementById("casePPT"); 
            }else{

                var f = document.getElementById("casePPT").files; 
                f = f[0]; 
            }
            var flieType =  f.name.substr(f.name.lastIndexOf(".")+1);
            if(flieType == "ppt" || flieType == "pptx" || flieType == "pdf"){
                if(f.size>20*1024*1024){
                    alert("ppt稿件不能超过20M"); 
                    return false;
                }
            }  
        }
           
    }

    if($("#caseWord").size()>0){
        if($("#caseWord").val()=="" || $("#caseWord").val()==null){  
        }else{
           if($.inArray(binfo, arr) != "-1" ){
                 var f = document.getElementById("caseWord"); 
                 if(f.size>20*1024*1023){
                    alert("案例文稿不能超过20M"); 
                    return false;
                }
            }else{
                 var f = document.getElementById("caseWord").files; 
                 if(f[0].size>20*1024*1024){
                    alert("案例文稿不能超过20M"); 
                    return false;
                }
            }     
        }
           
    }
    
    var  team_nums = $("#team_nums").val();
    if( team_nums =="" || team_nums ==null){
    
    }else{
        var val = $("#team_nums").val();
       if(!isNaN(val)&&val>0){ 
        
        }else{
            alert('所在研发团队规模要求输入正整数');
            return false;
        }
    }
    if($("#case_50").val()=="" || $("#case_50").val()==null || $("#case_50").val() == tishi || $("#case_50").val() == tishi_mobile){
        $("#case_50_message").html("案例解读大纲不能为空");
         alert("案例解读大纲不能为空");
        return false;
    }
    $("#subtn_pc").css({"background":"grey"}).attr("disabled", true);
    $("#form_pc").submit();
});
function show_input_message(input,message){
    if($(input).val() == "" || $(input).val() == null){
         $(input).css("color","grey").attr("value",message);
    }
    $(input).focus(function() {
        if($(input).val() == message){
            $(input).css("color",'grey').attr("value","");
             $(input).css("color",'#555555');
        }
    }).blur(function(event) {
       if($(input).val() == "" || $(input).val() == null){
            $(input).css("color","grey").attr("value",message);
       }
    });
}
function  show_input_notice(input,notice){
    $(input).focus(function(event) {
        $(input).html("");
         $(input+'_message').hide();
    }).blur(function(event) {
          if($(input).val()=="" || $(input).val()==null){
             $(input+'_message').show().html(notice);
        }
    });
}
function upload(name,ismust){
    var e = $("#"+name)
    // 检查元素是否存在
    if(e.size()>0){
        // 检查元素是否有值
        if(e.val()=="" || e.val()==null){
            // 是否必须有值
            if(ismust == 1){
                // 获取提示信息
                var message = e.data("error");
                $("#share_img_message").html(message);
                // alert(message);
                return false;
            }
        }else{ 
            if($.inArray(binfo, arr) != "-1" ){
                //ie浏览器
                 var f = document.getElementById(name);
            }else{
                //不是ie
                var f = document.getElementById(name).files; 
                f = f[0]
            }     
            var type = f.name.substr(f.name.lastIndexOf(".")+1);
            if (imgTypeValidate(type)) {
                return imgSizeValidate(f)
            }
        }
    }
}
/**
 * 检查图片格式是否正确
 * @param  {[type]} type [description]
 * @return {[type]}      [description]
 */
function imgTypeValidate(type) {
    var imgTypeAllow = ['jpg','gif','jpeg','png']
    if ( jQuery.inArray(type.toLowerCase(), imgTypeAllow) == '-1' ) {
        alert("分享者照片,请上传正确的图片格式(jpg,png,jpeg,gif)"); 
        return false
    } 
        return true
}
function imgSizeValidate(file) {
    var size = file.size;
    if(file.size>3*1024*1023){
        alert("分享者照片不能超过3M"); 
        return false;
    }
    return true

}
});