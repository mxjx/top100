<?php

return array (
	'default' => array (
		'hostname' => '127.0.0.1',
		'database' => 'top100',
		'username' => 'root',
		'password' => '',
		'tablepre' => 'top100_',
		'charset' => 'utf8',
		'type' => 'mysql',
		'debug' => true,
		'pconnect' => 0,
		'autoconnect' => 0
		)
);

?>